<?php

$lang['cal_su']			= 'ԿՐ';
$lang['cal_mo']			= 'ԵՐ';
$lang['cal_tu']			= 'ԵՔ';
$lang['cal_we']			= 'ՉՔ';
$lang['cal_th']			= 'ՀՆ';
$lang['cal_fr']			= 'ՈՒՐ';
$lang['cal_sa']			= 'ՇԲ';
$lang['cal_sun']		= 'Կիր';
$lang['cal_mon']		= 'Երկ';
$lang['cal_tue']		= 'Երք';
$lang['cal_wed']		= 'Չոր';
$lang['cal_thu']		= 'Հնգ';
$lang['cal_fri']		= 'ՈՒրբ';
$lang['cal_sat']		= 'Շբթ';
$lang['cal_sunday']		= 'Կիրակի';
$lang['cal_monday']		= 'Երկուշաբթի';
$lang['cal_tuesday']	= 'Երեքշաբթի';
$lang['cal_wednesday']	= 'Չորեքշաբթի';
$lang['cal_thursday']	= 'Հինգշաբթի';
$lang['cal_friday']		= 'Ուրբաթ';
$lang['cal_saturday']	= 'Շաբաթ';
$lang['cal_jan']		= 'Հուն';
$lang['cal_feb']		= 'Փետ';
$lang['cal_mar']		= 'Մար';
$lang['cal_apr']		= 'Ապր';
$lang['cal_may']		= 'Մայ';
$lang['cal_jun']		= 'Հուն';
$lang['cal_jul']		= 'Հուլ';
$lang['cal_aug']		= 'Օգս';
$lang['cal_sep']		= 'Սեպ';
$lang['cal_oct']		= 'Հոկ';
$lang['cal_nov']		= 'Նոյ';
$lang['cal_dec']		= 'Դեկ';
$lang['cal_january']	= 'Հունվար';
$lang['cal_february']	= 'Փետրվար';
$lang['cal_march']		= 'Մարտ';
$lang['cal_april']		= 'Ապրիլ';
$lang['cal_mayl']		= 'Մայիս';
$lang['cal_june']		= 'Հունիս';
$lang['cal_july']		= 'Հուլիս';
$lang['cal_august']		= 'Օգոստոս';
$lang['cal_september']	= 'Սեպտեմբեր';
$lang['cal_october']	= 'Հոկտեմբեր';
$lang['cal_november']	= 'Նոյեմբեր';
$lang['cal_december']	= 'Դեկտեմբեր';

/* End of file calendar_lang.php */
/* Location: ./system/language/russian/calendar_lang.php */
