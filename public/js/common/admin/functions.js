function notification(data) {
    var isStick = (data.status == 'success'),
        notice = new PNotify({
            text: data.msg,
            type: data.status,
            delay: 2000,
            shadow: false,
            hide: isStick,
            top: true,
            buttons: {
                closer: false,
                sticker: true
            }
        });
    notice.get().click(function() {
        notice.remove();
    });
}


$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

