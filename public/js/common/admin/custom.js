$(function() {
    if (typeof GLOBAL_FLASH_MESSAGE != 'undefined') {
        notification(GLOBAL_FLASH_MESSAGE);
    }

    $('[data-toggle="tooltip"]').tooltip();


    $('.delete_guide').click(function(event){
        event.preventDefault();
        var $li = $(this).closest('li');
        var id = $li.attr('data-id');
        var $modal = $('#remove-modal');
        $('#delete_guide').attr('data-id',id);
        $modal.modal('show');
    });

    $('#delete_guide').click(function(event){
        event.preventDefault();
        var id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: 'delete',
            data: {id : id},
            dataType: 'json',
            success: function(data) {
                if ( data.status ) {
                    $('#tours li[data-id="' + id + '"]').remove();
                    $('#remove-modal').modal('hide');
                }
            }
        });
    });

    $('.delete_dest_img').click(function(event){
        event.preventDefault();
        var id = $(this).attr('data_id');
        var path = $(this).attr('data_path');
        var $modal = $('#delete_img_modal');
        $('#delete_dest_img').attr('data_id',id);
        $('#delete_dest_img').attr('data_path',path);
        $modal.modal('show');
    });

    $('#delete_dest_img').click(function(event){
        event.preventDefault();
        var id = $(this).attr('data_id');
        var path = $(this).attr('data_path');;
        $.ajax({
            type: "POST",
            url:'/admin_countries/delete_img',
            data: {id : id, path:path},
            dataType: 'json',
            success: function(data) {
                if ( data.status ) {
                    $('#destination_images div[data-id="' + id + '"]').remove();
                    $('#delete_img_modal').modal('hide');
                }
            }
        });
    });


});