
$(document).ready(function(){
    /*clock*/
    setInterval( function() {
        var seconds = new Date().getSeconds();
        var sdegree = seconds * 6;
        var srotate = "rotate(" + sdegree + "deg)";
        $(".sec").css({"-moz-transform" : srotate, "-webkit-transform" : srotate});
    }, 1000 );
    setInterval( function() {
        /*yerevan time*/
        var hours1 = new Date().getHours();
        var mins = new Date().getMinutes();
        var hdegree1 = hours1 * 30 + (mins / 2);
        var hrotate1 = "rotate(" + hdegree1 + "deg)";
        $('#clock1').find(".hour").css({"-moz-transform" : hrotate1, "-webkit-transform" : hrotate1});
        /*Moscow time*/
        var hours2 = new Date().getHours()-1;
        var hdegree2 = hours2 * 30 + (mins / 2);
        var hrotate2 = "rotate(" + hdegree2 + "deg)";
        $('#clock2').find(".hour").css({"-moz-transform" : hrotate2, "-webkit-transform" : hrotate2});
        /*London time*/
        var hours3 = new Date().getHours()-4;
        var hdegree3 = hours3 * 30 + (mins / 2);
        var hrotate3 = "rotate(" + hdegree3 + "deg)";
        $('#clock3').find(".hour").css({"-moz-transform" : hrotate3, "-webkit-transform" : hrotate3});
        /*? time*/
        var hours4 = new Date().getHours()-9;
        var hdegree4 = hours4 * 30 + (mins / 2);
        var hrotate4 = "rotate(" + hdegree4 + "deg)";
        $('#clock4').find(".hour").css({"-moz-transform" : hrotate4, "-webkit-transform" : hrotate4});
    }, 1000 );
    setInterval( function() {
        var mins = new Date().getMinutes();
        var mdegree = mins * 6;
        var mrotate = "rotate(" + mdegree + "deg)";
        $('#clock1,#clock2,#clock3,#clock4').find(".min").css({"-moz-transform" : mrotate, "-webkit-transform" : mrotate});
    }, 1000 );

    $('#header .menu_section .menu_main li').hover(function(){
        $(this).find('.sub_menu_section').stop().slideDown(function(){
            $(this).find('.sub_menu_section').css('display','block')
        });
    },function(){
        $(this).find('.sub_menu_section').stop().slideUp(function(){
            $(this).find('.sub_menu_section').css('display','none')
        });
    });

    $('.featured_section_images').hover(function(){
            $('.featured_section_images_hover').fadeIn(300);
            $('#header .menu_section .menu_main li .sub_menu_section .featured_section .featured_section_images img').addClass('zoom_img')

    },function(){
            $('.featured_section_images_hover').fadeOut(300);
            $('#header .menu_section .menu_main li .sub_menu_section .featured_section .featured_section_images img').removeClass('zoom_img')
    })

    $(".grid-item" ).hover(
        function() {
            $(this ).find(".grid_item_block").css('display','block');
        }, function() {
            $(this ).find(".grid_item_block").css('display','block');
        }
    );
    $(".trip_block_bottom .fa-paper-plane" ).hover(
        function() {
            $(this ).removeClass("fa-paper-plane");
            $(this ).addClass("fa-paper-plane-o");
        }, function() {
            $(this ).removeClass("fa-paper-plane-o");
            $(this ).addClass("fa-paper-plane");
        }
    );
    /*$(".languages-sel .selectize-dropdown .active" ).hover(
        function() {
            $(this ).css("background-color", "#F3C88A!important");
            $(this ).css("color", "#72302E!important");
            $(this ).find("span").css("background-color", "#F3C88A!important");
            $(this ).find("span").css("color", "#72302E!important");
        }, function() {
            $(this ).removeClass("fa-paper-plane");
            $(this ).addClass("fa-paper-plane-o");
        }
    );*/
    $(".side").removeClass('hidden');
    $(".side").sidecontent();

    //$('.lets-plan-your-trip').click(function(){
    //    $('.sidecontentpullout').trigger('click');
    //});

    if ($('path').length) {
        $('path').css('fill',GLOBAL_DEFAULT_COLOR).
            attr('data-color', GLOBAL_DEFAULT_COLOR).
            attr('data-color-hover', GLOBAL_DEFAULT_COLOR_HOVER);

        $.each(GLOBAL_COUNTRIES_SET, function(index, value){
            $('path').eq(value.order_in_elems).css('fill',value.color_default).
                attr('data-color', value.color_default).
                attr('data-color-hover', value.color_hover).
                attr('data-id', value.id)
        });

        $('path').mouseover(
            function(){
                $('path').each(function(){
                    $(this).css('fill',$(this).attr('data-color'));
                });
                var id = $(this).attr('data-id');
                if (typeof id !== typeof undefined && id !== false) {
                    $.ajax({
                        type: "POST",
                        url: GLOBAL_GET_COUNTRY_INFO_URL,
                        data: {id : id, lang: GLOBAL_LANG},
                        dataType: "json",
                        success: function(data) {
                            if (data.status == 'success') {
                                $('#load-part').html(data.info);
                                $('#modal-area').html(data.modal);
                            } else {
                                notification(data);
                            }
                        }
                    });
                }

                $(this).css('fill',$(this).attr('data-color-hover'));
            }
        );


        (function() {
            var $section = $('section').first();
            var $panzoom = $section.find('.panzoom').panzoom({
                $zoomIn: $section.find(".zoom-in"),
                $zoomOut: $section.find(".zoom-out"),
                $zoomRange: $section.find(".zoom-range"),
                $reset: $section.find(".reset")
            });
            $panzoom.on('panzoomend', function( e, panzoom, matrix, changed ) {
                    var $elem = $(e.target);
                    var id = $elem.attr('data-id');
                    if (typeof id !== typeof undefined && id !== false) {
                        $.ajax({
                            type: "POST",
                            url: GLOBAL_GET_COUNTRY_INFO_URL,
                            data: {id : id, lang: GLOBAL_LANG},
                            dataType: "json",
                            success: function(data) {
                                if (data.status == 'success') {
                                    $('#load-part').html(data.info);
                                    $('#modal-area').html(data.modal);
                                    $('#our-notes').modal('show');
                                } else {
                                    notification(data);
                                }
                            }
                        });
                    }
            });
        })();


    }


    var $partners = $('#partners');
    if ($partners.length > 0) {
        $partners.imagesLoaded( function(){

            $partners.masonry({
                itemSelector : '.box',
                columnWidth: 6,
                isAnimated: true
            });
        });
    }

    var $partnersEducational = $('#partners-educational');
    if ($partnersEducational.length > 0) {
        $partnersEducational.imagesLoaded( function(){

            $partnersEducational.masonry({
                itemSelector : '.box',
                columnWidth: 6,
                isAnimated: true
            });
        });
    }

    var effects = [
        'fadeInLeftBig',
        'flash',
        'bounce',
        'shake',
        'tada',
        'swing',
        'wobble',
        'pulse',
        'flip',
        'rotateInUpRight',
        'rotateInDownRight',
        'fadeInUpBig',
        'fadeInRightBig',
    ];
    var effect = effects[Math.floor((Math.random() * 13))];
    $('#any-destination').textillate({
        loop: false,
        minDisplayTime: 2000,
        initialDelay: 0,
        autoStart: true,
        inEffects: [],
        in: {
            effect: effect,
            delayScale: 1.5,
            delay: 50,
            sync: false,
            shuffle: false,
            reverse: false,
            callback: function () {
                $('#any-destination').fadeOut('slow');

            }
        },

        // set the type of token to animate (available types: 'char' and 'word')
        type: 'char'
    });

    $('.load-more-feedbacks').click(function(event){
        event.preventDefault();
        $('.box-out').removeClass('hidden-feedback');
        $(this).hide();
        $('.feedback-item').show();
        $('.row.feedback-row .box-out').css('height', 'auto');
        resizeFeedbacks();
    });

    setTimeout(function(){
        $('.spinner').hide();
        $("#carousel-slider").owlCarousel({
            autoPlay: 3000,
            items : 6
        });
        $("#carousel-slider1").owlCarousel({
            autoPlay: 3000,
            items : 6
        });
    },2000);

    var $lanuagesSelectize =  $('#languages').selectize({
        preload: true,
        maxItems: 1,
        create: false,
        valueField: 'iso_code',
        labelField: 'name_origin',
        render: {
            option: function(option, escape) {
                return '<div class="languages-box">' +
                    '<a href="' + escape(option.href) + '" class="lang-href">'+
                    '<img alt="" src="' + GLOBAL_IMAGES_URL + '/flags/' + escape(option.flag) + '">'  +
                    '<span>' + "&nbsp;&nbsp;" + escape(option.name_origin) + '</span>' +
                    '</a>'+
                    '</div>'
            },
            item: function(option, escape) {
                return '<div class="languages-box">' +
                    '<span>'+
                    '<img alt="" src="' + GLOBAL_IMAGES_URL + '/flags/' + escape(option.flag) + '">'  +
                    '<span>' + "&nbsp;&nbsp;" + escape(option.name_origin) + '</span>' +
                    '</span>'+
                    '</div>'
            }
        }
    });
    $.each(GLOBAL_LANGUAGES, function(index, value) {
        $lanuagesSelectize[0].selectize.addOption(value);
    });
    $lanuagesSelectize[0].selectize.addItem(GLOBAL_LANG);
    $('#languages').next().find('div.selectize-input > input').prop('disabled', 'disabled');
    $('#languages').change(function(){
        var href = GLOBAL_LANGUAGES[$(this).val()].href;
        window.location = href;
    });
    var langCount = 0;
    for (var k in GLOBAL_LANGUAGES) {
        if (GLOBAL_LANGUAGES.hasOwnProperty(k)) {
            langCount++;
        }
    }
    if (langCount == 1){
        setTimeout(function(){
            $('.languages-sel .selectize-dropdown').addClass('do-not-show');
            console.log($('.selectize-dropdown-content').length)
        },500);

    }


    if (typeof GLOBAL_ALL_COUNTRIES != 'undefined' && $('#countries-filter-tours').length) {
        var $countriesFilter =  $('#countries-filter-tours').selectize({
            preload: true,
            maxItems: 1,
            create: false,
            valueField: 'id',
            labelField: 'name',
            searchField: 'name',
            render: {
                option: function(option, escape) {
                    return '<div class="languages-box">' +
                        '<img alt="" src="' + GLOBAL_UPLOADS_URL + GLOBAL_COUNTRY_UPLOADS_URL + escape(option.flag) + '">'  +
                        '<span>' + "&nbsp;&nbsp;" + escape(option.name) + '</span>' +
                        '</div>'
                },
                item: function(option, escape) {
                    return '<div class="languages-box">' +
                        '<img alt="" src="' + GLOBAL_UPLOADS_URL + GLOBAL_COUNTRY_UPLOADS_URL + escape(option.flag) + '">'  +
                        '<span>' + "&nbsp;&nbsp;" + escape(option.name) + '</span>' +
                        '</div>'
                }
            }
        });
        $.each(GLOBAL_ALL_COUNTRIES, function(index, value) {
            $countriesFilter[0].selectize.addOption(value);
        });
        function filterTours(countryId){
            $('.filterable').hide();
            if (countryId) {
                $('.filterable').each(function(index){
                    var countriesList = $.parseJSON($(this).attr('data-countries'));
                    if ($.inArray( countryId,  countriesList) != -1) {
                        $(this).stop().fadeIn();
                    }
                });
            }
            else {
                $('.filterable').stop().fadeIn();
            }
        }
        $('#countries-filter-tours').change(function(){
            var val = $(this).val();
            filterTours(val);
        });
        if (typeof GLOBAL_SELECTED_COUNTRY_ID != 'undefined') {
            $countriesFilter[0].selectize.addItem(GLOBAL_SELECTED_COUNTRY_ID);
        }
    }
    if (typeof GLOBAL_ALL_COUNTRIES != 'undefined' && $('#countries-filter-map').length) {
        var $countriesFilter =  $('#countries-filter-map').selectize({
            preload: true,
            maxItems: 1,
            create: false,
            valueField: 'id',
            labelField: 'name',
            searchField: 'name',
            render: {
                option: function(option, escape) {
                    return '<div class="languages-box">' +
                        '<img alt="" src="' + GLOBAL_UPLOADS_URL + GLOBAL_COUNTRY_UPLOADS_URL + escape(option.flag) + '">'  +
                        '<span>' + "&nbsp;&nbsp;" + escape(option.name) + '</span>' +
                        '</div>'
                },
                item: function(option, escape) {
                    return '<div class="languages-box">' +
                        '<img alt="" src="' + GLOBAL_UPLOADS_URL + GLOBAL_COUNTRY_UPLOADS_URL + escape(option.flag) + '">'  +
                        '<span>' + "&nbsp;&nbsp;" + escape(option.name) + '</span>' +
                        '</div>'
                }
            }
        });
        $.each(GLOBAL_ALL_COUNTRIES, function(index, value) {
            $countriesFilter[0].selectize.addOption(value);
        });



        $('#countries-filter-map').change(function(){
            var id = $(this).val();
            if (id == "") {
                $('#load-part').html('');
                $('#modal-area').html('');
                $('path').each(function(){
                    $(this).css('fill',$(this).attr('data-color'));
                });
            } else {

                if ($('path[data-id="' + id + '"]').length) {
                    $('path').each(function(){
                        $(this).css('fill',$(this).attr('data-color'));
                    });
                    $('path[data-id="' + id + '"]').css('fill',$('path[data-id="' + id + '"]').attr('data-color-hover'));

                }
                $.ajax({
                    type: "POST",
                    url: GLOBAL_GET_COUNTRY_INFO_URL,
                    data: {id : id, lang: GLOBAL_LANG},
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'success') {
                            $('#load-part').html(data.info);
                            $('#modal-area').html(data.modal);
                            $('#our-notes').modal('show');
                        } else {
                            notification(data);
                        }
                    }
                });


            }
        })
    }

    if ($('#visas-incoming-without').length) {
        $('#visas-incoming-without').dataTable({
            bFilter: true,
            bInfo: false,
            bServerSide: false,
            bProcessing: false,
            bPaginate: false,
            bAutoWidth: false,
            bStateSave: false,
            iDisplayLength: 25,
            sAjaxSource: null,
            sPaginationType: "bootstrap",
            aaData: aaDataWithout,
            aaSorting: [
                [0, "asc"]
            ],
            "aoColumns": [
                {
                    name: "order",
                    class: "hidden"
                },
                {
                    name: "Country",
                    sortable: false,
                    searchable: true,

                },
                {
                    name: "flag",
                    sortable: false,
                    searchable: true,

                },
                {
                    name: "content",
                    sortable: false,
                    searchable: true,

                }

            ],
            "initComplete": function(settings, json) {
                $('#visas-incoming-without_filter > label').contents().get(0).nodeValue = '',
                    $('#visas-incoming-without_filter > label').prepend('<i class="fa fa-search"></i>&nbsp;&nbsp;'),
                    $('#visas-incoming-without_filter > label').css({
                        'margin-top': '25%',
                        'margin-bottom': '10%'
                    })
            }
        });
    }

    if ($('#visas-incoming-with').length) {
        $('#visas-incoming-with').dataTable({
            bFilter: true,
            bInfo: false,
            bServerSide: false,
            bProcessing: false,
            bPaginate: false,
            bAutoWidth: false,
            bStateSave: false,
            iDisplayLength: 25,
            sAjaxSource: null,
            sPaginationType: "bootstrap",
            aaData: aaDataWith,
            aaSorting: [
                [0, "asc"]
            ],
            "aoColumns": [
                {
                    name: "order",
                    class: "hidden"
                },
                {
                    name: "Country",
                    sortable: false,
                    searchable: true,

                },
                {
                    name: "flag",
                    sortable: false,
                    searchable: true,

                },
                {
                    name: "content",
                    sortable: false,
                    searchable: true,

                }

            ],
            "initComplete": function(settings, json) {
                $('#visas-incoming-with_filter > label').contents().get(0).nodeValue = '',
                    $('#visas-incoming-with_filter > label').prepend('<i class="fa fa-search"></i>&nbsp;&nbsp;'),
                    $('#visas-incoming-with_filter > label').css({
                        'margin-top': '25%',
                        'margin-bottom': '10%'
                    })
            }
        });
    }

    if ($('#visas-outgoing-without').length) {
        $('#visas-outgoing-without').dataTable({

            bInfo: false,
            bServerSide: false,
            bProcessing: false,
            bPaginate: false,
            bAutoWidth: false,
            bFilter: true,
            bStateSave: false,
            iDisplayLength: 25,
            sAjaxSource: null,
            sPaginationType: "bootstrap",
            aaData: aaDataWithout,
            aaSorting: [
                [0, "asc"]
            ],
            "aoColumns": [
                {
                    name: "order",
                    class: "hidden"
                },
                {
                    name: "Country",
                    sortable: false,
                    searchable: true,

                },
                {
                    name: "flag",
                    sortable: false,
                    searchable: true,

                },
                {
                    name: "content",
                    sortable: false,
                    searchable: true,

                }

            ],
            "initComplete": function(settings, json) {
                $('#visas-outgoing-without_filter > label').contents().get(0).nodeValue = '',
                    $('#visas-outgoing-without_filter > label').prepend('<i class="fa fa-search"></i>&nbsp;&nbsp;'),
                    $('#visas-outgoing-without_filter > label').css({
                        'margin-top': '25%',
                        'margin-bottom': '10%'
                    })
            }
        });
    }

    if ($('#visas-outgoing-with').length) {
        $('#visas-outgoing-with').dataTable({
            bFilter: true,
            bInfo: false,
            bServerSide: false,
            bProcessing: false,
            bPaginate: false,
            bAutoWidth: false,
            bStateSave: false,
            iDisplayLength: 25,
            sAjaxSource: null,
            sPaginationType: "bootstrap",
            aaData: aaDataWith,
            aaSorting: [
                [0, "asc"]
            ],
            "aoColumns": [
                {
                    name: "order",
                    class: "hidden"
                },
                {
                    name: "Country",
                    sortable: false,
                    searchable: true,

                },
                {
                    name: "flag",
                    searchable: true,
                    sortable: false,

                },
                {
                    name: "content",
                    searchable: true,
                    sortable: false,

                }

            ],
            "initComplete": function(settings, json) {
                var search = $('#visas-outgoing-with_filter > label');
                search.contents().get(0).nodeValue = '',
                    search.prepend('<i class="fa fa-search"></i>&nbsp;&nbsp;'),
                    search.css({
                        'margin-top': '25%',
                        'margin-bottom': '10%'
                    })
            }
        });
    }

    var $letsPlanForm = $('#lets-plan-form');
    $letsPlanForm.validate({
        rules: {
            modal_first_name: {
                required:true
            },
            modal_email: {
                required: true,
                email: true
            },
            modal_telephone :{
                required:true
            }
        },
        highlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        success: function(label) {
            $(label).closest('form').find('.valid').removeClass("invalid");
        },
        errorPlacement: function(error, element) {
        }
    });
    $(document).on('submit', '#lets-plan-form', function(){
        event.preventDefault();
        if ($(this).valid()){
            var buttonText = $('#submit-modal').text();
            $('#submit-modal').html('<i class="fa fa-spin fa-spinner"></i>');
            var data = $(this).serializeObject();
            data.lang = GLOBAL_LANG;
            $.ajax({
                type: "POST",
                url: SEND_LETS_PLAN_YOUR_TRIP_URL,
                data: data,
                dataType: "json",
                success: function(res) {
                    $('#submit-modal').html(buttonText);
                    notification(res);
                    if (res.status == 'success') {
                        $('#lets-plan').modal('hide');
                    }
                }
            });
        }
    })

    $(document).on('submit', '#get_news_form', function(){
        event.preventDefault();
            var data = $(this).serializeObject();
            data.lang = GLOBAL_LANG;
            $.ajax({
                type: "POST",
                url: '/home/get_news',
                data: data,
                dataType: "json",
                success: function(res) {
                    //$('#submit-modal').html(buttonText);
                    notification(res);
                    if (res.status == 'success') {
                        $('#lets-plan').modal('hide');
                    }
                }
            });
    })


    var $orderForm = $('#order-form');
    if ($orderForm.length) {
        $orderForm.validate({
            rules: {
                order_first_name: {
                    required:true
                },
                order_last_name: {
                    required:true
                },
                order_travelers_count: {
                    required:true,
                },
                order_email: {
                    required: true,
                    email: true
                },
                order_captcha :{
                    required:true
                },
                order_telephone :{
                    required:true
                }
            },
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            success: function(label) {
                $(label).closest('form').find('.valid').removeClass("invalid");
            },
            errorPlacement: function(error, element) {
            }
        });

        $orderForm.submit(function(event){
            event.preventDefault();
            if ($orderForm.valid()){
                var buttonText = $('.send-order-btn').text();
                $('.send-order-btn').html('<i class="fa fa-spin fa-spinner"></i>');
                var data = $orderForm.serializeObject();
                data.tour_name = $('.tour-name').text();
                data.lang = GLOBAL_LANG;
                $.ajax({
                    type: "POST",
                    url: GLOBAL_ORDER_TOUR,
                    data: data,
                    dataType: "json",
                    success: function(res) {
                        $('.send-order-btn').html(buttonText);
                        notification(res);
                        if (res.status == 'success') {
                            $('#order-tour').modal('hide');
                        }
                    }
                });
            }
        })
    }

    var $contactUsForm = $('#contact-us-form');
    if ($contactUsForm.length) {
        $contactUsForm.validate({
            rules: {
                contact_us_name: {
                    required:true
                },
                contact_us_subject: {
                    required:true
                },
                contact_us_email: {
                    required: true,
                    email: true
                },
                contact_us_captcha :{
                    required:true
                },
                contact_us_message :{
                    required:true
                }
            },
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            success: function(label) {
                $(label).closest('form').find('.valid').removeClass("invalid");
            },
            errorPlacement: function(error, element) {
            }
        });

        $contactUsForm.submit(function(event){
            event.preventDefault();
            if ($contactUsForm.valid()){
                var buttonText = $('.send-message-contact-us').text();
                $('.send-message-contact-us').html('<i class="fa fa-spin fa-spinner"></i>');
                var data = $contactUsForm.serializeObject();
                data.lang = GLOBAL_LANG;
                $.ajax({
                    type: "POST",
                    url: GLOBAL_SEND_CONTACT_US_MSG,
                    data: data,
                    dataType: "json",
                    success: function(res) {
                        $('.send-message-contact-us').html(buttonText);
                        notification(res);
                    }
                });
            }
        })
    }


    $('.can-not-see').click(function(event){
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: CHANGE_CAPTCHA,
            data: {},
            dataType: "json",
            success: function(res) {
                if (res.status == 'error') {
                    notification(res)
                } else {
                    $('.captcha-image').attr('src', res.image)
                }
            }
        });
    });

    if ($('#order_date_from').length) {
        $('#order_date_from').datepicker();
        $('#order_date_to').datepicker();
    }


     if ($('#visas-incoming-without').length) {
         $('#visas-incoming-without').dataTable({
             bFilter: true,
             bInfo: true,
             bServerSide: false,
             bProcessing: false,
             bPaginate: true,
             bAutoWidth: false,
             bStateSave: true,
             iDisplayLength: 25,
             sAjaxSource: null,
             sPaginationType: "bootstrap",
             aaData: aaDataWithout,
             aaSorting: [
                 [0, "asc"]
             ],
             "aoColumns": [
                 {
                     name: "order",
                     class: "hidden"
                 },
                 {
                     name: "name",
                     sortable: false,

                 },
                 {
                     name: "flag",
                     searchable: false,
                     sortable: false,

                 }

             ]
         });
     }

    if ($('#visas-incoming-with').length) {
        $('#visas-incoming-with').dataTable({
            bFilter: true,
            bInfo: true,
            bServerSide: false,
            bProcessing: false,
            bPaginate: true,
            bAutoWidth: false,
            bStateSave: true,
            iDisplayLength: 25,
            sAjaxSource: null,
            sPaginationType: "bootstrap",
            aaData: aaDataWith,
            aaSorting: [
                [0, "asc"]
            ],
            "aoColumns": [
                {
                    name: "order",
                    class: "hidden"
                },
                {
                    name: "name",
                    sortable: false,
                },
                {
                    name: "flag",
                    searchable: false,
                    sortable: false,
                }

            ]
        });
    }

    if ($('#visas-outgoing-without').length) {
        $('#visas-outgoing-without').dataTable({
            bFilter: true,
            bInfo: true,
            bServerSide: false,
            bProcessing: false,
            bPaginate: true,
            bAutoWidth: false,
            bStateSave: true,
            iDisplayLength: 25,
            sAjaxSource: null,
            sPaginationType: "bootstrap",
            aaData: aaDataWithout,
            aaSorting: [
                [0, "asc"]
            ],
            "aoColumns": [
                {
                    name: "order",
                    class: "hidden"
                },
                {
                    name: "name",
                    sortable: false,

                },
                {
                    name: "flag",
                    searchable: false,
                    sortable: false,

                },
                {
                    name: "content",
                    searchable: false,
                    sortable: false,

                }

            ]
        });
    }

    if ($('#visas-outgoing-with').length) {
        $('#visas-outgoing-with').dataTable({
            bFilter: true,
            bInfo: true,
            bServerSide: false,
            bProcessing: false,
            bPaginate: true,
            bAutoWidth: false,
            bStateSave: true,
            iDisplayLength: 25,
            sAjaxSource: null,
            sPaginationType: "bootstrap",
            aaData: aaDataWith,
            aaSorting: [
                [0, "asc"]
            ],
            "aoColumns": [
                {
                    name: "order",
                    class: "hidden"
                },
                {
                    name: "name",
                    sortable: false,

                },
                {
                    name: "flag",
                    searchable: false,
                    sortable: false,

                },
                {
                    name: "content",
                    searchable: false,
                    sortable: false,

                }

            ]
        });
    }

    if (typeof GLOBAL_SELECTED_COUNTRY_SUBTYPE_FOR_VISAS != 'undefined'){
        $('.see-all').removeClass('hidden');
        $('.anchors').addClass('hidden');
        if (GLOBAL_SELECTED_COUNTRY_SUBTYPE_FOR_VISAS == 1) {
            $('#visas-outgoing-without_filter input[type="search"]').val(GLOBAL_SELECTED_COUNTRY_NAME_FOR_VISAS);
            $('#visas-outgoing-without_filter input[type="search"]').trigger('keyup');
            $('.visas-outgoing-with-container').hide();
        } else {
            $('#visas-outgoing-with_filter input[type="search"]').val(GLOBAL_SELECTED_COUNTRY_NAME_FOR_VISAS);
            $('#visas-outgoing-with_filter input[type="search"]').trigger('keyup');
            $('.visas-outgoing-without-container').hide();
        }
    } else {
        $('.with-visa').click(function(event){
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $('.visas-outgoing-with-container').offset().top
            }, 2000);
        });
        $('.without-visa').click(function(event){
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $('.visas-outgoing-without-container').offset().top
            }, 2000);
        })
    }
    if ($('.feedback-row').length) {
        setTimeout(function(){resizeFeedbacks();},10);
        $(window).resize(function() {
            $('.row.feedback-row .box-out').css('height', 'auto');
            resizeFeedbacks();
        });
    }

    /*if (typeof GLOBAL_OPEN != 'undefined') {
        setTimeout(function(){
            $('.sidecontentpullout').trigger('click');
        }, 3000);
        setTimeout(function(){
            if ($($('.sidecontentpullout').hasClass('opened'))){
                $('.sidecontentpullout').trigger('click');
            }
        }, 7000);
    }

        $('.sidecontentpullout').mouseover(function(){
            if ( $('.sidecontentpullout').hasClass('is-opened') || typeof GLOBAL_OPEN == 'undefined') {
                if (!$(this).hasClass('opened')) {
                    $(this).addClass('opened');
                    $(this).trigger('click');
                }
            }
        });*/

    /*$('.sidecontentpullout').click(function(){
        if ($(this).hasClass('is-opened')) {
            $.ajax({
                type: "POST",
                url: DO_NOT_OPEN_AGAIN,
                data: {},
                dataType: "json",
                success: function(res) {
                    if (res.status == 'error') {
                        notification(res)
                    }
                }
            });
        }
    })*/

/*    destination animate 'click to see'   */
    $(function(){
        var nav = $('.nav');
        $('.destination_content').find('.click_to_see').click(function(){
            var nav = $(this).parents('.destination_content').find('.show_more');
            if(nav.height() === 0){
                autoHeightAnimate(nav, 500);
                $(this).html('<i class="fa fa-angle-double-up"></i> ');
                $(this).parents('.destination_content').find(".destination_info").find('.click_to_see').html('<i class="fa fa-angle-double-up"></i> ');
            } else {
                nav.stop().animate({ height: '0' }, 500);
                $(this).html('<i class="fa fa-angle-double-down"></i>');
                $(this).parents('.destination_content').find(".destination_block").find('.click_to_see').html('<i class="fa fa-angle-double-down"></i> ');
            }
        });
    })
    /* Function to animate height: auto */
    function autoHeightAnimate(element, time){
        var curHeight = element.height(), // Get Default Height
            autoHeight = element.css('height', 'auto').height(); // Get Auto Height
        element.height(curHeight); // Reset to Default Height
        element.stop().animate({ height: autoHeight }, parseInt(time)); // Animate to Auto Height
    }
    setTimeout(function(){
        var slide_height = $('.slide_container').height();
        alert(slide_height);
        $('.continent_menus_home ').height(slide_height);
    }, 500);

/******/
    $(".destination_tabs").find("[aria-controls='europe']").click();

});
function resizeFeedbacks()
{
    if ($(window).width() > 750) {
        Array.prototype.max = function() {
            return Math.max.apply(null, this);
        };
        $('.row.feedback-row').each(function(){
            var arrayOfHeights = [];
            $(this).find('.box-out').each(function(){
                if ($(this).hasClass('hidden-feedback')) {
                    return true;
                }
                arrayOfHeights.push($(this).height());
            });
            var MaxHeight = arrayOfHeights.max() + 50;
            $(this).find('.box-out').each(function(){
                if ($(this).hasClass('hidden-feedback')) {
                    return true;
                }
                $(this).css('height', MaxHeight + 'px');
            });
        });
    }


}
$(document).on('click', '.open-fancy', function(){
    var params = $(this).attr('data-params');
    $.fancybox.open(jQuery.parseJSON(params), {padding: 0});
});

$(document).on('click', '.calculate_currency', function(){
    var val = $('.valute_from').val();
    var from = $('.select_valute_from').val();
    var to = $('.select_valute_to').val();
    if(val){
        $.ajax({
            type: "POST",
            url: '/home/get_currency',
            data: {amount : val,
                from:from,
                to:to,},
            dataType: "json",
            success: function(data) {
                if (data) {
                     $('.valute_to').val(data);
                }
            }
        });
    }
});

function initMap() {
    var myLatLng = {lat: 40.179658
        , lng: 44.527631
    };

    var map = new google.maps.Map(document.getElementById('g-map'), {
        zoom: 15,
        center: myLatLng
    });
    var infowindow = new google.maps.InfoWindow({
        content: 'Lore Travel'
    });
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Lore Travel',
        text: 'sdfsdf'
    });
    infowindow.open(map,marker);
}

$(document).on('keyup', '.customer_name', function(){
    var val = $(this).val();
    $('.customer_name').val(val);
});
$(document).on('keyup', '.descr_feedback', function(){
    var val = $(this).val();
    $('.descr_feedback').val(val);
});






/*     destination page    */
$(function () {
    // Slideshow
    $(".rslides").responsiveSlides({
        auto: false,
        pager: true,
        nav: true,
        speed: 500,
        maxwidth: 800,
        namespace: "centered-btns"
    });
});

/*     /destination page    */
