(function($)
{
	// This script was written by Steve Fenton
	// http://www.stevefenton.co.uk/Content/Jquery-Side-Content/
	// Feel free to use this jQuery Plugin
	// Version: 3.0.2
    // Contributions by: 
	
	var classModifier = "";
	var sliderCount = 0;
	var sliderWidth = "400px";
	
	var attachTo = "rightside";
	
	var totalPullOutHeight = 0;
	
	//function CloseSliders (thisId) {
	//	// Reset previous sliders
	//	for (var i = 0; i < sliderCount; i++) {
	//		var sliderId = classModifier + "_" + i;
	//		var pulloutId = sliderId + "_pullout";
	//
	//		// Only reset it if it is shown
	//		if ($("#" + sliderId).width() > 0) {
    //
	//			if (sliderId == thisId) {
	//				// They have clicked on the open slider, so we'll just close it
	//				showSlider = false;
	//			}
    //
	//			// Close the slider
	//			$("#" + sliderId).animate({
	//				width: "0px"
	//			}, 900);
	//
	//			// Reset the pullout
	//			if (attachTo == "leftside") {
	//				$("#" + pulloutId).animate({
	//					left: "0px"
	//				}, 900);
	//			} else {
	//				$("#" + pulloutId).animate({
	//					right: "0px"
	//				}, 900, function(){$('.sidecontentpullout').removeClass('opened').html('<i class="glyphicon glyphicon-arrow-left"></i>')});
	//			}
	//		}
	//	}
	//}
	
	function ToggleSlider () {
	
		var rel = $(this).attr("rel");

		var thisId = classModifier + "_" + rel;
		var thisPulloutId = thisId + "_pullout";
		var showSlider = true;
		
		if ($("#" + thisId).width() > 0) {
			showSlider = false;
		}
		
		CloseSliders(thisId);
		
		if (showSlider) {
			// Open this slider
			$("#" + thisId).animate({
				width: sliderWidth
			}, 900);
			
			// Move the pullout
			if (attachTo == "leftside") {
				$("#" + thisPulloutId).animate({
					left: sliderWidth
				}, 900);
			} else {
				$("#" + thisPulloutId).animate({
					right: sliderWidth
				}, 900, function(){$('.sidecontentpullout').addClass('opened').addClass('is-opened');$('.sidecontentpullout').html('<i class="glyphicon glyphicon-arrow-right"></i>')});
			}
		}
		
		return false;
	};

	$.fn.sidecontent = function (settings) {
	
		var config = {
			classmodifier: "sidecontent",
			attachto: "rightside",
			width: "300px",
			opacity: "0.8",
			pulloutpadding: "5",
			textdirection: "vertical",
			clickawayclose: false
		};
		
		if (settings) {
			$.extend(config, settings);
		}
		
		return this.each(function () {
		
			$This = $(this);
			
			// Hide the content to avoid flickering
			$This.css({ opacity: 0 });
			
			classModifier = config.classmodifier;
			sliderWidth = config.width;
			attachTo = config.attachto;
			
			var sliderId = classModifier + "_" + sliderCount;

			
			// Start the totalPullOutHeight with the configured padding
			if (totalPullOutHeight == 0) {
				totalPullOutHeight += parseInt(config.pulloutpadding);
			}
			
			// Wrap the content in a slider and add a pullout			
			$This.wrap('<div class="' + classModifier + '" id="' + sliderId + '"></div>').wrap('<div style="width: ' + sliderWidth + '"></div>');
			$("#" + sliderId).before('<div class="' + classModifier + 'pullout" id="' + sliderId + '_pullout" rel="' + sliderCount + '">' + '<i class="glyphicon glyphicon-arrow-left"></i>' + '</div>');

			
			// Hide the slider
			$("#" + sliderId).css({
				position: "fixed",
				overflow: "hidden",
				top: "0",
				width: "0px",
				zIndex: "999999999999",
				opacity: config.opacity
			});
			
			// For left-side attachment
			if (attachTo == "leftside") {
				$("#" + sliderId).css({
					left: "0px"
				});
			} else {
				$("#" + sliderId).css({
					right: "0px"
				});
			}
			
			// Set up the pullout
			$("#" + sliderId + "_pullout").css({
				position: "absolute",
				top: totalPullOutHeight + "px",
				zIndex: "1000",
				cursor: "pointer",
				opacity: config.opacity
			})
			
			$("#" + sliderId + "_pullout").on("click", ToggleSlider);
			
			var pulloutWidth = $("#" + sliderId + "_pullout").width();
			
			// For left-side attachment
			if (attachTo == "leftside") {
				$("#" + sliderId + "_pullout").css({
					left: "0px",
					width: pulloutWidth + "px"
				});
			} else {
				$("#" + sliderId + "_pullout").css({
					right: "0px",
					width: pulloutWidth + "px"
				});
			}
			
			totalPullOutHeight += parseInt($("#" + sliderId + "_pullout").height());
			totalPullOutHeight += parseInt(config.pulloutpadding);
			
			var suggestedSliderHeight = totalPullOutHeight + 30;
			if (suggestedSliderHeight > $("#" + sliderId).height()) {
				$("#" + sliderId).css({
					height: suggestedSliderHeight + "px"
				});
			}
			
			if (config.clickawayclose) {
				$("body").click( function () {
					CloseSliders("");
				});
			}
			
			// Put the content back now it is in position
			$This.css({ opacity: 1 });
			
			sliderCount++;
		});
		
		return this;
	};
})(jQuery);