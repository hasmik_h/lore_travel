$(function() {
    var $partners = $('#partners');
    $partners.sortable();
    $partners.disableSelection();

    $('#submit-changes').click(function(event){
        event.preventDefault();
        var ids = [];
        $('#partners li').each(function(index){
            var id = $(this).attr('data-id');
            ids.push(id);
        });
        $.ajax({
            type: "POST",
            url: GLOBAL_SAVE_ORDER_URL,
            data: {ids : ids},
            dataType: "json",
            success: function(data) {
                notification(data);
            }
        });
    });

    $('.delete-partner').click(function(event){
        event.preventDefault();
        var $li = $(this).closest('li');
        var id = $li.attr('data-id');
        var $modal = $('#remove-modal');
        $('#delete-partner').attr('data-id',id);
        $modal.modal('show');
    });

    $('#delete-partner').click(function(event){
        event.preventDefault();
        var id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: GLOBAL_DELETE_PARTNER,
            data: {id : id},
            dataType: "json",
            success: function(data) {
                notification(data);
                if (data.status == 'success') {
                    $('#partners li[data-id="' + id + '"]').remove();
                    $('#remove-modal').modal('hide');
                }
            }
        });
    });
});
