$(function() {
    var $feedbacks = $('#feedbacks');
    $feedbacks.sortable();
    $feedbacks.disableSelection();

    $('#submit-changes').click(function(event){
        event.preventDefault();
        var ids = [];
        $('#feedbacks li').each(function(index){
            var id = $(this).attr('data-id');
            ids.push(id);
        });
        $.ajax({
            type: "POST",
            url: GLOBAL_SAVE_ORDER_URL,
            data: {ids : ids},
            dataType: "json",
            success: function(data) {
                notification(data);
            }
        });
    });

    $('.delete-feedback').click(function(event){
        event.preventDefault();
        var $li = $(this).closest('li');
        var id = $li.attr('data-id');
        var $modal = $('#remove-modal');
        $('#delete-feedback').attr('data-id',id);
        $modal.modal('show');
    });

    $('#delete-feedback').click(function(event){
        event.preventDefault();
        var id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: GLOBAL_DELETE_FEEDBACK,
            data: {id : id},
            dataType: "json",
            success: function(data) {
                notification(data);
                if (data.status == 'success') {
                    $('#feedbacks li[data-id="' + id + '"]').remove();
                    $('#remove-modal').modal('hide');
                }
            }
        });
    });
});
