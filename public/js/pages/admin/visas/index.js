$(function() {
    var $visas = $('#visas');
    $visas.sortable();
    $visas.disableSelection();

    $('#submit-changes').click(function(event){
        event.preventDefault();
        var ids = [];
        $('#visas li').each(function(index){
            var id = $(this).attr('data-id');
            ids.push(id);
        });
        $.ajax({
            type: "POST",
            url: GLOBAL_SAVE_ORDER_URL,
            data: {ids : ids},
            dataType: "json",
            success: function(data) {
                notification(data);
            }
        });
    });

    $('.delete-visa').click(function(event){
        event.preventDefault();
        var $li = $(this).closest('li');
        var id = $li.attr('data-id');
        var $modal = $('#remove-modal');
        $('#delete-visa').attr('data-id',id);
        $modal.modal('show');
    });

    $('#delete-visa').click(function(event){
        event.preventDefault();
        var id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: GLOBAL_DELETE_VISA,
            data: {id : id},
            dataType: "json",
            success: function(data) {
                notification(data);
                if (data.status == 'success') {
                    $('#visas li[data-id="' + id + '"]').remove();
                    $('#remove-modal').modal('hide');
                }
            }
        });
    });
});
