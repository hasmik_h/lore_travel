$(function() {
    var $coomonSettingsForm = $('#common-settings-form');
    $coomonSettingsForm.validate({
        rules: {
            mail_contact_us: {
                required:true,
                email:true
            },
            mail_lets_plan_your_trip: {
                required:true,
                email:true
            },
            mail_order_tour: {
                required:true,
                email:true
            },
            url_facebook: {
                url:true
            },
            url_google_plus: {
                url:true
            },
            url_twitter: {
                url:true
            },
            url_pinterest: {
                url:true
            },
            url_linked_in: {
                url:true
            }
        },
        highlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        success: function(label) {
            $(label).closest('form').find('.valid').removeClass("invalid");
        },
        errorPlacement: function(error, element) {
        }
        });
});