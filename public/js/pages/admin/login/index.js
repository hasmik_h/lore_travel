$(function() {
    /*
     Fullscreen background
     */
    $.backstretch("/public/img/admin_login_1.jpg");

    var $loginForm = $('#login-form');

        $loginForm.validate({
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            success: function(label) {
                $(label).closest('form').find('.valid').removeClass("invalid");
            },
            errorPlacement: function(error, element) {
            }
        });


    $loginForm.submit(function(event) {
        event.preventDefault();

        if($loginForm.valid()) {

            $.ajax({
                type: "POST",
                url: GLOBAL_BASE_URL + GLOBAL_ADMIN_LOGIN_URL,
                data: {username: $('#username').val(), password: $('#password').val()},
                dataType: "json",
                success: function(data) {
                    if (data.status == 'success') {
                            window.location.href = GLOBAL_BASE_URL + GLOBAL_REDIRECT_ADMIN_URL;
                    } else {
                       $('.wrong-data').text(data.msg).fadeIn();
                        $('#login-form .form-group').addClass('has-error');
                    }
                }
            });
        }


    })

});