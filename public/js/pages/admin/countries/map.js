$(document).ready(function(){
    $('path').css('fill',GLOBAL_DEFAULT_COLOR).
        attr('data-color', GLOBAL_DEFAULT_COLOR).
        attr('data-color-hover', GLOBAL_DEFAULT_COLOR_HOVER);

    $.each(GLOBAL_COUNTRIES_SET, function(index, value){
        $('path').eq(value.order_in_elems).css('fill',value.color_default).
            attr('data-color', value.color_default).
            attr('data-color-hover', value.color_hover).
            attr('data-id', value.id)
    });

    $('path').hover(
        function(){
            $(this).css('fill',$(this).attr('data-color-hover'));
        },
        function(){
            $(this).css('fill',$(this).attr('data-color'));
        }
    );

    $('path').click(function() {
        var elemIndex = $(this).index();
        var id = $(this).attr('data-id');
        if (typeof id !== typeof undefined && id !== false) {
            $('#countries-select').val(id);
        } else {
            $('#countries-select').val(-1);
        }
        $('#save-button').attr('data-index',elemIndex);
        $('#choose-country-modal').modal('show');
    });

    $('#save-button').click(function(event){
        event.preventDefault();
        var id = $('#countries-select').val();
        var elemIndex = $(this).attr('data-index');
        $.ajax({
            type: "POST",
            url: GLOBAL_SAVE_COUNTRY_ON_MAP,
            data: {id : id, elemIndex: elemIndex},
            dataType: "json",
            success: function(data) {
                if (data.status == 'success') {
                    location.reload();
                } else {
                    notification(data);
                }
            }
        });
    })

});