$(function() {

    $("#languages").msDropdown();

    $languagesForm = $('#language-form');

    $languagesForm.submit(function(event) {
        event.preventDefault();

            $.ajax({
                type: "POST",
                url: GLOBAL_BASE_URL + GLOBAL_CHANGE_LANGUAGE,
                data: {languageId: $("#languages").val()},
                dataType: "json",
                success: function(data) {
                    if(data.status == 'error') {
                        notification(data);
                    } else {
                        location.reload();
                    }
                }
            });

    })

});