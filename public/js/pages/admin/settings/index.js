$(function() {


    var $settingsForm = $('#settings-form');

    $settingsForm.validate({
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            success: function(label) {
                $(label).closest('form').find('.valid').removeClass("invalid");
            },
            errorPlacement: function(error, element) {
            }
        });


    $settingsForm.submit(function(event) {
        event.preventDefault();

        if($settingsForm.valid()) {
            if ($('#new_password').val() != $('#retype_new_password').val()) {
                notification({
                    'status' : 'error',
                    'msg'    : GLOBAL_PASSWORDS_DO_NOT_MATCH
                });
                return false;
            }
            $.ajax({
                type: "POST",
                url: GLOBAL_BASE_URL + GLOBAL_CHANGE_PASSWORD,
                data: $settingsForm.serializeObject(),
                dataType: "json",
                success: function(data) {
                   notification(data);
                }
            });
        }

    })

});