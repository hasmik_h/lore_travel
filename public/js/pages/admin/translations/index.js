$(function () {
    gTable = $('#translations').dataTable({
        bFilter: true,
        bInfo: true,
        bServerSide: false,
        bProcessing: false,
        bPaginate: true,
        bAutoWidth: true,
        bStateSave: true,
        iDisplayLength: 25,
        sAjaxSource: null,
        sPaginationType: "bootstrap",
        aaData: aaData,
        aaSorting: [
            [1, "asc"]
        ],
        "aoColumns": [
            {
                name: "lang",
                sortable: false,
                width: '15%'
            },
            {
                name: "id",
                searchable: false,
                sortable: false,
                width: '10%'
            },
            {
                name: "content",
                sortable: false,
                class: 'translation-content'
            }, {
                name: "buttons",
                sortable: false,
                searchable: false,
                width: "1%"
            }]
    });
});
$(document).on('click', '.edit-translation', function (event) {
    event.preventDefault();
    var id = $(this).attr('data-id');
    $.ajax({
        type: "POST",
        url: GLOBAL_GET_BY_ID_URL,
        data: {id : id},
        dataType: "json",
        success: function(data) {
            if (data.status == 'success') {
                $.each(data.basicIfo.content, function(index, value){
                    $('textarea[name="content[' + index + ']"]').val(value);
                });
                $('#hidden-id').val(id);
                $('#edit-modal').modal('show');
            }
        }
    });


});