$(function() {
    var $tourGallery = $('#tour-gallery');
    $tourGallery.sortable();
    $tourGallery.disableSelection();

    $('#submit-changes').click(function(event){
        event.preventDefault();
        var ids = [];
        $('#tour-gallery li').each(function(index){
            var id = $(this).attr('data-id');
            ids.push(id);
        });
        $.ajax({
            type: "POST",
            url: GLOBAL_SAVE_ORDER_URL,
            data: {ids : ids},
            dataType: "json",
            success: function(data) {
                notification(data);
            }
        });
    });

    $('.delete-tour-gallery-image').click(function(event){
        event.preventDefault();
        var $li = $(this).closest('li');
        var id = $li.attr('data-id');
        var $modal = $('#remove-modal');
        $('#delete-tour-gallery-image').attr('data-id',id);
        $modal.modal('show');
    });

    $('#delete-tour-gallery-image').click(function(event){
        event.preventDefault();
        var id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: GLOBAL_DELETE_GALLERY_IMAGE,
            data: {id : id},
            dataType: "json",
            success: function(data) {
                notification(data);
                if (data.status == 'success') {
                    $('#tour-gallery li[data-id="' + id + '"]').remove();
                    $('#remove-modal').modal('hide');
                }
            }
        });
    });
});
