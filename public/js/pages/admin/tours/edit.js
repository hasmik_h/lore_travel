$(function() {
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );
    var $toursForm = $('#tours-form');
    $toursForm.validate({
            rules: {
                main_img: {
                required: (isEdit) ? false : true
                },
                thumb: {
                    required: (isEdit) ? false : true
                },
                slug: {
                    required:  true,
                    regex: "^[a-z0-9-_]+$"
                }
            },
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            success: function(label) {
                $(label).closest('form').find('.valid').removeClass("invalid");
            },
            errorPlacement: function(error, element) {
            }
        });
    if ($('#countries_attached').length) {
        $('#countries_attached').selectize({plugins:['remove_button'],selectOnTab:true});
    }
});