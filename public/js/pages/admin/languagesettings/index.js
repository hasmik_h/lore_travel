$(function() {

    var $languageSettingsForm = $('#language-settings-form');

    $languageSettingsForm.validate({
        highlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        success: function(label) {
            $(label).closest('form').find('.valid').removeClass("invalid");
        },
        errorPlacement: function(error, element) {
        }
    });

    $languageSettingsForm.submit(function(event) {
        event.preventDefault();
        if ($(this).valid()) {
            var $editModal = $('#edit-modal');
            var $originalLanguage = $('#lang-name-original');
            var $isDefaultLanguage = $('#is-default-language');
            var $isHiddenLanguage = $('#is-hidden-language');

            if ($isDefaultLanguage.is(':checked')) {
                $('#active-languages li').each(function() {
                    $(this).attr('data-is-default',0);
                });
            }

            var $li = $('#active-languages li[data-id="' + $editModal.attr('data-id') + '"]');
            $li.attr('data-name-origin', $originalLanguage.val()).
                attr('data-is-default',$isDefaultLanguage.is(':checked') ? 1 : 0).
                attr('data-is-hidden',$isHiddenLanguage.is(':checked') ? 1 : 0);
            $editModal.modal('hide');
        }
    });

    $('.btn-toggle').click(function() {
        $(this).find('.btn').toggleClass('active');

        $(this).find('.btn').toggleClass('btn-primary');

        $(this).find('.btn').toggleClass('btn-default');

    });
    var $activeLanguages = $('#active-languages');
    var $inactiveLanguages = $('#inactive-languages');
    $activeLanguages.sortable();
    $activeLanguages.disableSelection();


    $('#deactivate-language').click(function(event) {
        event.preventDefault();
        var $deactivateModal = $('#deactivate-modal');
        var id = $deactivateModal.attr('data-id');
        var nameEnglish = $deactivateModal.attr('data-name-english');
        var nameOrigin = $deactivateModal.attr('data-name-origin');
        var flag = $deactivateModal.attr('data-flag');
        $activeLanguages.find('li[data-id="' + id + '"]').remove();
        var newLiHtml = '<li class="ui-state-default"' +
                'data-id="' + id + '"' +
                'data-name-english="' + nameEnglish +  '"' +
                'data-name-origin="'  + nameOrigin +  '"' +
                'data-flag="'  + flag +  '"' +
                'data-is-hidden="0"' +
                'data-is-default="0"' +
                '>'   +
                '<span class="ui-icon ui-icon-arrowthick-2-n-s not-visible"></span>' +
                '<img src="/public/img/flags/' + flag + '" alt="flag">&nbsp;' +
                nameEnglish  +
                '<a href="#" class="glyphicon glyphicon glyphicon-arrow-right activate-language" data-toggle="tooltip" title="' +
                GLOBAL_ADC_ACTIVATE + '"></a>' +
                '</li>'
            ;
        $inactiveLanguages.append(newLiHtml);
        $activeLanguages.sortable();
        $activeLanguages.disableSelection();
        $deactivateModal.modal('hide');
        $('[data-toggle="tooltip"]').tooltip();
    });

    $('#submit-changes').click(function(event) {
        event.preventDefault();
        var $self = $(this);
        $self.text(GLOBAL_ADC_LOADING + '...').attr('disabled', true);
        var data = {
            active: [],
            inactive: []
        };
        $('#active-languages li').each(function() {
            var id = $(this).attr('data-id');
            var nameOrigin = $(this).attr('data-name-origin');
            var isHidden = parseInt($(this).attr('data-is-hidden'));
            var isDefault = parseInt($(this).attr('data-is-default'));
            data.active.push({id: id, nameOrigin: nameOrigin, isHidden : isHidden, isDefault: isDefault});
        });

        $('#inactive-languages li').each(function() {
            var id = $(this).attr('data-id');
            data.inactive.push({id: id});
        });
        $.ajax({
            type: "POST",
            url: GLOBAL_SAVE_LANGUAGES,
            data: {data : data},
            dataType: "json",
            success: function(data) {
                notification(data);
                $self.text(GLOBAL_ADC_SAVE).attr('disabled', false);
            }
        });
    })
});

$(document).on('click', '.activate-language', function(event) {
    event.preventDefault();
    var $li = $(this).closest('li');
    var id = $li.attr('data-id');
    var nameEnglish = $li.attr('data-name-english');
    var nameOrigin = $li.attr('data-name-origin');
    var flag = $li.attr('data-flag');
    $li.hide();
    var newLiHtml = '<li class="ui-state-default"' +
        'data-id="' + id + '"' +
        'data-name-english="' + nameEnglish +  '"' +
        'data-name-origin="'  + nameOrigin +  '"' +
        'data-flag="'  + flag +  '"' +
        'data-is-hidden="0"' +
        'data-is-default="0"' +
        '>'   +
        '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' +
        '<img src="/public/img/flags/' + flag + '" alt="flag">&nbsp;' +
        nameEnglish  +
        '<a href="#" class="glyphicon glyphicon glyphicon-arrow-left deactivate-language" data-toggle="tooltip" title="' +
        GLOBAL_ADC_DEACTIVATE + '"></a>' +
        '<a href="#" class="glyphicon glyphicon-pencil edit-language"></a>' +
        '</li>'
        ;
    $('#active-languages').append(newLiHtml);
    $('#active-languages').sortable();
    $('#active-languages').disableSelection();
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('click', '.deactivate-language', function(event) {
    event.preventDefault();
    var $li = $(this).closest('li');
    var isDefault = parseInt($li.attr('data-is-default'));
    if (isDefault) {
        $('#can-not-remove-default-modal').modal('show');
    } else {
        var id = $li.attr('data-id');
        var nameEnglish = $li.attr('data-name-english');
        var nameOrigin = $li.attr('data-name-origin');
        var flag = $li.attr('data-flag');
        var $deactivateModal = $('#deactivate-modal');
        $deactivateModal.attr('data-id', id).attr('data-name-origin',nameOrigin).attr('data-name-english',nameEnglish).attr('data-flag',flag);
        if ($(this).hasClass('predefined')) {
            $deactivateModal.modal('show');
        } else {
            $('#deactivate-language').trigger('click');
        }
    }
});

$(document).on('click', '.edit-language', function(event) {
    event.preventDefault();
    var $li = $(this).closest('li');
    var isDefault = parseInt($li.attr('data-is-default'));
    var id = $li.attr('data-id');
    var nameOrigin = $li.attr('data-name-origin');
    var isHidden = parseInt($li.attr('data-is-hidden'));
    var $editModal = $('#edit-modal');
    var $originalLanguage = $('#lang-name-original');
    var $isDefaultLanguage = $('#is-default-language');
    var $isHiddenLanguage = $('#is-hidden-language');
    $originalLanguage.val(nameOrigin);
    $isDefaultLanguage.attr('checked', !!isDefault);
    $isHiddenLanguage.attr('checked', !!isHidden);
    if (isDefault) {
        $isDefaultLanguage.attr('disabled', true);
    } else {
        $isDefaultLanguage.attr('disabled', false);
    }
    $editModal.attr('data-id', id);
    $editModal.modal('show');
});
