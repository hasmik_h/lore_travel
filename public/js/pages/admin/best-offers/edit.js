$(function() {
    var $bestOffersForm = $('#best-offers-form');
    $bestOffersForm.validate({
            rules: { image: {
                required: (isEdit) ? false : true
            }},
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            success: function(label) {
                $(label).closest('form').find('.valid').removeClass("invalid");
            },
            errorPlacement: function(error, element) {
            }
        });
});