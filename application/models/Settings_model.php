<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Settings_model extends ADC_Model
{
    function __construct()
    {
        $this->table = TBL_COMMON_SETTINGS;
        $this->translationsTable = TBL_COMMON_SETTINGS_TRANSLATIONS;
        parent::__construct();
    }

    public function getBasicInfo($selectArray)
    {
        foreach ($selectArray['main_table'] as &$mainTableField) {
            $mainTableField = $this->getTable() . '.' . $mainTableField;
        }
        foreach ($selectArray['translations_table'] as &$translationsTableField) {
            $translationsTableField = 'translations.' . $translationsTableField;
        }
        array_push($selectArray['translations_table'], 'translations.iso_code');
        $select = implode(',',$selectArray['main_table']) . ',' . implode(',',$selectArray['translations_table']);
        return $this->db->
        select($select)
            ->from($this->getTable($select))
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
    }

    public function updateMetaTags($selectArray, $post){
        $this->db->trans_start();
        foreach ($post as $postItemKey => $postItemValue) {
            if (in_array($postItemKey, $selectArray['main_table'])) {
                $this->db->update($this->getTable(), array($postItemKey => $postItemValue));
                continue;
            }
            if (is_array($postItemValue)) {
                foreach ($postItemValue as $isoCode => $translation) {
                    $this->db->where('iso_code', $isoCode)
                        ->update($this->getTranslationsTable(), array($postItemKey => $translation));
                }
            }
        }
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function getDefaultMapColors()
    {
        return $this->db->select('color_map_default, color_map_default_hover')
            ->get($this->getTable())->row_array();
    }

    public function getAllSettings()
    {
        return $this->db->get($this->getTable())->row_array();
    }
    public function getAllSettingsByLang($lang)
    {
        return $this->db->select($this->getTable() . '.*,' . '  translations.*')
            ->from($this->getTable())
            ->where('translations.iso_code', $lang)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->row_array();
    }

    public function save($post)
    {
        $this->db->update($this->getTable(), $post);
    }

}