<?php
require_once(APPPATH . 'models/ADC_Model.php');

class User_model extends ADC_Model {

    CONST USER_ROLE_ADMIN = 1;

    function __construct()
    {
        $this->table = TBL_USER;
        parent::__construct();
    }

    public function checkAndLoadUser($username, $password, $role)
    {
        $result = $this->db
            ->where('username', $username)
            ->where('password', md5($password))
            ->where('role', $role)
            ->get($this->getTable())
            ->row_array();

        if (NULL == $result) {
            return false;
        }

        $this->session->set_userdata(array(
            "userIdentity"=>$result
        ));

        return true;

    }

    public function checkAdminIdentity()
    {
        $userIdendity = $this->getUserIdentity();
        if (FALSE == $userIdendity) {
            return FALSE;
        }
        if ($userIdendity['role'] != ROLE_USER_ADMIN) {
            return FALSE;
        }
        return true;
    }

    public function getUserIdentity()
    {
        return $this->session->userdata("userIdentity");
    }

    public function logout()
    {
        $this->session->unset_userdata('userIdentity');
        $this->session->unset_userdata('adminLanguage');
    }

    public function checkUserPassword($password)
    {
        return ($this->getUserIdentity()['password'] == md5($password));
    }

    public function changePassword($newPassword)
    {
        $this->db->where('id',$this->getUserIdentity()['id']);
        $this->db->update($this->getTable(), array('password' => md5($newPassword)));
        $this->checkAndLoadUser($this->getUserIdentity()['username'], $newPassword, ROLE_USER_ADMIN);
    }

}