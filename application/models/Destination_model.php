<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Destination_model extends ADC_Model {
    public function add($continent, $title, $summary, $header, $description, $imgs) {
//        echo 'pre';
//        print_r( $imgs); die;
        $this->db->trans_start();
        $this->db->insert('_destination', array('date' => date("Y-m-d h:i:s"), 'continent' => $continent));
        $id = $this->db->insert_id();
        foreach ($title as $key => $value) {
            $this->db->insert('_destination_translations',
                array(
                    'title' => $title[$key],
                    'iso_code' => $key,
                    'summary' => $summary[$key],
                    'header' => $header[$key],
                    'description' => $description[$key],
                    'parent_id' => $id
                )
            );
        }
        foreach ($imgs as $img) {
            $this->db->insert('_destination_images',
                array(
                    'path' => $img['path'],
                    'name' => $img['name'],
                    'parent_id' => $id
                )
            );
        }
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }

    public function getDestinations($languageIsoCode) {
         $data = $this->db->select("*")
            ->from('_destination_translations AS dt')
            ->join('_destination as d', 'dt.parent_id = d.id')
            ->where('dt.iso_code = ', $languageIsoCode)
            ->get()->result_array();
        $data_imgs = $this->db->select("*")
            ->from('adc_destination_images')
            ->get()->result_array();
        foreach($data as &$res){
            $res['files'] = array();
            foreach($data_imgs as $img){
                if($res['parent_id'] == $img['parent_id']){
                    array_push($res['files'], $img);
                }
            }
        }
//            echo '<pre>';
//            print_r( $data ); die;
        return $data;
    }

    public function getDestinationList() {
         $data = $this->db->select("*")
            ->from('_destination_translations AS d')
            ->where('d.iso_code = ', 'en')
            ->get()->result_array();
        $data_imgs = $this->db->select("*")
            ->from('adc_destination_images')
            ->get()->result_array();
        foreach($data as &$res){
            $res['files'] = array();
            foreach($data_imgs as $img){
                if($res['parent_id'] == $img['parent_id']){
                    array_push($res['files'], $img);
                }
            }
        }
        return $data;
    }

    public function getDestinationById($id) {
        $data = $this->db->select("*")
            ->from('_destination_translations AS dt')
            ->join('_destination as d', 'dt.parent_id = d.id')
            ->where('dt.parent_id = ', $id)
            ->get()->result_array();
        $data_imgs = $this->db->select("*")
            ->from('adc_destination_images')
            ->get()->result_array();
        foreach($data as &$res){
            $res['files'] = array();
            foreach($data_imgs as $img){
                if($res['parent_id'] == $img['parent_id']){
                    array_push($res['files'], $img);
                }
            }
        }
        return $data;
    }

    public function deleteById($id){
        $sql = $this->db->delete('_destination', array('id' => $id));
        return $sql;
    }

    public function deleteImageById($id){
        $sql = $this->db->delete('_destination_images', array('id' => $id));
        return $sql;
    }

    public function edit($continent, $title, $summary, $header, $description, $imgs, $id) {
//        echo '<pre>';
//        var_dump( $id); die;
        $this->db->trans_start();
        $this->db->where('id', $id);
        $a = $this->db->update('_destination', array('date' => date("Y-m-d h:i:s"), 'continent' => $continent));

        foreach ($title as $key => $value) {
            $this->db->where('parent_id', $id);
            $this->db->where('iso_code', $key);
            $this->db->update('_destination_translations',
                array(
                    'title' => $title[$key],
                    'summary' => $summary[$key],
                    'header' => $header[$key],
                    'description' => $description[$key]
                )
            );
        }
        foreach ($imgs as $img) {
            $this->db->insert('_destination_images',
                array(
                    'path' => $img['path'],
                    'name' => $img['name'],
                    'parent_id' => $id
                )
            );
        }
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }


}
