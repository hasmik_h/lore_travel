<?php

class ADC_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    protected function getTable()
    {
        return $this->table;
    }

    protected function getTranslationsTable()
    {
        return isset($this->translationsTable) ? $this->translationsTable : null;
    }

    protected function getAllIds($tableName)
    {
        return $this->db->select('id')->get($tableName)->result_array();
    }

    protected function ifRowExist($tablaName, $where)
    {
        return $this->db->get_where($tablaName,$where)->num_rows();
    }

}