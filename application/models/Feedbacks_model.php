<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Feedbacks_model extends ADC_Model
{


    function __construct()
    {
        $this->table = TBL_FEEDBACKS;
        $this->translationsTable = TBL_FEEDBACK_TRANSLATIONS;
        parent::__construct();
    }

    public function getBasicInfo($id)
    {
        return $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.score, translations.iso_code, translations.content, translations.customer_name')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
    }

    public function getAllFeedbacks($languageIsoCode)
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.score, ' . ' translations.customer_name, translations.content')
            ->from($this->getTable())
            ->where('translations.iso_code', $languageIsoCode)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();
    }

    public function add($score, $text, $customerName)
    {
        $this->db->trans_start();
        $this->db->insert($this->getTable(), array('score' => $score));
        $id = $this->db->insert_id();
        foreach ($text as $key => $value) {
            $this->db->insert($this->getTranslationsTable(),
                array(
                    'parent_id' => $id,
                    'iso_code' => $key,
                    'content' => $value,
                    'customer_name' => $customerName[$key],
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }

    public function update($score, $text, $customerName, $id)
    {
        $this->db->trans_start();
        $this->db->where('id',$id)->update($this->getTable(), array('score' => $score));
        foreach ($text as $key => $value) {
            $this->db->where(array(
                'parent_id' => $id,
                'iso_code' => $key,
            ));
            $this->db->update($this->getTranslationsTable(),
                array(
                    'content' => $value,
                    'customer_name' => $customerName[$key],
                    ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status());
    }

    public function saveOrder($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $this->db->where('id', $ids[$i])->update($this->getTable(), array('order' => ($i+1)));
        }
        return true;
    }

    public function deleteFeedback($id)
    {
         $this->db->where('id',$id)->delete($this->getTable());
    }

}