<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Our_team_model extends ADC_Model
{

    function __construct()
    {
        $this->table = TBL_OUR_TEAM;
        $this->translationsTable = TBL_OUR_TEAM_TRANSLATIONS;
        parent::__construct();
    }

    public function getBasicInfo($id)
    {
        return $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.file,' .$this->getTable() . '.incoming_outgoing,' . $this->getTable() . '.link, translations.iso_code, translations.content, translations.name')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
    }

    public function getAllTeams($languageIsoCode)
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.file,' . $this->getTable() . '.link,' .'  translations.content, translations.name')
            ->from($this->getTable())
            ->where('translations.iso_code', $languageIsoCode)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();
    }

    public function getAllJobsByType($languageIsoCode)
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.file,' . $this->getTable() . '.link,' .'  translations.content, translations.name')
            ->from($this->getTable())
            ->where('translations.iso_code', $languageIsoCode)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();
    }

    public function add($text, $name, $link, $fileName)
    {
        $this->db->trans_start();
        $this->db->insert($this->getTable(), array('file' => $fileName, 'link' => $link));
        $id = $this->db->insert_id();
        foreach ($text as $key => $value) {

            $this->db->insert($this->getTranslationsTable(),
                array(
                    'parent_id' => $id,
                    'iso_code' => $key,
                    'name' => $name[$key],
                    'content' => $value,
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }

    public function update($text, $name, $link, $id, $fileName)
    {
        $this->db->trans_start();
        $updateArray = array(
            'link' => $link,
        );
        if ($fileName != false) {
            $updateArray['file'] = $fileName;
        }
        $this->db->where('id', $id)->update($this->getTable(), $updateArray);
        foreach ($text as $key => $value) {
            $this->db->where(array(
                'parent_id' => $id,
                'iso_code' => $key,
            ));
            $this->db->update($this->getTranslationsTable(),
                array(
                    'content' => $value,
                    'name' => $name[$key],
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status());
    }

    public function saveOrder($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $this->db->where('id', $ids[$i])->update($this->getTable(), array('order' => ($i+1)));
        }
        return true;
    }

    public function deleteBestOffer($id)
    {
         $this->db->where('id',$id)->delete($this->getTable());
    }

    public function getFileNameById($id)
    {
        $res = $this->db->select('file')->where('id', $id)->get($this->getTable())->row_array();
        return $res['file'];
    }
}