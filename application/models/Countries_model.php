<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Countries_model extends ADC_Model
{

    function __construct()
    {
        $this->table = TBL_COUNTRIES;
        $this->translationsTable = TBL_COUNTRY_TRANSLATIONS;
        parent::__construct();
    }

    public function getBasicInfo($id)
    {
        return $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.thumb,' . $this->getTable() . '.slug,' . $this->getTable() . '.flag,' . $this->getTable() . '.alternative_link,' . $this->getTable() . '.color_hover,' . $this->getTable() . '.color_default, translations.iso_code, translations.name, translations.comment_on_hover, translations.comment_on_click')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
    }

    public function getCountryNameBySlug($slug, $lang)
    {
        $res = $this->db->select('id')->from($this->getTable())->where('slug', $slug)->get()->row_array();
        $id = $res['id'];
        $subtype = $this->db->select('subtype')->from(TBL_VISAS)->where('country_id', $id)->get()->row_array();
        $name = $this->db->select('name')->from(TBL_COUNTRY_TRANSLATIONS)->where('iso_code', $lang)
            ->where('parent_id', $id)->get()->row_array();
        return array('subtype' => $subtype['subtype'], 'name' => $name['name']);
    }

    public function getCountryNameByIdAndLang($defaultLanguageIsoCode, $countryId)
    {
        return $this->db->select( 'translations.name')
            ->from($this->getTable())
            ->where('translations.iso_code', $defaultLanguageIsoCode)
            ->where($this->getTable() . '.id', $countryId)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->row_array();
    }
    public function getCountryIdBySlug($slug)
    {
        $res = $this->db->select('id')->from($this->getTable())->where('slug', $slug)->get()->row_array();
        return $res['id'];
    }

    public function getInfo($id, $lang)
    {
        $res = array();
        $res['info'] = $this->db->select( $this->getTable() . '.flag,' . $this->getTable() . '.alternative_link,' . $this->getTable() . '.id,' . $this->getTable() . '.thumb,' . $this->getTable() . '.slug,' .'translations.name, translations.comment_on_hover, translations.comment_on_click')
            ->from($this->getTable())
            ->where('translations.iso_code', $lang)
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->row_array();

        $gallery = $this->db->select(TBL_COUNTRY_GALLERY . '.file, translations.content')
            ->from(TBL_COUNTRY_GALLERY)
            ->where('translations.iso_code', $lang)
            ->where(TBL_COUNTRY_GALLERY . '.country_id', $id)
            ->join(TBL_COUNTRY_GALLERY_TRANSLATIONS . ' AS translations', 'translations.parent_id = ' . TBL_COUNTRY_GALLERY . '.id', 'LEFT')
            ->order_by(TBL_COUNTRY_GALLERY . '.order')
            ->get()->result_array();
        $res['gallery'] = array();
        foreach ($gallery as $image) {
            $current = array('href' => site_url(UPLOAD_PATH_READ . UPLOAD_PATH_COUNTRY_GALLERY . $image['file']));
            if ($image['content'] && !is_null($image['content'])) {
                $current['title'] = $image['content'];
            }
            array_push($res['gallery'], $current);
        }
        $res['has_tours'] = $this->db->where('country_id',$id)->get(TBL_TOUR_COUNTRY)->num_rows();

        $res['has_visa'] = $this->db->where('country_id',$id)->get(TBL_VISAS)->num_rows();
        return $res;
    }

    public function getAllCountries($defaultLanguageIsoCode)
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.flag,  translations.name')
            ->from($this->getTable())
            ->where('translations.iso_code', $defaultLanguageIsoCode)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by('translations.name')
            ->get()->result_array();
    }

    public function getCountriesForSelect($defaultLanguageIsoCode)
    {
        $res = $this->db->select($this->getTable() . '.id, ' . ' translations.name')
            ->from($this->getTable())
            ->where('translations.iso_code', $defaultLanguageIsoCode)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by('translations.name')
            ->get()->result_array();
        $resArray = array(-1 => lang('adc__not_selected'));
        foreach ($res as $row) {
            $resArray[$row['id']] = $row['name'];
        }
        return $resArray;
    }

    public function getCountriesForSelectize($lang, $notArmenia = true)
    {
        $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.flag, translations.name')
            ->from($this->getTable())
            ->where('translations.iso_code', $lang);
           if ($notArmenia) {
               $this->db->where($this->getTable() . '.id <> ', COUNTRY_ID_ARMENIA);
           }
        $res = $this->db->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by('translations.name')
            ->get()->result_array();
        return $res;
    }
    public function add(
        $thumb,
        $flag,
        $slug,
        $colorDefault,
        $colorHover,
        $alternativeLink,
        $name,
        $commentOnHover,
        $commentOnClick
    )
    {
        $updateArray = array(
            'slug' => $slug,
            'color_default' => $colorDefault,
            'color_hover' => $colorHover,
            'alternative_link' => $alternativeLink,
        );
        if ($thumb != false) {
            $updateArray['thumb'] = $thumb;
        }
        if ($flag != false) {
            $updateArray['flag'] = $flag;
        }
        $this->db->insert($this->getTable(), $updateArray);
        $id = $this->db->insert_id();
        foreach ($name as $key => $value) {
            $this->db->insert($this->getTranslationsTable(),
                array(
                    'name' => $value,
                    'comment_on_hover' => $commentOnHover[$key],
                    'comment_on_click' => $commentOnClick[$key],
                    'parent_id' => $id,
                    'iso_code' => $key,
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }

    public function update(
        $thumb,
        $flag,
        $slug,
        $colorDefault,
        $colorHover,
        $alternativeLink,
        $name,
        $commentOnHover,
        $commentOnClick,
        $removeThumb,
        $id
    )
    {
        $this->db->trans_start();
        $updateArray = array(
            'slug' => $slug,
            'color_default' => $colorDefault,
            'color_hover' => $colorHover,
            'alternative_link' => $alternativeLink
        );

        if ($thumb != false) {
            $updateArray['thumb'] = $thumb;
        }
        if ($flag != false) {
            $updateArray['flag'] = $flag;
        }
        if ($removeThumb) {
            $updateArray['thumb'] = null;
        }
        $this->db->where('id', $id)->update($this->getTable(), $updateArray);
        foreach ($name as $key => $value) {
            $this->db->where(array(
                'parent_id' => $id,
                'iso_code' => $key,
            ));
            $this->db->update($this->getTranslationsTable(),
                array(
                    'name' => $value,
                    'comment_on_hover' => $commentOnHover[$key],
                    'comment_on_click' => $commentOnClick[$key],
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status());
    }

    public function saveOrder($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $this->db->where('id', $ids[$i])->update($this->getTable(), array('order' => ($i+1)));
        }
        return true;
    }

    public function deleteCountry($id)
    {
         $this->db->where('id',$id)->delete($this->getTable());
    }

    public function getFileNamesById($id)
    {
        $res = $this->db->select('flag,thumb')->where('id', $id)->get($this->getTable())->row_array();
        return $res;
    }

    public function saveCountryOnMap($id, $elemIndex)
    {
        if ($id == -1) {
            $this->db->where('order_in_elems',$elemIndex)->update($this->getTable(),array('order_in_elems' => -1));
        } else {
            $this->db->where('id',$id)->update($this->getTable(),array('order_in_elems' => $elemIndex));
        }
    }

    public function getAllCountriesThatHaveMapSet()
    {
        $result = $this->db->select('id, order_in_elems, color_default, color_hover')
            ->where('order_in_elems >', -1)
            ->get($this->getTable())
            ->result_array();
        return json_encode($result);
    }

}