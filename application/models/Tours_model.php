<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Tours_model extends ADC_Model
{

    public $tourTypes;
    function __construct()
    {
        $this->table = TBL_TOURS;
        $this->translationsTable = TBL_TOUR_TRANSLATIONS;
        $this->tourTypes = array(
           TOUR_TYPE_INCOMING => lang('adc_incoming'),
           TOUR_TYPE_OUTGOING => lang('adc_outgoing'),
           TOUR_TYPE_AIRTICKETS => 'Airtickets',
           TOUR_TYPE_EDUCATIONAL => lang('adc_educational')
        );
        parent::__construct();
    }


    public function getBasicInfo($id)
    {
        return $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.thumb,' . $this->getTable() . '.slug,' . $this->getTable() . '.main_img,translations.iso_code, translations.name, translations.description, translations.description_long, translations.description_additional')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
    }
    public function getBasicInfoBySlug($slug, $lang)
    {
        $res = $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.TYPE, ' . $this->getTable() . '.main_img,translations.iso_code, translations.name,  translations.description_long, translations.description_additional')
            ->from($this->getTable())
            ->where($this->getTable() . '.slug', $slug)
            ->where('translations.iso_code', $lang)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->row_array();

        $gallery = $this->db->
            select('gallery.file, gallery_translations.content')
            ->from($this->getTable())
            ->join(TBL_TOUR_GALLERY . ' AS gallery', $this->getTable() . '.id = gallery.tour_id', 'INNER')
            ->join(TBL_TOUR_GALLERY_TRANSLATIONS . ' AS gallery_translations', 'gallery.id = gallery_translations.parent_id', 'LEFT')
            ->where($this->getTable() . '.slug', $slug)
            ->where('gallery_translations.iso_code', $lang)
            ->get()->result_array();
        $galleryArray = array();
        foreach($gallery as $image) {
            $current = array('href' => site_url(UPLOAD_PATH_READ . UPLOAD_PATH_TOUR_GALLERY . $image['file']));
            if ($image['content']) {
                $current['title'] = $image['content'];
            }
            array_push($galleryArray, $current);
        }
        return array ('tour' => $res, 'gallery' => $galleryArray);

    }

    public function getTourNameByIdAndLang($defaultLanguageIsoCode, $tourId)
    {
        return $this->db->select( 'translations.name')
            ->from($this->getTable())
            ->where('translations.iso_code', $defaultLanguageIsoCode)
            ->where($this->getTable() . '.id', $tourId)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->row_array();
    }

    public function getAllToursByType($lang, $type)
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.thumb, ' . $this->getTable() . '.main_img,  translations.name')
            ->from($this->getTable())
            ->where('translations.iso_code', $lang)
            ->where($this->getTable() . '.type', $type)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();
    }

    public function getAllToursByTypeAndCountry($lang, $type)
    {
        $res = $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.thumb, ' . $this->getTable() . '.slug, ' . $this->getTable() . '.main_img,  translations.name, translations.description')
            ->from($this->getTable())
            ->where('translations.iso_code', $lang)
            ->where($this->getTable() . '.type', $type)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();

        $tourCountryRel = $this->db->get(TBL_TOUR_COUNTRY)->result_array();
        $tourCountryRelArray = array();
        foreach($tourCountryRel as $tourCountry) {
            if (!isset($tourCountryRelArray[$tourCountry['tour_id']])){
                $tourCountryRelArray[$tourCountry['tour_id']] = array();
            }
            array_push($tourCountryRelArray[$tourCountry['tour_id']], $tourCountry['country_id']);
        }
        return array('tours' => $res, 'tourCountries' => $tourCountryRelArray);
    }

    public function add(
        $type,
        $thumb,
        $mainImg,
        $slug,
        $name,
        $description,
        $descriptionLong,
        $descriptionAdditional,
        $tourCountryRel
    )
    {
        $updateArray = array(
            'type' => $type,
            'slug' => $slug,
            'order' => 0
        );
        if ($thumb != false) {
            $updateArray['thumb'] = $thumb;
        }
        if ($mainImg != false) {
            $updateArray['main_img'] = $mainImg;
        }
        $this->db->insert($this->getTable(), $updateArray);
        $id = $this->db->insert_id();

        if ($tourCountryRel != false) {
            foreach ($tourCountryRel as $tourCountryRelCountryId) {
                $this->db->insert(TBL_TOUR_COUNTRY, array(
                    'tour_id' => $id,
                    'country_id' => $tourCountryRelCountryId
                ));
            }
        }

        foreach ($name as $key => $value) {
            $this->db->insert($this->getTranslationsTable(),
                array(
                    'name' => $value,
                    'description' => $description[$key],
                    'description_long' => $descriptionLong[$key],
                    'description_additional' => $descriptionAdditional[$key],
                    'parent_id' => $id,
                    'iso_code' => $key,
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }

    public function update(
        $thumb,
        $mainImg,
        $slug,
        $name,
        $description,
        $descriptionLong,
        $descriptionAdditional,
        $tourCountryRel,
        $id
    )
    {
        $this->db->trans_start();
        if ($tourCountryRel != false) {
            $this->db->where('tour_id', $id)->delete(TBL_TOUR_COUNTRY);
            foreach ($tourCountryRel as $tourCountryRelCountryId) {
                $this->db->insert(TBL_TOUR_COUNTRY, array(
                    'tour_id' => $id,
                    'country_id' => $tourCountryRelCountryId
                ));
            }
        }
        $updateArray = array(
            'slug' => $slug,
        );

        if ($thumb != false) {
            $updateArray['thumb'] = $thumb;
        }
        if ($mainImg != false) {
            $updateArray['main_img'] = $mainImg;
        }
        $this->db->where('id', $id)->update($this->getTable(), $updateArray);
        foreach ($name as $key => $value) {
            $this->db->where(array(
                'parent_id' => $id,
                'iso_code' => $key,
            ));
            $this->db->update($this->getTranslationsTable(),
                array(
                    'name' => $value,
                    'description' => $description[$key],
                    'description_long' => $descriptionLong[$key],
                    'description_additional' => $descriptionAdditional[$key],
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status());
    }

    public function saveOrder($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $this->db->where('id', $ids[$i])->update($this->getTable(), array('order' => ($i+1)));
        }
        return true;
    }

    public function deleteTour($id)
    {
         $this->db->where('id',$id)->delete($this->getTable());
    }

    public function getFileNamesById($id)
    {
        $res = $this->db->select('main_img,thumb')->where('id', $id)->get($this->getTable())->row_array();
        return $res;
    }

    public function getSelectedCountries($tourId)
    {
        $dbRes = $this->db->select('country_id')->where('tour_id', $tourId)->get(TBL_TOUR_COUNTRY)->result_array();
        $result = array();
        foreach ($dbRes as $row) {
            array_push($result, $row['country_id']);
        }
        return $result;
    }


}