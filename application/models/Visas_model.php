<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Visas_model extends ADC_Model
{


    function __construct()
    {
        $this->table = TBL_VISAS;
        $this->translationsTable = TBL_VISA_TRANSLATIONS;
        parent::__construct();
    }


    public function mapType($type)
    {
        $typeArray = array(
            VISA_TYPE_TO_ARMENIA => lang('adc_to_armenia'),
            VISA_TYPE_FROM_ARMENIA => lang('adc_from_armenia'),
        );
        return $typeArray[$type];
    }

    public function mapSubtype($subtype)
    {
        $subTypeArray = array(
            VISA_SUBTYPE_WITHOUT => lang('adc_without_visa'),
            VISA_SUBTYPE_BEFORE => lang('adc_visa_before_trip')
        );
        return $subTypeArray[$subtype];
    }

    public function getBasicInfo($id)
    {
        return $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.country_id, ' . $this->getTable() . '.file, translations.iso_code, translations.content,  translations.country')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
    }

    public function getAllVisasByTypeAndSubtype($lang, $type, $subtype)
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.file,  translations.content,  translations.country')
            ->from($this->getTable())
            ->where('translations.iso_code', $lang)
            ->where($this->getTable() . '.type', $type)
            ->where($this->getTable() . '.subtype', $subtype)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();
    }

    public function getAllVisasByType($lang, $type)
    {
        $res = $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.order, ' . $this->getTable() . '.country_id, ' . $this->getTable() . '.subtype, ' . $this->getTable() . '.file,  translations.content,   translations.country')
            ->from($this->getTable())
            ->where('translations.iso_code', $lang)
            ->where($this->getTable() . '.type', $type)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();

        $resArray = array(
            VISA_SUBTYPE_BEFORE => array(),
            VISA_SUBTYPE_WITHOUT => array(),
        );

        foreach ($res as $row) {
            array_push($resArray[$row['subtype']], $row);
        }

        return $resArray;
    }

    public function add($text, $country, $countryId, $type, $subtype, $fileName)
    {
        $this->db->trans_start();
        $this->db->insert($this->getTable(), array('file' => $fileName, 'type' => $type, 'subtype' => $subtype, 'country_id' => $countryId));
        $id = $this->db->insert_id();
        foreach ($text as $key => $value) {
            $this->db->insert($this->getTranslationsTable(),
                array(
                    'parent_id' => $id,
                    'iso_code' => $key,
                    'content' => $value,
                    'country' => $country[$key],
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }

    public function update($text,$country, $countryId,  $id, $fileName)
    {
        $this->db->trans_start();
        $this->db->where('id', $id)->update($this->getTable(), array('country_id' => $countryId));

        if ($fileName != false) {
            $this->db->where('id', $id)->update($this->getTable(), array('file' => $fileName));
        }
        foreach ($text as $key => $value) {
            $this->db->where(array(
                'parent_id' => $id,
                'iso_code' => $key,
            ));
            $this->db->update($this->getTranslationsTable(),
                array(
                    'content' => $value,
                    'country' => $country[$key],

                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status());
    }

    public function saveOrder($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $this->db->where('id', $ids[$i])->update($this->getTable(), array('order' => ($i+1)));
        }
        return true;
    }

    public function deleteVisa($id)
    {
         $this->db->where('id',$id)->delete($this->getTable());
    }

    public function getFileNameById($id)
    {
        $res = $this->db->select('file')->where('id', $id)->get($this->getTable())->row_array();
        return $res['file'];
    }
}