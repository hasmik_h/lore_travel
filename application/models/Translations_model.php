<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Translations_model extends ADC_Model
{

    function __construct()
    {
        $this->table = TBL_UNIVERSAL_TEXT;
        $this->translationsTable = TBL_UNIVERSAL_TEXT_TRANSLATIONS;
        parent::__construct();
    }

    public function getAllTranslations()
    {
        return $this->db->
        select($this->getTable() . '.id, translations.iso_code, translations.content')
            ->from($this->getTable())
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by('translations.parent_id')
            ->get()->result_array();
    }

    public function preloadTranslations($ids, $lang)
    {
        $res = $this->db->
        select($this->getTable() . '.id, translations.content')
            ->from($this->getTable())
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->where_in($this->getTable() . '.id',$ids)
            ->where('translations.iso_code', $lang)
            ->get()->result_array();
        $resArray = array();
        foreach ($res as $row) {
            $resArray[$row['id']] = $row['content'];
        }
        return $resArray;
    }

    public function getById($id)
    {
        return $this->db->
        select('translations.iso_code, translations.content')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by('translations.parent_id')
            ->get()->result_array();
    }

    public function save($id, $content)
    {
        foreach ($content as $lang => $text) {
            $this->db->where('parent_id', $id)->where('iso_code', $lang)
                ->update($this->getTranslationsTable(), array('content' => $text));
        }
    }

}