<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Country_gallery_model extends ADC_Model
{


    function __construct()
    {
        $this->table = TBL_COUNTRY_GALLERY;
        $this->translationsTable = TBL_COUNTRY_GALLERY_TRANSLATIONS;
        parent::__construct();
    }

    public function getBasicInfo($id)
    {
        return $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.file, translations.iso_code, translations.content')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
    }

    public function getImagesForCountry($defaultLanguageIsoCode, $countryId)
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.file,  translations.content')
            ->from($this->getTable())
            ->where('translations.iso_code', $defaultLanguageIsoCode)
            ->where($this->getTable() . '.country_id', $countryId)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();
    }

    public function add($text, $fileName, $countryId)
    {
        $this->db->trans_start();
        $this->db->insert($this->getTable(), array('file' => $fileName, 'country_id' => $countryId));
        $id = $this->db->insert_id();
        foreach ($text as $key => $value) {
            $this->db->insert($this->getTranslationsTable(),
                array(
                    'parent_id' => $id,
                    'iso_code' => $key,
                    'content' => $value,
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }

    public function update($text, $id, $fileName)
    {
        $this->db->trans_start();
        if ($fileName != false) {
            $updateArray['file'] = $fileName;
            $this->db->where('id', $id)->update($this->getTable(), $updateArray);
        }
        foreach ($text as $key => $value) {
            $this->db->where(array(
                'parent_id' => $id,
                'iso_code' => $key,
            ));
            $this->db->update($this->getTranslationsTable(),
                array(
                    'content' => $value,
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status());
    }

    public function saveOrder($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $this->db->where('id', $ids[$i])->update($this->getTable(), array('order' => ($i+1)));
        }
        return true;
    }

    public function deleteCountryGalleryImage($id)
    {
         $this->db->where('id',$id)->delete($this->getTable());
    }

    public function deleteCountryGalleryImagesByCountryId($countryId)
    {
        $this->db->where('country_id',$countryId)->delete($this->getTable());
    }

    public function getFileNameById($id)
    {
        $res = $this->db->select('file')->where('id', $id)->get($this->getTable())->row_array();
        return $res['file'];
    }

    public function getFilesByCountryId($countryId)
    {
       return $this->db->select('file')->where('country_id', $countryId)
           ->get($this->getTable())->result_array();
    }
}