<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Partners_model extends ADC_Model
{

    function __construct()
    {
        $this->table = TBL_PARTNERS;
        parent::__construct();
    }

    public function getBasicInfo($id)
    {
        return $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.file,' .$this->getTable() . '.simple_educational,' . $this->getTable() . '.link')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->get()->result_array();
    }

    public function getAllPartners()
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.file,' . $this->getTable() . '.link,')
            ->from($this->getTable())
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();
    }

    public function getAllPartnersByType( $type)
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.file,' . $this->getTable() . '.link,')
            ->from($this->getTable())
            ->where($this->getTable() . '.simple_educational', $type)
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();
    }

    public function add($link, $simpleEducational, $fileName)
    {
        $this->db->trans_start();
        $this->db->insert($this->getTable(), array('file' => $fileName, 'link' => $link, 'simple_educational' => $simpleEducational));
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }

    public function update($link, $simpleEducational, $id, $fileName)
    {
        $this->db->trans_start();
        $updateArray = array(
            'link' => $link,
            'simple_educational' => $simpleEducational
        );
        if ($fileName != false) {
            $updateArray['file'] = $fileName;
        }
        $this->db->where('id', $id)->update($this->getTable(), $updateArray);
        $this->db->trans_complete();
        return ($this->db->trans_status());
    }

    public function saveOrder($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $this->db->where('id', $ids[$i])->update($this->getTable(), array('order' => ($i+1)));
        }
        return true;
    }

    public function deletePartner($id)
    {
         $this->db->where('id',$id)->delete($this->getTable());
    }

    public function getFileNameById($id)
    {
        $res = $this->db->select('file')->where('id', $id)->get($this->getTable())->row_array();
        return $res['file'];
    }
}