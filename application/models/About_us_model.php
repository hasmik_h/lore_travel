<?php
require_once(APPPATH . 'models/ADC_Model.php');

class About_us_model extends ADC_Model
{

    public $translations;
    function __construct()
    {
        $this->table = TBL_ABOUT_US;
        $this->translationsTable = TBL_ABOUT_US_TRANSLATIONS;
        $this->translations = array(
            ABOUT_US_GLOBE => 'Globe Travel',
            ABOUT_US_CARLSON_WAGONLIT => 'Carlson Wagonlit',
            ABOUT_US_HOTELS_PRO => 'Hotels Pro',
            ABOUT_US_WHY_US => lang('adc_why_us')
        );
        parent::__construct();
    }

    public function getBasicInfo($id)
    {
        return $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.file,translations.iso_code, translations.content')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
    }
    public function update(
        $file,
        $content,
        $id
    )
    {
        $this->db->trans_start();
        if ($file != false) {
            $this->db->where('id', $id)->update($this->getTable(), array('file' => $file));
        }
        foreach ($content as $key => $value) {
            $this->db->where(array(
                'parent_id' => $id,
                'iso_code' => $key,
            ));
            $this->db->update($this->getTranslationsTable(),
                array(
                    'content' => $value,
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status());
    }

    public function getAll($lang)
    {
        $res = $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.file,translations.iso_code, translations.content')
            ->where('translations.iso_code', $lang)
            ->from($this->getTable())
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
        $resArray = array();
        foreach ($res as $row) {
            $resArray[$row['id']] = $row;
        }
        return $resArray;
    }

}