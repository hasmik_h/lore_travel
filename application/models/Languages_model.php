<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Languages_model extends ADC_Model {

    CONST USER_ROLE_ADMIN = 1;

    function __construct()
    {
        $this->table = TBL_LANGUAGES;
        parent::__construct();
    }

    public function setLanguageForAdmin()
    {
        $result = $this->db
            ->where('is_admin', 1)
            ->get($this->getTable())
            ->row_array();

        if (NULL == $result) {
            return false;
        }

        $this->session->set_userdata(array(
            "adminLanguage"=>
                array(
                    'isoCode' => $result['iso_code'],
                    'nameEnglish' => strtolower($result['name_english'])
                )
        ));

        return true;
    }

    public function getAdminLanguage()
    {
        return $this->session->userdata('adminLanguage');
    }
    public function getAdminLanguageIsoCode()
    {
        return $this->session->userdata('adminLanguage')['isoCode'];
    }

    public function getAdminLanguageNameEnglish()
    {
        return $this->session->userdata('adminLanguage')['nameEnglish'];
    }

    public function getAdminLanguages()
    {
       return $this->db->where('may_be_admin',1)
            ->get($this->getTable())
            ->result_array();

    }

    public function changeAdminLanguage($languageId)
    {
        $this->db->update($this->getTable(), array('is_admin' => 0));
        $this->db->where('id', $languageId)
            ->update($this->getTable(), array('is_admin' => 1));
        $this->setLanguageForAdmin();
    }

    public function getActiveLanguages($onlyVisibles = false)
    {
         $this->db->where('is_active',1);
        if ($onlyVisibles) {
            $this->db->where('is_hidden',0);
        }
        return $this->db->order_by('order')
                        ->get($this->getTable())
                        ->result_array();
    }

    public function getActiveAndInactiveLanguages()
    {
        $result = $this->db->get($this->getTable())->result_array();
        $activeLanguages = array();
        $inactiveLanguages = array();
        foreach ($result as $row) {
            if ($row['is_active']) {
                array_push($activeLanguages, $row);
            } else {
                array_push($inactiveLanguages, $row);
            }
        }

        //sorting Active Languages by "order" filed
        usort($activeLanguages, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        return array (
            'active' => $activeLanguages,
            'inactive' => $inactiveLanguages
        );
    }

    public function saveLanguages($languages)
    {
        $activeLanguages   = $languages['active'];
        $inactiveLanguages = $languages['inactive'];
        $activeLanguageIsoCodes = array();
        $inactiveLanguageIsoCodes = array();

        foreach ($activeLanguages as $activeLanguage) {
            $activeLanguageIsoCodes[$activeLanguage['id']] = $this->getLanguageIsoCodeById($activeLanguage['id']);
        }

        foreach ($inactiveLanguages as $inactiveLanguage) {
            $inactiveLanguageIsoCodes[$inactiveLanguage['id']] = $this->getLanguageIsoCodeById($inactiveLanguage['id']);
        }


        $tables = $this->getMainAndTranslationLanguagesPairs();

        //deleting inactive languages info

        foreach ($tables as $translationTable) {
            foreach ($inactiveLanguageIsoCodes as $inactiveLanguageIsoCode) {
                $this->db->where('iso_code', $inactiveLanguageIsoCode)
                    ->delete($translationTable);
            }
        }

        //filling all translation tables
        //with active languages translations
        //if they are missing

        foreach ($tables as $mainTable => $translationTable) {
            //getting table's fields to clone
            $fields = $this->db->list_fields($translationTable);
            //removing id,parent_id and iso_code fields
            unset ($fields['id']);
            unset ($fields['parent_id']);
            unset ($fields['iso_code']);
            $fieldsToInsertEmpty = array();

            foreach ($fields as $field) {
                $fieldsToInsertEmpty[$field] = '';
            }
            foreach ($activeLanguageIsoCodes as $activeLanguageIsoCode) {
                $parentTableIds = $this->getAllIds($mainTable);
                foreach ($parentTableIds as $parentTableId) {
                    $ifExists = $this->ifRowExist(
                        $translationTable,
                        array(
                            'parent_id' => $parentTableId['id'],
                            'iso_code'  => $activeLanguageIsoCode
                        )
                    );
                    if ($ifExists) {
                        continue;
                    }
                    $fieldsToInsert = $fieldsToInsertEmpty;
                    $fieldsToInsert['parent_id'] = $parentTableId['id'];
                    $fieldsToInsert['iso_code'] = $activeLanguageIsoCode;
                    $this->db->insert($translationTable, $fieldsToInsert);
                }
            }
        }


            //making changes in languages table itself
        $this->db->update($this->getTable(), array(
            'is_default' => 0,
            'is_hidden' => 0,
            'is_active' => 0,
            'order' => 0
        ));

        foreach ($activeLanguages as $key => $activeLanguage) {
            $this->db->where('id',$activeLanguage['id'])
                ->update($this->getTable(),
                array(
                    'is_hidden' => $activeLanguage['isHidden'],
                    'name_origin' => $activeLanguage['nameOrigin'],
                    'is_default' => $activeLanguage['isDefault'],
                    'is_active' => 1,
                    'order' => $key
                ));
        }

        $this->db->trans_complete();

        return  ($this->db->trans_status());
    }

    public function getActiveLanguagesIsoCodes()
    {
        return $this->db->select('iso_code')
            ->where('is_active',1)
            ->get($this->getTable())
            ->result_array();
    }

    public function getDefaultLanguageIsoCode()
    {
        $defaultLang = $this->db->select('iso_code')
            ->where('is_active',1)
            ->where('is_default',1)
            ->get($this->getTable())
            ->row_array();
        return $defaultLang['iso_code'];
    }

    protected function getMainAndTranslationLanguagesPairs()
    {
        return array(
            TBL_UNIVERSAL_TEXT => TBL_UNIVERSAL_TEXT_TRANSLATIONS,
            TBL_SLIDER => TBL_SLIDER_TRANSLATIONS,
            TBL_COUNTRIES => TBL_COUNTRY_TRANSLATIONS,
            TBL_COUNTRY_GALLERY => TBL_COUNTRY_GALLERY_TRANSLATIONS,
            TBL_TOURS => TBL_TOUR_TRANSLATIONS,
            TBL_TOUR_GALLERY => TBL_TOUR_GALLERY_TRANSLATIONS,
            TBL_BEST_OFFERS => TBL_BEST_OFFER_TRANSLATIONS,
            TBL_FEEDBACKS => TBL_FEEDBACK_TRANSLATIONS,
            TBL_COMMON_SETTINGS => TBL_COMMON_SETTINGS_TRANSLATIONS,
            TBL_ABOUT_US => TBL_ABOUT_US_TRANSLATIONS,
            TBL_VISAS => TBL_VISA_TRANSLATIONS,
        );
    }

    protected function getLanguageIsoCodeById($id)
    {
        $lang = $this->db->select('iso_code')
        ->where('id', $id)
            ->get($this->getTable())
            ->row_array();
        return $lang['iso_code'];
    }


}