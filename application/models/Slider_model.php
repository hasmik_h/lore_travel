<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Slider_model extends ADC_Model
{


    function __construct()
    {
        $this->table = TBL_SLIDER;
        $this->translationsTable = TBL_SLIDER_TRANSLATIONS;
        parent::__construct();
    }

    public function getBasicInfo($id)
    {
        return $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.file,' . $this->getTable() . '.link, translations.iso_code, translations.content, translations.link_text, translations.title')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
    }

    public function getAllSlides($languageIsoCode)
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.file,  translations.title, ' . $this->getTable() . '.link, translations.content, translations.link_text')
            ->from($this->getTable())
            ->where('translations.iso_code', $languageIsoCode)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();
    }

    public function add($text, $title, $linkText, $link, $fileName)
    {
        $this->db->trans_start();
        $this->db->insert($this->getTable(), array('file' => $fileName, 'link' => $link));
        $id = $this->db->insert_id();
        foreach ($text as $key => $value) {
            $this->db->insert($this->getTranslationsTable(),
                array(
                    'parent_id' => $id,
                    'iso_code' => $key,
                    'content' => $value,
                    'link_text' => $linkText[$key],
                    'title' => $title[$key],
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }

    public function update($text, $title, $linkText, $link, $id, $fileName)
    {
        $this->db->trans_start();
        $updateArray = array(
            'link' => $link
        );
        if ($fileName != false) {
            $updateArray['file'] = $fileName;
        }
        $this->db->where('id', $id)->update($this->getTable(), $updateArray);
        foreach ($text as $key => $value) {
            $this->db->where(array(
                'parent_id' => $id,
                'iso_code' => $key,
            ));
            $this->db->update($this->getTranslationsTable(),
                array(
                    'content' => $value,
                    'link_text' => $linkText[$key],
                    'title' => $title[$key],
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status());
    }

    public function saveOrder($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $this->db->where('id', $ids[$i])->update($this->getTable(), array('order' => ($i+1)));
        }
        return true;
    }

    public function deleteSlide($id)
    {
         $this->db->where('id',$id)->delete($this->getTable());
    }

    public function getFileNameById($id)
    {
        $res = $this->db->select('file')->where('id', $id)->get($this->getTable())->row_array();
        return $res['file'];
    }
}