<?php
require_once(APPPATH . 'models/ADC_Model.php');

class Tour_gallery_model extends ADC_Model
{


    function __construct()
    {
        $this->table = TBL_TOUR_GALLERY;
        $this->translationsTable = TBL_TOUR_GALLERY_TRANSLATIONS;
        parent::__construct();
    }

    public function getBasicInfo($id)
    {
        return $this->db->
        select($this->getTable() . '.id, ' . $this->getTable() . '.file, translations.iso_code, translations.content')
            ->from($this->getTable())
            ->where($this->getTable() . '.id', $id)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->get()->result_array();
    }

    public function getImagesForTour($defaultLanguageIsoCode, $tourId)
    {
        return $this->db->select($this->getTable() . '.id, ' . $this->getTable() . '.file,  translations.content')
            ->from($this->getTable())
            ->where('translations.iso_code', $defaultLanguageIsoCode)
            ->where($this->getTable() . '.tour_id', $tourId)
            ->join($this->getTranslationsTable() . ' AS translations', 'translations.parent_id = ' . $this->getTable() . '.id', 'LEFT')
            ->order_by($this->getTable() . '.order')
            ->get()->result_array();
    }

    public function add($text, $fileName, $tourId)
    {
        $this->db->trans_start();
        $this->db->insert($this->getTable(), array('file' => $fileName, 'tour_id' => $tourId));
        $id = $this->db->insert_id();
        foreach ($text as $key => $value) {
            $this->db->insert($this->getTranslationsTable(),
                array(
                    'parent_id' => $id,
                    'iso_code' => $key,
                    'content' => $value,
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status()) ? $id : false;
    }

    public function update($text, $id, $fileName)
    {
        $this->db->trans_start();
        if ($fileName != false) {
            $updateArray['file'] = $fileName;
            $this->db->where('id', $id)->update($this->getTable(), $updateArray);
        }
        foreach ($text as $key => $value) {
            $this->db->where(array(
                'parent_id' => $id,
                'iso_code' => $key,
            ));
            $this->db->update($this->getTranslationsTable(),
                array(
                    'content' => $value,
                ));
        }
        $this->db->trans_complete();
        return ($this->db->trans_status());
    }

    public function saveOrder($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $this->db->where('id', $ids[$i])->update($this->getTable(), array('order' => ($i+1)));
        }
        return true;
    }

    public function deleteTourGalleryImage($id)
    {
         $this->db->where('id',$id)->delete($this->getTable());
    }

    public function deleteTourGalleryImagesByTourId($tourId)
    {
        $this->db->where('tour_id',$tourId)->delete($this->getTable());
    }

    public function getFileNameById($id)
    {
        $res = $this->db->select('file')->where('id', $id)->get($this->getTable())->row_array();
        return $res['file'];
    }

    public function getFilesByTourId($tourId)
    {
       return $this->db->select('file')->where('tour_id', $tourId)
           ->get($this->getTable())->result_array();
    }
}