<div class="panel panel-default">
    <div class="panel-heading">
        Partners
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'partners-form',
            ));?>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="radio-inline">
                        <input type="radio" name="simple_educational" value="<?php echo PARTNER_TYPE_SIMPLE?>" <?php  if(isset($id) && $simple_educational == PARTNER_TYPE_SIMPLE) {echo 'checked=""';}?>>Simple
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="simple_educational"  value="<?php echo PARTNER_TYPE_EDUCATIONAL?>" <?php  if(isset($id) && $simple_educational == PARTNER_TYPE_EDUCATIONAL) {echo 'checked=""';}?>>Educational
                    </label>
                </div>
                <?php if (isset($id)) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                        <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_PARTNERS . $file)?>" class="img-responsive img-thumbnail">
                        </div>
                    </div>

                <?php } ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_image');?></label>
                        <?php echo form_upload(array(
                            'class' => 'form-control',
                            'name'  => 'image',
                            'id'  => 'image',
                        ));?>
                    </div>

                <div class="form-group">
                    <label><?php echo lang('adc_link');?> <?php echo requiredAsterisk();?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'link',
                        'value' => isset($id) ? $link : '',
                    ));?>
                </div>
                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>


            </div>
            <!-- /.col-lg-6 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->