<div class="panel panel-default">
    <div class="panel-heading">
        Partners
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            &nbsp;
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            <?php echo createSortableULForPartners($partners);?>
                        </p>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo site_url('admin_partners/edit')?>" class="btn btn-primary pull-right"><?php echo lang('adc_add')?></a>
                <button type="submit" class="btn btn-primary pull-right margin-right-5" id="submit-changes"><?php echo lang('adc_save')?></button>
            </div>
        </div>
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->


<div id="remove-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-danger text-danger">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p><?php echo lang('adc_are_you_sure')?></p>
            </div>
            <div class="modal-footer">
                <?php echo form_button(array('content' => lang('adc_close'), 'type' => 'button', 'class' => 'btn btn-default', 'data-dismiss' => 'modal'));?>
                <?php echo form_button(array('content' => lang('adc_yes'), 'type' => 'submit', 'class' => 'btn btn-primary', 'id' => 'delete-partner'));?>
            </div>
        </div>

    </div>
</div>
