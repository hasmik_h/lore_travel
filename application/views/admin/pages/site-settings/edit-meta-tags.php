<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo $pageTitle;?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'meta-tags-form',
            ));?>
            <div class="col-lg-6">
                <?php foreach ($activeLanguagesIsoCodes as $activeLanguagesIsoCode) { ?>
                    <?php if ($slug == SLUG_HOME) { ?>
                        <div class="form-group">
                            <label><?php echo lang('adc_meta_keywords');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo requiredAsterisk();?>
                                <?php echo  explanationSign(lang('adc_meta_keywords_explanation'), 'right');?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'required' => 'required',
                                'name'  => transformInputLanguage('meta_keywords_home_page',$activeLanguagesIsoCode),
                                'value' =>  $meta_keywords_home_page[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('adc_meta_description');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo requiredAsterisk();?>
                                <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'required' => 'required',
                                'name'  => transformInputLanguage('meta_description_home_page',$activeLanguagesIsoCode),
                                'value' =>  $meta_description_home_page[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('adc_meta_og_title');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_og_title_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('home_page_og_title',$activeLanguagesIsoCode),
                                'value' =>  $home_page_og_title[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('adc_meta_og_description');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_og_description_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('home_page_og_description',$activeLanguagesIsoCode),
                                'value' =>  $home_page_og_description[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                    <?php } elseif ($slug == SLUG_ABOUT_US) { ?>
                        <div class="form-group">
                            <label><?php echo lang('adc_meta_keywords');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_keywords_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_keywords_about_us',$activeLanguagesIsoCode),
                                'value' =>  $meta_keywords_about_us[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('adc_meta_description');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_description_about_us',$activeLanguagesIsoCode),
                                'value' =>  $meta_description_about_us[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                    <?php } elseif ($slug == SLUG_INCOMING_TOURS) { ?>
                        <div class="form-group">
                            <label><?php echo lang('adc_meta_keywords');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_keywords_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_keywords_tours_incoming',$activeLanguagesIsoCode),
                                'value' =>  $meta_keywords_tours_incoming[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('adc_meta_description');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_description_tours_incoming',$activeLanguagesIsoCode),
                                'value' =>  $meta_description_tours_incoming[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                    <?php } elseif ($slug == SLUG_OUTGOING_TOURS) { ?>
                        <div class="form-group">
                            <label><?php echo lang('adc_meta_keywords');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_keywords_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_keywords_tours_outgoing',$activeLanguagesIsoCode),
                                'value' =>  $meta_keywords_tours_outgoing[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('adc_meta_description');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_description_tours_outgoing',$activeLanguagesIsoCode),
                                'value' =>  $meta_description_tours_outgoing[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                    <?php } elseif ($slug == SLUG_EDUCATIONAL_TOURS) { ?>
                        <div class="form-group">
                            <label><?php echo lang('adc_meta_keywords');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_keywords_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_keywords_tours_education',$activeLanguagesIsoCode),
                                'value' =>  $meta_keywords_tours_education[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('adc_meta_description');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_description_tours_education',$activeLanguagesIsoCode),
                                'value' =>  $meta_description_tours_education[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                    <?php } elseif ($slug == SLUG_DESTINATION) { ?>
                        <div class="form-group">
                            <label><?php echo lang('adc_meta_keywords');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_keywords_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_keywords_interactive_map',$activeLanguagesIsoCode),
                                'value' =>  $meta_keywords_destination[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('adc_meta_description');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_description_interactive_map',$activeLanguagesIsoCode),
                                'value' =>  $meta_description_interactive_map[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>


                <?php } elseif ($slug == SLUG_BOOK_HOTEL) { ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_meta_keywords');?>
                            <?php echo translateLanguage($activeLanguagesIsoCode);?>
                            <?php echo  explanationSign(lang('adc_meta_keywords_explanation'));?>
                        </label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control',
                            'rows' => 3,
                            'name'  => transformInputLanguage('meta_keywords_book_hotel',$activeLanguagesIsoCode),
                            'value' =>  $meta_keywords_book_hotel[$activeLanguagesIsoCode['iso_code']]
                        ));?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('adc_meta_description');?>
                            <?php echo translateLanguage($activeLanguagesIsoCode);?>
                            <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                        </label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control',
                            'rows' => 3,
                            'name'  => transformInputLanguage('meta_description_book_hotel',$activeLanguagesIsoCode),
                            'value' =>  $meta_description_book_hotel[$activeLanguagesIsoCode['iso_code']]
                        ));?>
                    </div>

                <?php } elseif ($slug == SLUG_VISAS_TO_ARMENIA) { ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_meta_keywords');?>
                            <?php echo translateLanguage($activeLanguagesIsoCode);?>
                            <?php echo  explanationSign(lang('adc_meta_keywords_explanation'));?>
                        </label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control',
                            'rows' => 3,
                            'name'  => transformInputLanguage('meta_keywords_visas_to_armenia',$activeLanguagesIsoCode),
                            'value' =>  $meta_keywords_visas_to_armenia[$activeLanguagesIsoCode['iso_code']]
                        ));?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('adc_meta_description');?>
                            <?php echo translateLanguage($activeLanguagesIsoCode);?>
                            <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                        </label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control',
                            'rows' => 3,
                            'name'  => transformInputLanguage('meta_description_visas_to_armenia',$activeLanguagesIsoCode),
                            'value' =>  $meta_description_visas_to_armenia[$activeLanguagesIsoCode['iso_code']]
                        ));?>
                    </div>

                    <?php } elseif ($slug == SLUG_VISAS_FROM_ARMENIA) { ?>
                        <div class="form-group">
                            <label><?php echo lang('adc_meta_keywords');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_keywords_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_keywords_visas_from_armenia',$activeLanguagesIsoCode),
                                'value' =>  $meta_keywords_visas_from_armenia[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('adc_meta_description');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_description_visas_from_armenia',$activeLanguagesIsoCode),
                                'value' =>  $meta_description_visas_from_armenia[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                    <?php } elseif ($slug == SLUG_NOT_DECIDED) { ?>
                        <div class="form-group">
                            <label><?php echo lang('adc_meta_keywords');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_keywords_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_keywords_not_decided',$activeLanguagesIsoCode),
                                'value' =>  $meta_keywords_not_decided[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('adc_meta_description');?>
                                <?php echo translateLanguage($activeLanguagesIsoCode);?>
                                <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                            </label>
                            <?php echo form_textarea(array(
                                'class' => 'form-control',
                                'rows' => 3,
                                'name'  => transformInputLanguage('meta_description_not_decided',$activeLanguagesIsoCode),
                                'value' =>  $meta_description_not_decided[$activeLanguagesIsoCode['iso_code']]
                            ));?>
                        </div>


                <?php } elseif ($slug == SLUG_CONTACT_US) { ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_meta_keywords');?>
                            <?php echo translateLanguage($activeLanguagesIsoCode);?>
                            <?php echo  explanationSign(lang('adc_meta_keywords_explanation'));?>
                        </label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control',
                            'rows' => 3,
                            'name'  => transformInputLanguage('meta_keywords_contact_us',$activeLanguagesIsoCode),
                            'value' =>  $meta_keywords_contact_us[$activeLanguagesIsoCode['iso_code']]
                        ));?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('adc_meta_description');?>
                            <?php echo translateLanguage($activeLanguagesIsoCode);?>
                            <?php echo  explanationSign(lang('adc_meta_description_explanation'));?>
                        </label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control',
                            'rows' => 3,
                            'name'  => transformInputLanguage('meta_description_contact_us',$activeLanguagesIsoCode),
                            'value' =>  $meta_description_contact_us[$activeLanguagesIsoCode['iso_code']]
                        ));?>
                    </div>

                <?php } ?>

                    <hr>
              <?php } ?>

            </div>

            <div class="col-lg-6">
                <?php if ($slug == SLUG_HOME) { ?>
                <?php if (isset($home_page_og_image)) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                        <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_SETTINGS . $home_page_og_image)?>" class="img-responsive img-thumbnail">
                        </div>
                    </div>

                <?php } ?>
                    <div class="form-group">
                        <label>
                            <?php echo lang('adc_meta_og_image') . ' (' . lang('adc_width') . '=' . lang('adc_height') . ')';?>
                            <?php echo  explanationSign(lang('adc_meta_og_image_explanation'));?>
                        </label>
                        <?php echo form_upload(array(
                            'class' => 'form-control',
                            'name'  => 'home_page_og_image',
                            'id'  => 'home_page_og_image',
                        ));?>
                    </div>

                <div class="form-group">
                    <label>
                        <?php echo lang('adc_meta_og_type');?>
                        <?php echo  explanationSign(lang('adc_meta_og_type_explanation'));?>
                    </label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'home_page_og_type',
                        'value' =>  $home_page_og_type
                    ));?>
                </div>

                <div class="form-group">
                    <label>
                        <?php echo lang('adc_meta_og_url');?>
                        <?php echo  explanationSign(lang('adc_meta_og_url_explanation'));?>
                    </label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'home_page_og_url',
                        'value' =>  $home_page_og_url
                    ));?>
                </div>
                <?php } ?>
                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>


            </div>
            <!-- /.col-lg-6 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->