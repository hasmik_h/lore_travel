<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_common_settings');?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'common-settings-form',
            ));?>

            <div class="col-lg-12">
                <div class="form-group">
                    <label><?php echo lang('adc_mail_contact_us');?> <?php echo requiredAsterisk(); ?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'mail_contact_us',
                        'value'  => $mail_contact_us,
                    ));?>
                </div>
                <div class="form-group">
                    <label><?php echo lang('adc_mail_lets_plan_your_trip');?> <?php echo requiredAsterisk(); ?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'mail_lets_plan_your_trip',
                        'value'  => $mail_lets_plan_your_trip,
                    ));?>
                </div>
                <div class="form-group">
                    <label><?php echo lang('adc_mail_order_tour');?> <?php echo requiredAsterisk(); ?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'mail_order_tour',
                        'value'  => $mail_order_tour,
                    ));?>
                </div>
                <div class="form-group">
                    <label><?php echo lang('adc_url_facebook');?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'url_facebook',
                        'value'  => $url_facebook,
                    ));?>
                </div>
                <div class="form-group">
                    <label><?php echo lang('adc_url_google_plus');?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'url_google_plus',
                        'value'  => $url_google_plus,
                    ));?>
                </div>
                <div class="form-group">
                    <label><?php echo lang('adc_twitter');?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'url_twitter',
                        'value'  => $url_twitter,
                    ));?>
                </div>
                <div class="form-group">
                    <label><?php echo lang('adc_pinterest');?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'url_pinterest',
                        'value'  => $url_pinterest,
                    ));?>
                </div>
                <div class="form-group">
                    <label><?php echo lang('adc_linked_in');?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'url_linked_in',
                        'value'  => $url_linked_in,
                    ));?>
                </div>
                <div class="form-group">
                    <label>
                        Amadeus url
                    </label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'amadeus_url',
                        'value' =>  $amadeus_url
                    ));?>
                </div>
                <div class="form-group">
                    <label>
                        Iata url
                    </label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'iata_url',
                        'value' =>  $iata_url
                    ));?>
                </div>
                <div class="form-group">
                    <label>
                        Sabre url
                    </label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'sabre_url',
                        'value' =>  $sabre_url
                    ));?>
                </div>
                <div class="form-group">
                    <label>
                        Snow
                    </label>
                    <?php echo form_dropdown('snow', array('No','Yes'), $snow,'class="form-control"');?>

                </div>

                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>


            </div>
            <!-- /.col-lg-12 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->