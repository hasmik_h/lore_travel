<!-- Top content -->
<div class="top-content">

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1><strong>ADC</strong> <?php echo lang('adc_administration_system');?></h1>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <div class="text-danger wrong-data" style="display:none"></div>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="" method="post" class="login-form" id="login-form">
                            <div class="form-group">
                                <input type="text" name="username" placeholder="<?php echo lang('adc_username');?>" class="form-username form-control" id="username">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" placeholder="<?php echo lang('adc_password');?>" class="form-password form-control" id="password">
                            </div>

                            <button type="submit" class="btn" id="login-btn"><?php echo lang('adc_sign_in');?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>