<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_translations');?>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            &nbsp;
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <table id="translations" class="table table-striped table-bordered table-condensed table-hover dataTable no-footer">
                            <thead>
                            <tr>
                            <td><?php echo lang('adc_language')?></td>
                            <td>ID</td>
                            <td><?php echo lang('adc_text')?></td>
                            <td>&nbsp;</td>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->


<div id="edit-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php  echo
        form_open('', array(
            'role' => 'form',
            'id'   => 'translations-form',
        ));?>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary text-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <?php foreach ($activeLanguagesIsoCodes as $activeLanguagesIsoCode) { ?>
                    <div class="form-group">
                        <label><?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control',
                            'rows' => 2,
                            'name'  => transformInputLanguage('content',$activeLanguagesIsoCode),
                        ));?>
                    </div>
                    <hr>
                <?php } ?>
                <input type="hidden" value="" name="id" id="hidden-id">
            </div>
            <div class="modal-footer">
                <?php echo form_button(array('content' => lang('adc_close'), 'type' => 'button', 'class' => 'btn btn-default', 'data-dismiss' => 'modal'));?>
                <?php echo form_button(array('content' => lang('adc_save'), 'type' => 'submit', 'class' => 'btn btn-primary', 'id' => 'edit-translation'));?>
                <?php echo form_close()?>
            </div>
        </div>

    </div>
</div>
