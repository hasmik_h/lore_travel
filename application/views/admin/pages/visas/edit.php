<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_visas');?> - <?php echo $typeText;?> - <?php echo $subtypeText;?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'visas-form',
            ));?>
            <div class="col-lg-6">
                <?php foreach ($activeLanguagesIsoCodes as $activeLanguagesIsoCode) { ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_text');?> <?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control mceAdvanced',
                            'rows' => 3,
                            'name'  => transformInputLanguage('text',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $content[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('adc_country');?> <?php echo translateLanguage($activeLanguagesIsoCode);?><?php echo requiredAsterisk(); ?></label>
                        <?php echo form_input(array(
                            'class' => 'form-control',
                            'name'  => transformInputLanguage('country',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $country[$activeLanguagesIsoCode['iso_code']] : '',
                            'required' => 'required'
                        ));?>
                    </div>
                    <hr>
              <?php } ?>

            </div>
            <!-- /.col-lg-6 (nested) -->

            <div class="col-lg-6">
                <?php if (isset($id)) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                        <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_VISAS . $file)?>" class="img-responsive img-thumbnail">
                        </div>
                    </div>

                <?php } ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_image') .' (' . FLAG_WIDTH . 'x' . FLAG_HEIGHT . ')';?> <?php if (!isset($id)) {echo requiredAsterisk();};?></label>
                        <?php echo form_upload(array(
                            'class' => 'form-control',
                            'name'  => 'image',
                            'id'  => 'image',
                        ));?>
                    </div>

                <div class="form-group <?php echo ($type == VISA_TYPE_TO_ARMENIA) ? 'hidden' : ''?>">
                    <label><?php echo lang('adc_country');?></label>
                    <?php if (!isset($country_id)) {
                        $country_id = -1;
                    }?>
                    <?php echo form_dropdown('country_id', $countriesNotSetForSelect, $country_id,'id="country_id" class="form-control"');?>
                </div>

                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>


            </div>
            <!-- /.col-lg-6 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->