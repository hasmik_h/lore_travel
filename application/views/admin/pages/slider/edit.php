<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_slider');?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'slider-form',
            ));?>
            <div class="col-lg-6">
                <?php foreach ($activeLanguagesIsoCodes as $activeLanguagesIsoCode) { ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_text');?> <?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control mceAdvanced',
                            'rows' => 3,
                            'name'  => transformInputLanguage('text',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $content[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>
                    <div class="form-group">
                        <label><?php echo lang('adc_title');?> <?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_input(array(
                            'class' => 'form-control',
                            'name'  => transformInputLanguage('title',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $title[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('adc_link_text');?> <?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_input(array(
                            'class' => 'form-control',
                            'name'  => transformInputLanguage('link_text',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $link_text[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>
                    <hr>
              <?php } ?>

            </div>
            <!-- /.col-lg-6 (nested) -->

            <div class="col-lg-6">
                <?php if (isset($id)) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                        <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_SLIDER . $file)?>" class="img-responsive img-thumbnail">
                        </div>
                    </div>

                <?php } ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_image') . ' (' . SLIDER_WIDTH . 'x' . SLIDER_HEIGHT . ')';?> <?php if (!isset($id)) {echo requiredAsterisk();};?></label>
                        <?php echo form_upload(array(
                            'class' => 'form-control',
                            'name'  => 'slider_image',
                            'id'  => 'slider_image',
                        ));?>
                    </div>

                <div class="form-group">
                    <label><?php echo lang('adc_link');?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'link',
                        'value' => isset($id) ? $link : ''
                    ));?>
                </div>
                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>


            </div>
            <!-- /.col-lg-6 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->