<div class="panel panel-default" xmlns="http://www.w3.org/1999/html">
    <div class="panel-heading">
        <?php echo lang('adc_destination');?>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <form action="<?=base_url()?><?=(isset($destination))?('admin_countries/edit'):'admin_countries/add';?>" method="post" enctype="multipart/form-data">
                    <div class=" col-lg-6 no-padding">
                        <div class="form-group">
                            <label> Select continent <span class="text-danger">*</span></label>
                            <select name="continent" class="form-control" required="required" >
                                <option value="1" <?=(isset($destination) && $destination[0]['continent'] == 1)?('selected'):'';?> > Europe </option>
                                <option value="2" <?=(isset($destination) && $destination[0]['continent'] == 2)?('selected'):'';?> > Asia </option>
                                <option value="3" <?=(isset($destination) && $destination[0]['continent'] == 3)?('selected'):'';?> > Africa </option>
                                <option value="4" <?=(isset($destination) && $destination[0]['continent'] == 4)?('selected'):'';?> > America </option>
                                <option value="5" <?=(isset($destination) && $destination[0]['continent'] == 5)?('selected'):'';?> > Australia </option>
                            </select>
                        </div>
                        <!-- armenian -->
                        <div class="form-group">
                            <label> Title (Armenian) <span class="text-danger">*</span></label>
                            <input type="text" name="title[hy]" value="<?=(isset($destination))?($destination[0]['title']):'';?>" class="form-control" required="required" rows="3">
                        </div>
                        <label>Short description (Armenian)<span class="text-danger">*</span></label>
                        <textarea name="summary[hy]" cols="40" rows="3" class="form-control mceAdvanced" ><?=(isset($destination))?($destination[0]['summary']):'';?></textarea><br>
                        <div class="form-group">
                            <label> Header (Armenian) <span class="text-danger">*</span></label>
                            <input type="text" name="header[hy]" value="<?=(isset($destination))?($destination[0]['header']):'';?>" class="form-control" required="required" rows="3">
                        </div>
                        <label>Long description (Armenian)<span class="text-danger">*</span></label>
                        <textarea name="description[hy]" cols="40" rows="3" class="form-control mceAdvanced" ><?=(isset($destination))?($destination[0]['description']):'';?></textarea>
                        <hr>
                        <!-- english -->
                        <div class="form-group">
                            <label> Title (English) <span class="text-danger">*</span></label>
                            <input type="text" name="title[en]" value="<?=(isset($destination))?($destination[1]['title']):'';?>" class="form-control" required="required" rows="3">
                        </div>
                        <label>Short description (English)<span class="text-danger">*</span></label>
                        <textarea name="summary[en]" cols="40" rows="3" class="form-control mceAdvanced"><?=(isset($destination))?($destination[1]['summary']):'';?></textarea><br>
                        <div class="form-group">
                            <label> Header (English) <span class="text-danger">*</span></label>
                            <input type="text" name="header[en]" value="<?=(isset($destination))?($destination[1]['header']):'';?>" class="form-control" required="required" rows="3">
                        </div>
                        <label>Long description (English)<span class="text-danger">*</span></label>
                        <textarea name="description[en]" cols="40" rows="3" class="form-control mceAdvanced" ><?=(isset($destination))?($destination[1]['description']):'';?></textarea>
                    </div>
                    <div class="col-lg-6 no-padding">
                        <br>
                        <?php if(isset($destination) && !empty($destination)):?>
                            <div class="row">
                                <div class="col-sm-12" id="destination_images">
                                    <?php foreach($destination[0]['files'] as $files):?>
                                        <div style="position: relative; display: inline-block" data-id="<?=$files['id']?>">
                                            <img src="<?=base_url()?><?=$files['path']?>" style=" width: 100px;" class="img-responsive img-thumbnail">
                                            <i class="fa fa-times-circle delete_dest_img" data_id="<?=$files['id']?>" data_path="<?=$files['path']?>"></i>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <br>
                        <?php endif; ?>
                        <div class="form-group">
                            <label><?=(isset($destination))?('Add'):'Choose';?> Images (462*347px)<span class="text-danger">*</span></label>
                            <input type="file" name="guide_imgs[]" class="form-control" id="main_img" multiple>
                        </div>
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    <input type="hidden" value="<?=(isset($destination))?($destination[1]['parent_id']):'';?>" name="parent_id" />
                </form>
            </div>
        </div>
    </div>
</div>

<div id="delete_img_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-danger text-danger">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this image?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="delete_dest_img">Yes</button>
            </div>
        </div>
    </div>
</div>