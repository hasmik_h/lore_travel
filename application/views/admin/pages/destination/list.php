<link href="<?=base_url()?>public/css/pages/admin/tours/index.css" rel="stylesheet">
<link href="<?=base_url()?>public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css" rel="stylesheet">
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Guide list
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <p>
                                    &nbsp;
                                </p>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-12">
                                <p> </p>
                                <ul id="tours" class="ui-sortable">
                                    <?php if(isset($destination) && !empty($destination)):?>
                                        <?php foreach($destination as $data):?>
                                            <li class="ui-state-default ui-sortable-handle" data-id="<?=$data['parent_id']?>">
        <!--                                        <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>-->
                                                <?php if(isset($data['files']) && !empty($data['files'])):?>
                                                    <img class="tour-thumb" src="<?=base_url()?><?=$data['files'][0]['path']?>" alt="img">&nbsp;<?=$data['title']?>
                                                <?php else:?>
                                                    <img class="tour-thumb" src="<?=base_url()?>" alt="img">&nbsp;<?=$data['title']?>
                                                <?php endif; ?>
                                                <a href="#" class="glyphicon glyphicon-remove delete_guide pull-right"></a>
                                                <a href="<?=base_url()?>admin_countries/destination/<?=$data['parent_id']?>" class="glyphicon glyphicon-pencil edit-tour"></a>
        <!--                                        <a href="http://loretravel.dev/admin_tours/gallery/1/5" class="glyphicon glyphicon-picture gallery-tour" data-toggle="tooltip" title="Gallery"></a>-->
                                            </li>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                    <li class=" ui-sortable-handle">
                                        List is empty!
                                    </li>
                                    <?php endif; ?>
                                </ul>
                                <p></p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <a href="<?=base_url()?>admin_countries/destination" class="btn btn-primary pull-right">Add</a>
<!--                        <button type="submit" class="btn btn-primary pull-right margin-right-5" id="submit-changes">Save</button>-->
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->


        <div id="remove-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header bg-danger text-danger">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="delete_guide">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>