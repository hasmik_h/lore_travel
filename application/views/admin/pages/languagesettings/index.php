<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_languages');?>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            <?php echo lang('adc_languages_on_left')?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            <?php echo lang('adc_languages_on_right')?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6">
                        <?php echo createNotSortableULForLanguages($inactiveLanguages);?>
            </div>
            <!-- /.col-lg-6 (nested) -->

            <div class="col-lg-6">
                         <?php echo createSortableULForLanguages($activeLanguages);?>
                         <?php echo form_button(array(
                             'type' => 'submit',
                             'class' => 'btn btn-primary',
                             'id' => 'submit-changes',
                             'content' => lang('adc_save')
                         ))?>
            </div>
            <!-- /.col-lg-6 (nested) -->
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->

<!-- Deactivate Modal -->
<div id="deactivate-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-danger text-danger">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo lang('adc_are_you_sure')?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo lang('adc_warning_deactivate_language')?></p>
            </div>
            <div class="modal-footer">
                <?php echo form_button(array('content' => lang('adc_close'), 'type' => 'button', 'class' => 'btn btn-default', 'data-dismiss' => 'modal'));?>
                <?php echo form_button(array('content' => lang('adc_deactivate'), 'type' => 'button', 'class' => 'btn btn-danger', 'id' => 'deactivate-language'));?>
            </div>
        </div>

    </div>
</div>

<!-- Can not remove default language Modal -->
<div id="can-not-remove-default-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-danger text-danger">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p><?php echo lang('adc_choose_another_default_language')?></p>
            </div>
            <div class="modal-footer">
                <?php echo form_button(array('content' => 'OK', 'type' => 'button', 'class' => 'btn btn-primary', 'data-dismiss' => 'modal'));?>
            </div>
        </div>

    </div>
</div>

<!-- Edit Modal -->
<div id="edit-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-info text-info">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo lang('adc_edit')?></h4>
            </div>
            <div class="modal-body">
                <?php echo
                form_open('', array(
                'role' => 'form',
                'id'   => 'language-settings-form',
                ));?>
                <div class="form-group">
                    <label><?php echo lang('adc_language_original_name')?><?php echo requiredAsterisk();?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'id'  => 'lang-name-original',
                        'required' => 'true'
                    ));?>
                </div>

                <div class="checkbox">
                    <label>
                        <?php  echo form_checkbox(array('id' => 'is-default-language')); ?><?php echo lang('adc_default_language')?>
                    </label>
                </div>

                <div class="checkbox">
                    <label>
                        <?php  echo form_checkbox(array('id' => 'is-hidden-language')); ?><?php echo lang('adc_hidden')?>
                    </label>
                </div>

            </div>
            <div class="modal-footer">
                <?php echo form_button(array('content' => lang('adc_close'), 'type' => 'button', 'class' => 'btn btn-default', 'data-dismiss' => 'modal'));?>
                <?php echo form_button(array('content' => 'OK', 'type' => 'submit', 'class' => 'btn btn-primary', 'id' => 'deactivate-language'));?>
                <?php echo form_close()?>
            </div>
        </div>

    </div>
</div>