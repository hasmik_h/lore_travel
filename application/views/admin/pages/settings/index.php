<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_admin_settings');?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open('', array(
                'role' => 'form',
                'id'   => 'settings-form',
            ));?>
            <div class="col-lg-6">

                    <div class="form-group">
                        <label><?php echo lang('adc_old_password');?><?php echo requiredAsterisk(); ?></label>
                        <?php echo form_password(array(
                            'class' => 'form-control',
                            'name'  => 'current_password',
                            'id'  => 'current_password',
                            'required' => 'true'
                        ));?>
                    </div>
                    <div class="form-group">
                        <label><?php echo lang('adc_new_password');?><?php echo requiredAsterisk(); ?></label>
                        <?php echo form_password(array(
                            'class' => 'form-control',
                            'name'  => 'new_password',
                            'id'  => 'new_password',
                            'required' => true
                        ));?>
                    </div>
                    <div class="form-group">
                        <label><?php echo lang('adc_repeat_new_password');?><?php echo requiredAsterisk(); ?></label>
                        <?php echo form_password(array(
                            'class' => 'form-control',
                            'name'  => 'retype_new_password',
                            'id'  => 'retype_new_password',
                            'required' => 'true'
                        ));?>
                    </div>
                    <?php echo form_button(array(
                        'type' => 'submit',
                        'class' => 'btn btn-primary',
                        'content' => lang('adc_save')
                    ))?>

                    <?php echo form_button(array(
                        'type' => 'reset',
                        'class' => 'btn btn-default',
                        'content' => lang('adc_reset')
                    ))?>

            </div>
            <!-- /.col-lg-6 (nested) -->
            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->