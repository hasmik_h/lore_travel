<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_admin_language');?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open('', array(
                'role' => 'form',
                'id'   => 'language-form',
            ));?>
            <div class="col-lg-6">

                    <div class="form-group">
                        <label><?php echo lang('adc_admin_language');?></label>
                        <select id="languages" class="form-control">

                            <?php foreach($possibleAdminLanguages as $language)  {?>
                                <option value="<?php echo $language['id']?>"
                                        data-image="/public/img/flags/<?php echo $language['flag']?>"
                                        <?php if ($language['is_admin'] == 1) { ?>
                                             selected="selected"
                                        <?php } ?>
                                    >
                                    <?php echo $language['name_origin'] ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>

                    <?php echo form_button(array(
                        'type' => 'submit',
                        'class' => 'btn btn-primary',
                        'content' => lang('adc_save')
                    ))?>


            </div>
            <!-- /.col-lg-6 (nested) -->
            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->