<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_tours');?> - <?php echo $tourTypeName;?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'tours-form',
            ));?>
            <div class="col-lg-6">
                <?php foreach ($activeLanguagesIsoCodes as $activeLanguagesIsoCode) { ?>

                    <div class="form-group">
                        <label><?php echo lang('adc_name');?> <?php echo translateLanguage($activeLanguagesIsoCode);?><?php echo requiredAsterisk(); ?></label>
                        <?php echo form_input(array(
                            'class' => 'form-control',
                            'name'  => transformInputLanguage('name',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $name[$activeLanguagesIsoCode['iso_code']] : '',
                            'required' => 'required'
                        ));?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('adc_short_description');?> <?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control mceAdvanced',
                            'rows' => 3,
                            'name'  => transformInputLanguage('description',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $description[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('adc_long_description');?> <?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control mceAdvanced',
                            'rows' => 3,
                            'name'  => transformInputLanguage('description_long',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $description_long[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>

                    <div class="form-group <?php if ($type == TOUR_TYPE_INCOMING) {echo 'hidden';}?>">
                        <label>Additional description <?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control mceAdvanced',
                            'rows' => 3,
                            'name'  => transformInputLanguage('description_additional',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $description_additional[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>

                    <hr>
              <?php } ?>

            </div>
            <!-- /.col-lg-6 (nested) -->

            <div class="col-lg-6">

                <div class="form-group">
                    <label><?php echo lang('adc_slug');?><?php echo requiredAsterisk(); ?><?php echo explanationSign(lang('adc_slug_explanation_tours'));?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'slug',
                        'value' => isset($id) ? $slug : ''
                    ));?>
                </div>

                <?php if (isset($id)) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_TOURS . $main_img)?>" class="img-responsive img-thumbnail">
                        </div>
                    </div>

                <?php } ?>
                <div class="form-group">
                    <label><?php echo lang('adc_main_image') . ' (' . TOUR_MAIN_IMAGE_WIDTH . 'x' . TOUR_MAIN_IMAGE_HEIGHT . ')';?> <?php if (!isset($id)) {echo requiredAsterisk();};?></label>
                    <?php echo form_upload(array(
                        'class' => 'form-control',
                        'name'  => 'main_img',
                        'id'  => 'main_img',
                    ));?>
                </div>
                <hr>

                <?php if (isset($id) && !is_null($thumb)) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                        <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_TOURS . $thumb)?>" class="img-responsive img-thumbnail">
                        </div>
                    </div>
                <?php } ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_image_little') . ' (' . TOUR_THUMB_WIDTH . 'x' . TOUR_THUMB_HEIGHT . ')';?> <?php if (!isset($id)) {echo requiredAsterisk();};?></label>
                        <?php echo form_upload(array(
                            'class' => 'form-control',
                            'name'  => 'thumb',
                            'id'  => 'thumb',
                        ));?>
                    </div>

                <?php if ($type != TOUR_TYPE_INCOMING && $type != TOUR_TYPE_AIRTICKETS) { ?>
                <div class="form-group">
                    <label><?php echo lang('adc_attached_countries');?> (<?php echo lang('adc_you_can_select_several');?>)</label>
                    <select name="countries_attached[]" class="form-control" id="countries_attached" multiple="multiple">
                        <?php foreach($countriesForSelect as $countryId => $countryName) { ?>
                            <option value="<?php echo $countryId; ?>" <?php echo (in_array($countryId, $selectedCountries)) ? 'selected="selected"' : ''?>><?php echo $countryName; ?></option>
                        <?php } ?>
                    </select>
                <?php } ?>
                <hr>

                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>


            </div>
            <!-- /.col-lg-6 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->