<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo 'Our Team';?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'our-team-form',
            ));?>
            <div class="col-lg-6">
                <?php foreach ($activeLanguagesIsoCodes as $activeLanguagesIsoCode) { ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_text');?> <?php echo translateLanguage($activeLanguagesIsoCode);?> <?php echo requiredAsterisk();?></label>
                        <?php echo form_input(array(
                            'class' => 'form-control',
                            'required' => 'required',
                            'rows' => 3,
                            'name'  => transformInputLanguage('text',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $content[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>

                    <div class="form-group">
                        <label>Position <?php echo translateLanguage($activeLanguagesIsoCode);?> <?php echo requiredAsterisk();?></label>
                        <?php echo form_input(array(
                            'class' => 'form-control',
                            'required' => 'required',
                            'rows' => 3,
                            'name'  => transformInputLanguage('name',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $name[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>
                    <hr>
                <?php } ?>

            </div>
            <!-- /.col-lg-6 (nested) -->

            <div class="col-lg-6">
                <?php if (isset($id)) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_BEST_OFFERS . $file)?>" class="img-responsive img-thumbnail">
                        </div>
                    </div>

                <?php } ?>
                <div class="form-group">
                    <label><?php echo lang('adc_image') . ' (' . lang('adc_width') . '=' . lang('adc_height') . ')';?> <?php if (!isset($id)) {echo requiredAsterisk();};?></label>
                    <?php echo form_upload(array(
                        'class' => 'form-control',
                        'name'  => 'image',
                        'id'  => 'image',
                    ));?>
                </div>

                <div class="form-group">
                    <label><?php echo lang('adc_link');?> <?php echo requiredAsterisk();?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'link',
                        'value' => isset($id) ? $link : '',
                        'required' => 'required',
                    ));?>
                </div>
                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>


            </div>
            <!-- /.col-lg-6 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->