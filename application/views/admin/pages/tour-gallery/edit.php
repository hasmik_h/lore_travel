<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_gallery');?>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php $breadcrumbName = isset($id) ? lang('adc_edit') : lang('adc_add')?>
                <?php echo hmBreadcrumbs(array(
                    lang('adc_tours'),
                    $tourTypeName => 'admin_tours/list_of_tours/' . $tourType,
                    $tourName => 'admin_tours/edit/' . $tourId,
                    lang('adc_gallery') => 'admin_tours/gallery/' . $tourType . '/' . $tourId,
                    $breadcrumbName
                ))?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">&nbsp;</div>
        </div>
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'tour-gallery-form',
            ));?>
            <div class="col-lg-6">
                <?php foreach ($activeLanguagesIsoCodes as $activeLanguagesIsoCode) { ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_text');?> <?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control',
                            'rows' => 3,
                            'name'  => transformInputLanguage('text',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $content[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>
                    <hr>
              <?php } ?>

            </div>
            <!-- /.col-lg-6 (nested) -->

            <div class="col-lg-6">
                <?php if (isset($id)) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                        <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_TOUR_GALLERY . $file)?>" class="img-responsive img-thumbnail">
                        </div>
                    </div>

                <?php } ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_image') . ' (' . TOUR_GALLERY_IMAGE_WIDTH . 'x' . TOUR_GALLERY_IMAGE_HEIGHT . ')';?> <?php if (!isset($id)) {echo requiredAsterisk();};?></label>
                        <?php echo form_upload(array(
                            'class' => 'form-control',
                            'name'  => 'image',
                            'id'  => 'image',
                        ));?>
                    </div>
                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>


            </div>
            <!-- /.col-lg-6 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->