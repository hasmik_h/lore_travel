<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_countries');?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'countries-form',
            ));?>
            <div class="col-lg-6">
                <?php foreach ($activeLanguagesIsoCodes as $activeLanguagesIsoCode) { ?>

                    <div class="form-group">
                        <label><?php echo lang('adc_name');?> <?php echo translateLanguage($activeLanguagesIsoCode);?><?php echo requiredAsterisk(); ?></label>
                        <?php echo form_input(array(
                            'class' => 'form-control',
                            'name'  => transformInputLanguage('name',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $name[$activeLanguagesIsoCode['iso_code']] : '',
                            'required' => 'required'
                        ));?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('adc_comment_on_hover');?> <?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control mceAdvanced',
                            'rows' => 3,
                            'name'  => transformInputLanguage('comment_on_hover',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $comment_on_hover[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('adc_comment_on_click');?> <?php echo translateLanguage($activeLanguagesIsoCode);?></label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control mceAdvanced',
                            'rows' => 3,
                            'name'  => transformInputLanguage('comment_on_click',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $comment_on_click[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>


                    <hr>
              <?php } ?>

            </div>
            <!-- /.col-lg-6 (nested) -->

            <div class="col-lg-6">

                <div class="form-group">
                    <label><?php echo lang('adc_slug');?><?php echo requiredAsterisk(); ?><?php echo explanationSign(lang('adc_slug_explanation_countries'));?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'slug',
                        'value' => isset($id) ? $slug : ''
                    ));?>
                </div>

                <div class="form-group">
                    <label>
                        Alternative link
                    </label>
                    <?php if (!isset($alternative_link)) { $alternative_link = 1;}?>
                    <?php echo form_dropdown('alternative_link', array('No','Yes'), $alternative_link,'class="form-control"');?>

                </div>


                <div class="form-group">
                    <label><?php echo lang('adc_color_default');?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'color_default',
                        'type'  => 'color',
                        'value' => (isset($id) && !is_null($color_default) && $color_default)
                            ? $color_default : $color_map_default
                    ));?>
                </div>

                <div class="form-group">
                    <label><?php echo lang('adc_color_hover');?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'color_hover',
                        'type'  => 'color',
                        'value' => (isset($id) && !is_null($color_hover) && $color_hover)
                            ? $color_hover : $color_map_default_hover
                    ));?>
                </div>

                <?php if (isset($id)) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_COUNTRIES . $flag)?>" class="img-responsive img-thumbnail">
                        </div>
                    </div>

                <?php } ?>
                <div class="form-group">
                    <label><?php echo lang('adc_flag') . ' (' . FLAG_WIDTH . 'x' . FLAG_HEIGHT . ')';?> <?php if (!isset($id)) {echo requiredAsterisk();};?></label>
                    <?php echo form_upload(array(
                        'class' => 'form-control',
                        'name'  => 'flag',
                        'id'  => 'flag',
                    ));?>
                </div>
                <hr>

                <?php if (isset($id) && !is_null($thumb)) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                        <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_COUNTRIES . $thumb)?>" class="img-responsive img-thumbnail">
                        </div>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remove_thumb" value="1"><?php echo lang('adc_remove'); ?>
                        </label>
                    </div>
                <?php } ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_image_little') . ' (' . lang('adc_width') . '=' . lang('adc_height') . ')';?></label>
                        <?php echo form_upload(array(
                            'class' => 'form-control',
                            'name'  => 'thumb',
                            'id'  => 'thumb',
                        ));?>
                    </div>
                <hr>

                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>


            </div>
            <!-- /.col-lg-6 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->