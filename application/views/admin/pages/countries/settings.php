<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_map');?> - <?php echo lang('adc_settings');?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'map-settings-form',
            ));?>

            <div class="col-lg-12">

                    <div class="form-group">
                        <label><?php echo lang('adc_color_default');?></label>
                        <?php echo form_input(array(
                            'class' => 'form-control',
                            'name'  => 'color_map_default',
                            'type'  => 'color',
                            'value'  => $color_map_default,
                        ));?>
                    </div>
                <div class="form-group">
                    <label><?php echo lang('adc_color_default_hover');?></label>
                    <?php echo form_input(array(
                        'class' => 'form-control',
                        'name'  => 'color_map_default_hover',
                        'type'  => 'color',
                        'value'  => $color_map_default_hover,
                    ));?>
                </div>

                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>


            </div>
            <!-- /.col-lg-12 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->