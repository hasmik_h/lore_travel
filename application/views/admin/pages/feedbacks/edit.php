<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo lang('adc_feedbacks');?>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php  echo
            form_open_multipart('', array(
                'role' => 'form',
                'id'   => 'feedbacks-form',
            ));?>
            <div class="col-lg-6">
                <?php foreach ($activeLanguagesIsoCodes as $activeLanguagesIsoCode) {  ?>
                    <div class="form-group">
                        <label><?php echo lang('adc_customer_name');?> <?php echo translateLanguage($activeLanguagesIsoCode);?><?php echo requiredAsterisk(); ?></label>
                        <?php echo form_input(array(
                            'class' => 'form-control',
                            'name'  => transformInputLanguage('customer_name',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $customer_name[$activeLanguagesIsoCode['iso_code']] : '',
                            'required' => 'required'
                        ));?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('adc_text');?> <?php echo translateLanguage($activeLanguagesIsoCode);?> <?php echo requiredAsterisk();?></label>
                        <?php echo form_textarea(array(
                            'class' => 'form-control mceAdvanced',
                            'required' => 'required',
                            'rows' => 3,
                            'name'  => transformInputLanguage('text',$activeLanguagesIsoCode),
                            'value' => isset($id) ? $content[$activeLanguagesIsoCode['iso_code']] : ''
                        ));?>
                    </div>
                    <hr>
              <?php } ?>

            </div>
            <!-- /.col-lg-6 (nested) -->

            <div class="col-lg-6">

                <div class="form-group">
                    <label><?php echo lang('adc_score');?> <?php echo requiredAsterisk();?></label>
                    <?php $selected = isset($id) ? $score : '5'; ?>
                    <?php  echo form_dropdown('score', array(
                        '1' =>1,
                        '2' =>2,
                        '3' =>3,
                        '4' =>4,
                        '5' =>5,
                    ), $selected, 'class="form-control"');?>
                </div>

                <?php echo form_button(array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'content' => lang('adc_save')
                ))?>
            </div>
            <!-- /.col-lg-6 (nested) -->

            <?php echo form_close()?>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->