<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ADC <?php echo lang('adc_administration_system');?></title>

    <link rel="shortcut icon" href="<?php echo base_url();?>public/img/favicons/adc/favicon.ico" />

    <link href="<?php echo base_url();?>public/css/vendor/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>public/css/vendor/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>public/css/vendor/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo base_url();?>public/css/vendor/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>public/css/vendor/pnotify.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>public/editor/tinymce/skins/lightgray/skin.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>public/css/common/admin/custom.css" rel="stylesheet">

    <?php
    if (!empty($additionalCssFiles)) {
        echo addCssFiles($additionalCssFiles);
    }
    ?>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>public/js/vendor/html5shiv.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo base_url();?>public/js/vendor/jquery.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/validate.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/pnotify.min.js"></script>
    <script src="<?php echo base_url();?>public/editor/tinymce/tinymce.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/metisMenu.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/sb-admin-2.js"></script>

    <script src="<?php echo base_url();?>public/js/common/admin/functions.js"></script>
    <script src="<?php echo base_url();?>public/js/common/admin/custom.js"></script>


    <?php
    if (!empty($additionalJsFiles)) {
        echo addJsFiles($additionalJsFiles);
    }
    ?>
    <?php
    if (!empty($globalJsVariables)) {
        echo createGlobalJsVariables($globalJsVariables);
    }
    ?>

    <script src="<?php echo base_url();?>public/js/common/admin/init.tinymce.js"></script>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url("admin") ?>">ADC <?php echo lang('adc_administration_system');?></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="<?php echo site_url("admin_settings") ?>"><i class="fa fa-gear fa-settings"></i> <?php echo lang('adc_admin_settings')?></a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?php echo site_url("admin_settings/language") ?>"><i class="fa  fa-language"></i> <?php echo lang('adc_admin_language')?></a>
                    <li class="divider"></li>
                    <li><a href="<?php echo site_url("admin/logout") ?>"><i class="fa fa-sign-out "></i> <?php echo lang('adc_logout')?></a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
            </li>
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="#"><i class="fa fa-gear fa-settings"></i> <?php echo lang('adc_settings');?><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url("admin_site_settings/common") ?>"><i class="fa fa-gear fa-settings"></i> <?php echo lang('adc_common');?></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url("admin_language_settings") ?>"><i class="fa fa-language"></i> <?php echo lang('adc_languages');?></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-tags"></i> <?php echo lang('adc_meta_tags');?> <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_HOME);?>"><?php echo lang('adc_home');?></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_ABOUT_US);?>"><?php echo lang('adc_about_us');?></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_INCOMING_TOURS);?>"><?php echo lang('adc_tours_incoming');?></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_OUTGOING_TOURS);?>"><?php echo lang('adc_tours_outgoing');?></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_EDUCATIONAL_TOURS);?>"><?php echo lang('adc_tours_educational');?></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_DESTINATION);?>"><?php echo lang('adc_destination');?></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_BOOK_HOTEL);?>"><?php echo lang('adc_book_hotel');?></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_VISAS_TO_ARMENIA);?>"><?php echo lang('adc_visas_to_armenia');?></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_VISAS_FROM_ARMENIA);?>"><?php echo lang('adc_visas_from_armenia');?></a>
                                    </li>
                                    <li style="display: none">
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_NOT_DECIDED);?>"><?php echo lang('adc_not_decided');?></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin_site_settings/edit_meta_tags/' . SLUG_CONTACT_US);?>"><?php echo lang('adc_contact_us');?></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li><a href="<?php echo site_url('admin_translations')?>"><i class="fa fa-file-text"></i> <?php echo lang('adc_translations');?></a></li>
                    <li>
                        <a href="#"><i class="fa fa-home"></i> <?php echo lang('adc_home');?><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('admin_slider')?>"><i class="fa fa-sliders"></i> <?php echo lang('adc_slider');?></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin_best_offers')?>"><i class="fa fa-fire"></i> <?php echo lang('adc_best_offer');?></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin_feedbacks')?>"><i class="fa fa-comments"></i> <?php echo lang('adc_feedbacks');?></a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-info-circle"></i> <?php echo lang('adc_about_us');?><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('admin_about_us_jobs')?>"><?php echo 'What we Do';?></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin_our_team')?>"><?php echo 'Meet Our Teem';?></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin_our_sertificates')?>"><?php echo 'Our Sertificates';?></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin_our_partners')?>"><?php echo 'Our Partners';?></a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin_tours/list_of_tours/' . TOUR_TYPE_AIRTICKETS)?>"><i class="fa fa-plane"></i> <?php echo 'Airtickets';?></a>
                    </li>
                    <li><a href="<?php echo site_url('admin_partners')?>"><i class="fa fa-user"></i> <?php echo lang('adc_partners');?></a></li>
                    <!--<li>
                        <a href="#"><i class="fa fa-globe"></i> <?php /*echo lang('adc_countries');*/?><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php /*echo site_url('admin_countries/settings')*/?>"><i class="fa fa-gear fa-settings"></i> <?php /*echo lang('adc_settings');*/?></a>
                            </li>
                            <li>
                                <a href="<?php /*echo site_url('admin_countries/list_of_countries')*/?>"><i class="fa fa-list"></i> <?php /*echo lang('adc_list');*/?></a>
                            </li>
                            <li>
                                <a href="<?php /*echo site_url('admin_countries/map')*/?>"><i class="fa fa-map-marker"></i> <?php /*echo lang('adc_map');*/?></a>
                            </li>

                        </ul>
                    </li>-->
                        <!-- /.nav-second-level -->
                    <li>
                        <a href="<?php echo site_url('admin_countries/destination_list')?>"><i class="fa fa-map-marker"></i> <?=lang('adc_destination');?></a>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-plane"></i> <?php echo lang('adc_tours');?><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('admin_tours/list_of_tours/' . TOUR_TYPE_INCOMING)?>"><i class="fa fa-arrow-left"></i> <?php echo lang('adc_incoming');?></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin_tours/list_of_tours/' . TOUR_TYPE_OUTGOING)?>"><i class="fa fa-arrow-right"></i> <?php echo lang('adc_outgoing');?></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin_tours/list_of_tours/' . TOUR_TYPE_EDUCATIONAL)?>"><i class="fa fa-book"></i> <?php echo lang('adc_educational');?></a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-file"></i> <?php echo lang('adc_visas');?><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#"><i class="fa fa-arrow-left"></i> <?php echo lang('adc_to_armenia');?> <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="<?php echo site_url('admin_visas/list_of_visas/' . VISA_TYPE_TO_ARMENIA . '/' . VISA_SUBTYPE_WITHOUT);?>"><?php echo lang('adc_without_visa');?></a>
                                    </li>

                                    <li>
                                        <a href="<?php echo site_url('admin_visas/list_of_visas/' . VISA_TYPE_TO_ARMENIA . '/' . VISA_SUBTYPE_BEFORE);?>"><?php echo lang('adc_visa_before_trip');?></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-arrow-right"></i> <?php echo lang('adc_from_armenia');?> <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="<?php echo site_url('admin_visas/list_of_visas/' . VISA_TYPE_FROM_ARMENIA . '/' . VISA_SUBTYPE_WITHOUT);?>"><?php echo lang('adc_without_visa');?></a>
                                    </li>

                                    <li>
                                        <a href="<?php echo site_url('admin_visas/list_of_visas/' . VISA_TYPE_FROM_ARMENIA . '/' . VISA_SUBTYPE_BEFORE);?>"><?php echo lang('adc_visa_before_trip');?></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                &nbsp;
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
               <?php echo $pageView ?>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</body>

</html>
