<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ADC <?php echo lang('adc_administration_system');?></title>

    <link rel="shortcut icon" href="<?php echo base_url();?>public/img/favicons/adc/favicon.ico" />

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link href="<?php echo base_url();?>public/css/vendor/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>public/css/vendor/font-awesome.css" rel="stylesheet">

    <?php
    if (!empty($additionalCssFiles)) {
        echo addCssFiles($additionalCssFiles);
    }
    ?>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>public/js/vendor/html5shiv.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo base_url();?>public/js/vendor/jquery.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/backstretch.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/validate.min.js"></script>

    <?php
    if (!empty($additionalJsFiles)) {
        echo addJsFiles($additionalJsFiles);
    }
    ?>
    <?php
    if (!empty($globalJsVariables)) {
        echo createGlobalJsVariables($globalJsVariables);
    }
    ?>

</head>
<body>
    <?php echo $pageView ?>
</body>

</html>