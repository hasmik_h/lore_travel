<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="<?php echo $lang?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo getMetaData($metaTags);?>
    <title>Lore Travel</title>
    <link href="<?php echo base_url();?>public/css/vendor/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/common/site/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/common/site/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/fwslider.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/tooltip.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/sidecontent.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/pnotify.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/fancybox/jquery.fancybox.css?v=2.1.5"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/selectize.bootstrap3.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/text-animation/animate.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/owl.carousel.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/vendor/owl.theme.css"/>
    <link rel="shortcut icon" href="<?php echo base_url();?>public/img/favicons/website/favicon.ico" />
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>public/img/favicons/website/favicon.ico">
    <link rel="shortcut icon" href="<?php echo base_url();?>public/css/vendor/slick.css" />
    <link rel="shortcut icon" href="<?php echo base_url();?>public/css/vendor/slick-theme.css" />
    <style>
        .selectize-dropdown-content div[data-value="<?php echo $lang?>"]{
            display:none!important;
        }
    </style>
    <?php
    if (!empty($additionalCssFiles)) {
        echo addCssFiles($additionalCssFiles);
    }
    ?>
    <?php echo getGoogleAnalyticsCode();?>
    <?php echo getJivoChatCode();?>
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>public/js/vendor/html5shiv.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/respond.min.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
    <script src="<?php echo base_url();?>public/js/vendor/ie.tags.js"></script>
    <![endif]-->
    <script src="<?php echo base_url();?>public/js/vendor/jquery.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/validate.min.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/pnotify.min.js"></script>

    <!--start slider fwslider -->
    <script src="<?php echo base_url();?>public/js/vendor/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/css3-mediaqueries.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/fwslider.js"></script>

    <!--end slider fwslider -->
    <!--start slider bxslider -->
    <script src="<?php echo base_url();?>public/js/vendor/jquery.bxslider.js"></script>
<!--    <script src="--><?php //echo base_url();?><!--public/js/vendor/jquery.mousewheel-3.0.6.pack.js"></script>-->
    <script src="<?php echo base_url();?>public/js/vendor/jquery.fancybox.js?v=2.1.5"></script>
    <script src="<?php echo base_url();?>public/css/vendor/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script src="<?php echo base_url();?>public/css/vendor/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <script src="<?php echo base_url();?>public/js/vendor/selectize.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/sidecontent.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/text-animation/jquery.fittext.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/text-animation/jquery.lettering.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/text-animation/jquery.textillate.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/owl.carousel.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/jquery.panzoom.js"></script>
    <script src="<?php echo base_url();?>public/js/vendor/masonry.pkgd.js"></script>

    <?php
    if (!empty($additionalJsFiles)) {
        echo addJsFiles($additionalJsFiles);
    }
    ?>
    <?php
    if (!empty($globalJsVariables)) {
        echo createGlobalJsVariables($globalJsVariables);
    }
    ?>
    <script src="<?php echo base_url();?>public/js/common/site/functions.js"></script>
    <script src="<?php echo base_url();?>public/js/common/site/main.js"></script>
    <?php if ($settings['snow']) { ?>
        <script src="<?php echo base_url();?>public/js/vendor/snowfall.jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                setTimeout(function(){
                    $(document).snowfall('clear');
                    document.body.className  = "darkBg";
                    $(document).snowfall({flakeCount: 10, image :"<?php echo site_url(IMAGES_PATH_READ);?>/flake.png", minSize: 10, maxSize:32});
                },4000);

            });
        </script>
    <?php } ?>

</head>
<body>
<!--<div class="side hidden lets-plan-your-trip-block" title=''>
    <i class="fa fa-paper-plane-o"></i>
    <span><a href="#" class="lets-plan-your-trip" data-toggle="modal" data-target=".lets-plan" ><?php echo $translations[2]?></a></span>
</div>-->
<div class="all-contianer">
    <header id="header">
        <div class="header_section">
           <div class="container no-padding">
               <div class="col-md-6 no-padding logo_section">
                   <h1 class="logo">
                       <a href="<?php echo site_url($lang)?>">
                           Lore Travel
                       </a>
                   </h1>
               </div>
               <div class="col-md-6 no-padding">
                   <select class="form-control languages-sel" id="languages"></select>
               </div>
               <div class="clearfix"></div>
           </div>
        </div>
        <div class="menu_section">
            <div class="container no-padding menu_box">
                <ul class="menu_main">
                    <li>
                        <a class="active" href="<?=base_url()?><?=$lang?>">Home</a>
                    </li>
                    <li>
                        <a href="#"><?=$translations[9]?></a>
                        <div class="sub_menu_section">
                            <div class="sub_menu">
                                <ul>
                                    <li><a href="#"><?=$translations[144]?></a></li>
                                    <li><a href="<?=base_url()?><?=$lang?>/destination"><?=$translations[145]?></a></li>
                                    <li><a href="#"><?=$translations[146]?></a></li>
                                    <li><a href="#"><?=$translations[147]?></a></li>
                                </ul>
                            </div>
                            <div class="featured_section">
                                <span class="title_section">FEATURED</span>
                                <div class="featured_section_images">
                                    <img src="<?php echo base_url();?>public/uploads/best-offers/<?=$bestOffers[0]['file']?>" />
                                    <div class="featured_section_images_box">
                                        <a class="featured_img_title" href="#"><?=$bestOffers[0]['content']?></a>
                                        <ul class="featured_list">
                                            <li><i class="fa fa-clock-o"></i><a href="#">November 27, 2015</a></li>
                                            <li><i class="fa fa-folder-o"></i><a href="#">Europe,Italy,Armenia</a></li>
                                        </ul>
                                    </div>
                                    <div class="featured_section_images_hover"></div>
                                </div>
                            </div>
                            <div class="news_later">
                                <span class="title_section">RECENT</span>
                                <div class="news_later_section">
                                    <?php foreach($bestOffers as $data):?>
                                        <div class="news_later_box">
                                            <div class="col-md-3 no-padding">
                                                <img src="<?php echo base_url();?>public/uploads/best-offers/<?=$data['file']?>"  />
                                            </div>
                                            <div class="news_content col-md-9 no-padding">
                                                <a class="news_content_title" href="#"><?=$data['content']?> </a>
                                                <a class="news_content_date" href="#"><i class="fa fa-clock-o"></i>March 2, 2016</a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </li>
                    <li>
                        <a href="<?=base_url()?><?=$lang?>/about-us">About us</a>
                        <div class="sub_menu_section">
                            <div class="sub_menu">
                                <ul>
                                    <li><a href="#">Europe</a></li>
                                    <li><a href="#">North & Central America</a></li>
                                    <li><a href="#">Australia & South Pacific</a></li>
                                    <li><a href="#">Asia</a></li>
                                    <li><a href="#">South America</a></li>
                                    <li><a href="#">South America</a></li>
                                </ul>
                            </div>
                            <div class="featured_section">
                                <span class="title_section">FEATURED</span>
                                <div class="featured_section_images">
                                    <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg" />
                                    <div class="featured_section_images_box">
                                        <a class="featured_img_title" href="#">The Hilltop Towns of Tuscany</a>
                                        <ul class="featured_list">
                                            <li><i class="fa fa-clock-o"></i><a href="#">November 27, 2015</a></li>
                                            <li><i class="fa fa-folder-o"></i><a href="#">Europe,Italy,Armenia</a></li>
                                        </ul>
                                    </div>
                                    <div class="featured_section_images_hover"></div>
                                </div>
                            </div>
                            <div class="news_later">
                                <span class="title_section">RECENT</span>
                                <div class="news_later_section">
                                    <div class="news_later_box">
                                        <div class="col-md-3 no-padding">
                                            <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />
                                        </div>
                                        <div class="news_content col-md-9 no-padding">
                                            <a class="news_content_title" href="#">Discovering the Battlefields of Normandy</a>
                                            <a class="news_content_date" href="#"><i class="fa fa-clock-o"></i>March 2, 2016</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="news_later_box">
                                        <div class="col-md-3 no-padding">
                                            <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />
                                        </div>
                                        <div class="news_content col-md-9 no-padding">
                                            <a class="news_content_title" href="#">Discovering the Battlefields of Normandy</a>
                                            <a class="news_content_date" href="#"><i class="fa fa-clock-o"></i>March 2, 2016</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="news_later_box">
                                        <div class="col-md-3 no-padding">
                                            <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />
                                        </div>
                                        <div class="news_content col-md-9 no-padding">
                                            <a class="news_content_title" href="#">Discovering the Battlefields of Normandy</a>
                                            <a class="news_content_date" href="#"><i class="fa fa-clock-o"></i>March 2, 2016</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </li>
                    <li>
                        <a href="#">Contact Us</a>
                        <div class="sub_menu_section">
                            <div class="sub_menu">
                                <ul>
                                    <li><a href="#">Europe</a></li>
                                    <li><a href="#">North & Central America</a></li>
                                    <li><a href="#">Australia & South Pacific</a></li>
                                    <li><a href="#">Asia</a></li>
                                    <li><a href="#">South America</a></li>
                                    <li><a href="#">South America</a></li>
                                </ul>
                            </div>
                            <div class="featured_section">
                                <span class="title_section">FEATURED</span>
                                <div class="featured_section_images">
                                    <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg" />
                                    <div class="featured_section_images_box">
                                        <a class="featured_img_title" href="#">The Hilltop Towns of Tuscany</a>
                                        <ul class="featured_list">
                                            <li><i class="fa fa-clock-o"></i><a href="#">November 27, 2015</a></li>
                                            <li><i class="fa fa-folder-o"></i><a href="#">Europe,Italy,Armenia</a></li>
                                        </ul>
                                    </div>
                                    <div class="featured_section_images_hover"></div>
                                </div>
                            </div>
                            <div class="news_later">
                                <span class="title_section">RECENT</span>
                                <div class="news_later_section">
                                    <div class="news_later_box">
                                        <div class="col-md-3 no-padding">
                                            <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />
                                        </div>
                                        <div class="news_content col-md-9 no-padding">
                                            <a class="news_content_title" href="#">Discovering the Battlefields of Normandy</a>
                                            <a class="news_content_date" href="#"><i class="fa fa-clock-o"></i>March 2, 2016</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="news_later_box">
                                        <div class="col-md-3 no-padding">
                                            <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />
                                        </div>
                                        <div class="news_content col-md-9 no-padding">
                                            <a class="news_content_title" href="#">Discovering the Battlefields of Normandy</a>
                                            <a class="news_content_date" href="#"><i class="fa fa-clock-o"></i>March 2, 2016</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="news_later_box">
                                        <div class="col-md-3 no-padding">
                                            <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />
                                        </div>
                                        <div class="news_content col-md-9 no-padding">
                                            <a class="news_content_title" href="#">Discovering the Battlefields of Normandy</a>
                                            <a class="news_content_date" href="#"><i class="fa fa-clock-o"></i>March 2, 2016</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </li>
                    <li>
                        <a href="#">FAQ</a>
                        <div class="sub_menu_section">
                            <div class="sub_menu">
                                <ul>
                                    <li><a href="#">Europe</a></li>
                                    <li><a href="#">North & Central America</a></li>
                                    <li><a href="#">Australia & South Pacific</a></li>
                                    <li><a href="#">Asia</a></li>
                                    <li><a href="#">South America</a></li>
                                    <li><a href="#">South America</a></li>
                                </ul>
                            </div>
                            <div class="featured_section">
                                <span class="title_section">FEATURED</span>
                                <div class="featured_section_images">
                                    <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg" />
                                    <div class="featured_section_images_box">
                                        <a class="featured_img_title" href="#">The Hilltop Towns of Tuscany</a>
                                        <ul class="featured_list">
                                            <li><i class="fa fa-clock-o"></i><a href="#">November 27, 2015</a></li>
                                            <li><i class="fa fa-folder-o"></i><a href="#">Europe,Italy,Armenia</a></li>
                                        </ul>
                                    </div>
                                    <div class="featured_section_images_hover"></div>
                                </div>
                            </div>
                            <div class="news_later">
                                <span class="title_section">RECENT</span>
                                <div class="news_later_section">
                                    <div class="news_later_box">
                                        <div class="col-md-3 no-padding">
                                            <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg" />
                                        </div>
                                        <div class="news_content col-md-9 no-padding">
                                            <a class="news_content_title" href="#">Discovering the Battlefields of Normandy</a>
                                            <a class="news_content_date" href="#"><i class="fa fa-clock-o"></i>March 2, 2016</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="news_later_box">
                                        <div class="col-md-3 no-padding">
                                            <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />
                                        </div>
                                        <div class="news_content col-md-9 no-padding">
                                            <a class="news_content_title" href="#">Discovering the Battlefields of Normandy</a>
                                            <a class="news_content_date" href="#"><i class="fa fa-clock-o"></i>March 2, 2016</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="news_later_box">
                                        <div class="col-md-3 no-padding">
                                            <img src="<?php echo base_url();?>public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />
                                        </div>
                                        <div class="news_content col-md-9 no-padding">
                                            <a class="news_content_title" href="#">Discovering the Battlefields of Normandy</a>
                                            <a class="news_content_date" href="#"><i class="fa fa-clock-o"></i>March 2, 2016</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <!-- Modal -->
    <div class="modal fade lets-plan" id="lets-plan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $translations[2]?></h4>
                </div>
                <form action="#" method="post" id="lets-plan-form">
                <div class="modal-body">

                        <div class="form-group">
                            <label for="modal_destination"><?php echo $translations[54]?></label>
                            <input type="text" class="form-control" id="modal_destination" name="modal_destination" placeholder="<?php echo $translations[55]?>">
                        </div>
                        <div class="form-group">
                            <label for="modal_travel_plans"><?php echo $translations[56]?></label>
                            <textarea class="form-control" rows="3" id="modal_travel_plans" name="modal_travel_plans" placeholder="<?php echo $translations[57]?>"></textarea>
                        </div>
                        <div class="form-group">
                            <label><?php echo $translations[58]?></label>
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label for="modal_date_to_go"><?php echo $translations[59]?></label>
                                    <select  name="modal_date_to_go" id="modal_date_to_go"  class="form-control">
                                        <option value="<?php echo $translations[60]?>"><?php echo $translations[60]?></option>
                                        <option value="<?php echo $translations[61]?>"><?php echo $translations[61]?></option>
                                        <option value="<?php echo $translations[62]?>"><?php echo $translations[62]?></option>
                                        <option value="<?php echo $translations[63]?>"><?php echo $translations[63]?></option>
                                        <option value="<?php echo $translations[64]?>"><?php echo $translations[64]?></option>
                                        <option value="<?php echo $translations[65]?>"><?php echo $translations[65]?></option>
                                        <option value="<?php echo $translations[66]?>"><?php echo $translations[66]?></option>
                                        <option value="<?php echo $translations[67]?>"><?php echo $translations[67]?></option>
                                        <option value="<?php echo $translations[68]?>"><?php echo $translations[68]?></option>
                                        <option value="<?php echo $translations[69]?>"><?php echo $translations[69]?></option>
                                        <option value="<?php echo $translations[70]?>"><?php echo $translations[70]?></option>
                                        <option value="<?php echo $translations[71]?>"><?php echo $translations[71]?></option>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label for="modal_day_tour"><?php echo $translations[72]?></label>
                                    <input class="form-control" type="number" id="modal_day_tour" name="modal_day_tour" placeholder="<?php echo $translations[73]?>">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label for="modal_count_travelers"><?php echo $translations[74]?></label>
                                    <select name="modal_count_travelers" id="modal_count_travelers" class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="modal_amount"><?php echo $translations[75]?></label>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" class="form-control" id="modal_amount" name="modal_amount" placeholder="<?php echo $translations[90]?>">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" name="modal_currency">
                                        <option>EUR</option>
                                        <option>USD</option>
                                        <option>AMD</option>
                                        <option>RUR</option>
                                        <option>GEL</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="modal_comments"><?php echo $translations[76]?></label>
                            <textarea class="form-control" rows="3" id="modal_comments" name="modal_comments"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="required" for="modal_first_name"><?php echo $translations[77]?></label>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="modal_first_name" name="modal_first_name"  placeholder="<?php echo $translations[78]?>">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="modal_last_name"  placeholder="<?php echo $translations[79]?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label for="form-email" class="required"><?php echo $translations[80]?></label>
                                    <input type="text" class="form-control" name="modal_email" id="form-email" placeholder="<?php echo $translations[81]?>">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label for="form-telephone" class="required"><?php echo $translations[82]?></label>
                                    <input type="text" class="form-control" name="modal_telephone" id="form-telephone" placeholder="<?php echo $translations[83]?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label for="form-email" class="required"><?php echo $translations[113]?></label>
                                    <input type="text" class="form-control" name="modal_captcha">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <a href="#" class="can-not-see"><?php echo $translations[116]?></a>
                                    <img class="img-responsive captcha-image" alt="" src="<?php echo site_url(CAPTCHA_PATH_IMG_PATH . $captcha['filename'])?>">
                                </div>
                            </div>
                        </div>
                        <p class="red-txt">*<?php echo $translations[84]?></p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="button button-gray" data-dismiss="modal"><?php echo $translations[85]?></button>
                    <button type="submit" id="submit-modal" class="button button-blue save-changes-btn"><?php echo $translations[86]?></button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container wallaper">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-11 col-xs-12">

            </div>
        </div>
    </div>

    <?php echo $pageView ?>

</div>

<footer class="footer">
    <div class="container wallaper text-center">
        <div class="row">
            <div class="col-xs-12">
                <nav class="footer-info">
                    <div class="footer_blocks footer_blocks1">
                        <ul>
                            <li class="title">About</li>
                            <li><a href="#">Our team</a></li>
                            <li><a href="#">Blog</a></li>
                            <li class="social">
                                <a href="https://facebook.com/"><i class="fa fa-facebook"></i></a>
                                <a href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer_blocks footer_blocks2">
                        <ul>
                            <li class="title">Services</li>
                            <li><a href="#">Tours</a></li>
                            <li><a href="#">Tickets</a></li>
                            <li><a href="#">Visas</a></li>
                            <li><a href="#">Accommodation</a></li>
                        </ul>
                    </div>
                    <div class="footer_blocks footer_blocks3">
                        <ul>
                            <li class="title">Contact</li>
                            <li><a href="#">Charents str. 23</a></li>
                            <li><a href="#">Yerevan, Armenia</a></li>
                            <li><a href="#">+374 (010) 36-95-95</a></li>
                        </ul>
                    </div>
                    <div class="footer_blocks footer_blocks4">
                        <ul>
                            <li class="title">Get NEW's</li>
                            <li>
                                <form action="#" method="post" id="get_news_form">
                                    <input type="email" name="email" class="footer_email" placeholder="Email Address">
                                    <input type="submit" value="Send" class="footer_send">
                                </form>
                            </li>
                        </ul>
                    </div>

                </nav>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-xs-12">
                <p>&copy; Copyright 2016 ADC LLC | All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
