<video src="/public/video_8.mp4" loop="true" autoplay controls tabindex="0">
    <source src="/public/video_8.mp4" type="video/mp4">
</video>
<div class="col-md-12 no_padding">
    <!--    <div style="margin-top: -1px" class="col-md-12 visas_fon_img no_padding">-->
    <div class="col-md-4 col-md-offset-4 no_padding">
        <div class="no_padding">
            <h1>VISAS</h1>
        </div>
    </div>
</div>
<!--</div>-->
<!--<div class="container wallpaper">-->
<!--    <div class="row see-all hidden">-->
<!--        <div class="col-xs-12 text-center mt20">-->
<!--            <a class="button button-blue dib" href="--><?php //echo site_url($lang) . '/' .SLUG_VISAS_FROM_ARMENIA;?><!--">--><?php //echo $translations[118]?><!--</a>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<?php //echo  $translations[42]?><!--&#160;	&#160; &#8595;<span> See Below</span>-->
<div id="tables_contein" class="no_padding">
    <div>
        <div class="information_visas">
            <div class="col-md-6 information_visa without no_padding ">
                <p><?php echo $translations[40] ?></p>
            </div>
            <div class="col-md-6 information_visa with no_padding">
                <p><?php echo $translations[42] ?></p>
            </div>
        </div>
        <div id="visas-without">
            <table id="visas-outgoing-without">
                <thead>
                <tr>
                    <th class="hidden"></th>
                    <th class="text-center col-md-2"><?php echo $translations[44] ?></th>
                    <th class="text-left col-md-3"><?php echo $translations[43] ?></th>
                    <!--                    <th class="text-center col-md-6">-->
                    <?php //echo $translations[45]?><!--</th>-->
                </tr>
                </thead>
                <tbody class="text-center">

                </tbody>
            </table>
        </div>

        <div id="visas-with">
            <table id="visas-outgoing-with">
                <thead>
                <tr>
                    <th class="hidden"></th>
                    <th class="text-center col-md-2"><?php echo $translations[44] ?></th>
                    <th class="text-left col-md-3"><?php echo $translations[43] ?></th>
                    <!--                    <th class="text-center col-md-6">-->
                    <?php //echo $translations[45]?><!--</th>-->
                </tr>
                </thead>
                <tbody class="text-center">

                </tbody>
            </table>
        </div>

        <div class="go_top col-md-4 col-md-offset-4 text-center">
            <button class="btn">Back TOP <i class="glyphicon glyphicon-arrow-up"></i></button>
        </div>
    </div>
</div>
<script>
    //    $('#tables_contein > div > div.information_visas > div:nth-child(1)').not('.activ_element').addClass('activ_element');
    //    $('#tables_contein > div > div.information_visas > div:nth-child(2) > p span.click_to_see').empty();
    //    $('#tables_contein > div > div.information_visas > div:nth-child(2) > p span.click_to_see').html('<span id="click_to_see">Click to see</span>');

    function no_information(param) {
        var count = $('#' + param + ' > tbody > tr  td:nth-child(4):contains("false")').length;
        for (var i = 1; i <= count; i++) {
            $("#" + param + " > tbody > tr:nth-child(" + i + ")  td:nth-child(4):contains('false')").text('no information');
        }
    }
    $(document).ready(function () {
        $('#visas-without').hide();
        $('#visas-with').show();
        $('#tables_contein > div > div.information_visas > div:nth-child(1)').addClass('activ_element');
        $('#tables_contein > div > div.information_visas > div:nth-child(2) > p').append('<span id="click_to_see">Click to see</span>');

        $('#tables_contein > div > div.information_visas > div:nth-child(1)').on('click', function () {
            if (!$(this).hasClass("activ_element")) {
                $('#tables_contein > div > div.information_visas > div:nth-child(1)').addClass('activ_element');
                $('#tables_contein > div > div.information_visas > div:nth-child(2) > p').append('<span id="click_to_see">Click to see</span>');

                $('#tables_contein > div > div.information_visas > div:nth-child(2)').removeClass('activ_element');
                $('#tables_contein > div > div.information_visas > div:nth-child(1) > p > span').remove();

                $('#visas-without').hide();
                $('#visas-with').show();
            }
        });

        $('#tables_contein > div > div.information_visas > div:nth-child(2)').on('click', function () {
            if (!$(this).hasClass("activ_element")) {
                $('#tables_contein > div > div.information_visas > div:nth-child(2)').addClass('activ_element');

                $('#tables_contein > div > div.information_visas > div:nth-child(1)').removeClass('activ_element');
                $('#tables_contein > div > div.information_visas > div:nth-child(1) > p').not('.activ_element').append('<span id="click_to_see">Click to see</span>');

                $('#tables_contein > div > div.information_visas > div:nth-child(2) > p > span').remove();

                $('#visas-without').show();
                $('#visas-with').hide();
            }

        });
        $('.go_top > button').on('click', function () {
            $("html, body").animate({scrollTop: "0%"}, 2000);
        });
//        #tables_contein > div > div.information_visas > div.col-md-6.information_visa.without.no_padding.activ_element > p
        no_information('visas-outgoing-without');
        no_information('visas-outgoing-with');

//        if(!$('#tables_contein > div > div.information_visas > div.col-md-6.information_visa.with').hasClass('activ_element')){
//            $('#tables_contein > div > div.information_visas > div.col-md-6.information_visa.with').addClass()
//        }
//        var count = $('#visas-outgoing-with > tbody > tr').length;
//        for(var i = 1; i <= count; i++){
//            if(i % 2 == 0){
//                $("#visas-outgoing-with > tbody > tr:nth-child("+i+") > :nth-child(4), #visas-outgoing-with > tbody > tr:nth-child("+i+") > :nth-child(3)").css({
//    //                'background-color': '#EDD49F'
//                })
//            }
//        }
//        var str = $('#tables_contein > div > div.information_visas > div.col-md-6.information_visa.without.no_padding.activ_element > p').text();
//        var res = str.charAt(0);
//        alert(res);


    })
</script>


<!--=====================   hro     =============================-->

<div>
    <!--    <div class="container wallpaper">-->
    <!--        <div class="row see-all hidden">-->
    <!--            <div class="col-xs-12 text-center mt20">-->
    <!--                <a class="button button-blue dib" href="-->
    <?php //echo site_url($lang) . '/' .SLUG_VISAS_FROM_ARMENIA;?><!--">--><?php //echo $translations[118]?><!--</a>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--        <div class="row anchors visas-anchors ">-->
    <!--            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">-->
    <!--                <a class="button button-blue dib without-visa" href="#" >-->
    <?php //echo $translations[120]?><!--</a>-->
    <!--            </div>-->
    <!--            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">-->
    <!--                <a class="button button-blue dib with-visa" href="#" >-->
    <?php //echo $translations[119]?><!--</a>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!---->
    <!--    <div class="container wallaper visas-outgoing-without-container">-->
    <!--        <div>-->
    <!--            <div class="box-out-border">-->
    <!--                <div class="row">-->
    <!--                    <div class="col-xs-12 text-center">-->
    <!--                        <div class="main-title text-center">-->
    <!--                            <div class="out-divider">-->
    <!--                                <h3>--><?php //echo  $translations[40]?><!--</h3>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                        <table id="visas-outgoing-without" class="table table-striped table-bordered table-condensed table-hover dataTable no-footer mt20">-->
    <!--                            <thead>-->
    <!--                            <tr>-->
    <!--                                <th class="hidden"></th>-->
    <!--                                <th class="text-center">--><?php //echo $translations[43]?><!--</th>-->
    <!--                                <th class="text-center">--><?php //echo $translations[44]?><!--</th>-->
    <!--                                <th class="text-center">--><?php //echo $translations[45]?><!--</th>-->
    <!--                            </tr>-->
    <!--                            </thead>-->
    <!--                            <tbody>-->
    <!---->
    <!--                            </tbody>-->
    <!--                        </table>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!---->
    <!---->
    <!--    <div class="container wallaper visas-outgoing-with-container" id="#visas-outgoing-with-container">-->
    <!--        <div>-->
    <!--            <div class="box-out-border">-->
    <!--                <div class="row">-->
    <!--                    <div class="col-xs-12 text-center">-->
    <!--                        <div class="main-title text-center">-->
    <!--                            <div class="out-divider">-->
    <!--                                <h3>--><?php //echo  $translations[42]?><!--</h3>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                        <table id="visas-outgoing-with" class="table table-striped table-bordered table-condensed table-hover dataTable no-footer mt20">-->
    <!--                            <thead>-->
    <!--                            <tr>-->
    <!--                                <th class="hidden"></th>-->
    <!--                                <th class="text-center">--><?php //echo $translations[43]?><!--</th>-->
    <!--                                <th class="text-center">--><?php //echo $translations[44]?><!--</th>-->
    <!--                                <th class="text-center">--><?php //echo $translations[45]?><!--</th>-->
    <!--                            </tr>-->
    <!--                            </thead>-->
    <!--                            <tbody>-->
    <!---->
    <!--                            </tbody>-->
    <!--                        </table>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->

    <!--=================================== HRO end ========================-->
    <!--my views-->
</div>


































