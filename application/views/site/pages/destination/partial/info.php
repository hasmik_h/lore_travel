<div class="map-hover text-center">
    <div class="db">
        <h2 class="dib"><?php echo $info['name'];?></h2>
        <img alt="" class="flag" src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_COUNTRIES . $info['flag']) ?>">
    </div>
    <?php if ($info['thumb']) { ?>
    <div class="box-out-image dib">
        <a href="javascript:;" class="open-fancy" data-params='<?php echo json_encode($gallery);?>'>
            <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_COUNTRIES . $info['thumb']) ?>" alt="" class="img-responsive">
        </a>
    </div>
    <?php } ?>
    <p>
        <?php echo $info['comment_on_hover']?>
    </p>
    <div class="row">
        <div class="col-xs-12">
            <?php if ($info['id'] == COUNTRY_ID_ARMENIA) { ?>
                <div class="col-xs-12 text-center">
                    <a target="_blank" href="<?php echo site_url($lang . '/' . SLUG_INCOMING_TOURS)?>" class="dib mt10 button button-green"><?php echo $see_tours?>&nbsp;<?php echo $info['name'];?></a>
                </div>
            <?php } else {?>
                <?php if ($has_tours) { ?>
                    <div class="col-xs-12 text-center">
                        <a target="_blank" href="<?php echo site_url($lang . '/' . SLUG_OUTGOING_TOURS . '/' . $info['slug'])?>" class="dib mt10 button button-green"><?php echo $see_tours?>&nbsp;<?php echo $info['name'];?></a>
                    </div>
                <?php } elseif ($info['alternative_link']) { ?>
                    <div class="col-xs-12 text-center">
                        <a  href="#" data-toggle="modal" data-target=".lets-plan" class="dib mt10 button button-green "><?php echo $alternative?></a>
                    </div>
                <?php } ?>
                <?php  if ($has_visa) { ?>
                    <div class="col-xs-12 text-center">
                        <a target="_blank" href="<?php echo site_url($lang . '/' . SLUG_VISAS_FROM_ARMENIA . '/' . $info['slug'])?>" class="dib mt10 button button-green"><?php echo $visa_for?><?php echo $info['name'];?></a>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>