<!-- Modal -->
<div class="modal fade" id="our-notes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $info['name'];?></h4>
            </div>
            <div class="modal-body">
              <div class="row">

                      <?php if ($info['id'] == COUNTRY_ID_ARMENIA) { ?>
                  <div class="col-lg-6  margin-bottom-15">
                          <a target="_blank" href="<?php echo site_url($lang . '/' . SLUG_INCOMING_TOURS)?>" class="dib mt10 button button-green pull-left"><?php echo $see_tours?>&nbsp;<?php echo $info['name'];?></a>
                  </div>
                      <?php } else {?>
                          <?php if ($has_tours) { ?>
                  <div class="col-lg-6  margin-bottom-15">
                              <a target="_blank" href="<?php echo site_url($lang . '/' . SLUG_OUTGOING_TOURS . '/' . $info['slug'])?>" class="dib mt10 button button-green pull-left"><?php echo $see_tours?>&nbsp;<?php echo $info['name'];?></a>
                      </div>
                          <?php } elseif ($info['alternative_link']) { ?>
                              <div class="col-lg-6  margin-bottom-15">
                                  <a  href="#" data-toggle="modal" data-target=".lets-plan" class="dib mt10 button button-green pull-left"><?php echo $alternative?></a>
                              </div>
                              <?php } ?>
                          <?php  if ($has_visa) { ?>
                  <div class="col-lg-6 margin-bottom-15">
                              <a target="_blank" href="<?php echo site_url($lang . '/' . SLUG_VISAS_FROM_ARMENIA . '/' . $info['slug'])?>" class="dib mt10 button button-green pull-right"><?php echo $visa_for?><?php echo $info['name'];?></a>
                      </div>
                          <?php } ?>
                      <?php } ?>
                  <div class="clearfix"></div>
                      <div class="mt10">
                          <div class="col-lg-12">
                          <?php echo $info['comment_on_click'];?>
                          </div>
                      </div>

              </div>
                <?php if (count($gallery)) { ?>
                    <div class="gallery_box tours-gallery-box gallery-modal">
                            <div id="images_box" class="clearfix">
                                <?php $i = 1; $galleryCount = count($gallery);?>
                                <?php foreach ($gallery as $image) { ?>
                                    <?php if ($i % 4 == 1) {?>
                                        <div class="row">
                                    <?php } ?>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="box photo">
                                            <a class="open-fancy" href="javascript:;"
                                               data-params='<?php echo json_encode(shiftArray($gallery, $i-1))?>'>
                                                <img src="<?php echo $image['href'] ?>" alt="gallery image">
                                            </a>
                                        </div>
                                    </div>

                                    <?php if ($i++ % 4 == 0 || $i-1 == $galleryCount) { ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>

                            </div> <!-- #images_box -->
                        </div>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="button button-gray" data-dismiss="modal"><?php echo $close?></button>
            </div>
        </div>
    </div>
</div>
