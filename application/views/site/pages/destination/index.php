<!--<link rel="stylesheet" href="--><?//=base_url()?><!--public/css/vendor/responsiveslides.css">-->
<link rel="stylesheet" href="<?=base_url()?>public/css/vendor/themes.css">
<script src="<?=base_url()?>public/js/vendor/slider/responsiveslides.min.js"></script>


<script>

</script>



<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 destination_head">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 destination_head_block">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            <h2 class="title_about"><?php echo $translations[9];?></h2>
            <span>
                The 20 Years experiance in incomming and outgoing tourism <br>
                We know all about the good rest ...
            </span>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            <input id="destination_search" class=" pull-right" placeholder="<?=$translations[152];?>" />
        </div>
    </div>
</div>
<div class="col-lg-7 col-md-10 col-sm-10 col-xs-11 destination_body_block no-padding ">
    <ul class="nav nav-tabs destination_tabs" role="tablist">
        <li role="presentation"><a href="#europe" aria-controls="europe" role="tab" data-toggle="tab"><?=$translations[139];?></a></li>
        <li role="presentation"><a href="#asia" aria-controls="asia" role="tab" data-toggle="tab"><?=$translations[140];?></a></li>
        <li role="presentation"><a href="#africa" aria-controls="africa" role="tab" data-toggle="tab"><?=$translations[141];?></a></li>
        <li role="presentation"><a href="#america" aria-controls="america" role="tab" data-toggle="tab"><?=$translations[142];?></a></li>
        <li role="presentation"><a href="#australia" aria-controls="australia" role="tab" data-toggle="tab" style="margin-right: 0"><?=$translations[143];?></a></li>
    </ul>
</div> <br>
<!-- Tab panes -->

<div class="tab-content">
    <?php for($i=1; $i<=5; $i++):?>
        <?php if($i == 1){$id = 'europe';
            }elseif($i == 2){$id = 'asia';
            }elseif($i == 3){$id = 'africa';
            }elseif($i == 4){$id = 'america';
            }elseif($i == 5){$id = 'australia';}
        ; ?>
        <?php if(isset($pageData) && !empty($pageData["cont".$i])):?>
            <div role="tabpanel" class="tab-pane active" id="<?=$id;?>">
                <?php foreach($pageData["cont".$i] as $data):?>
                    <div class="col-lg-7 col-md-10 col-sm-10 col-xs-11 destination_body_block no-padding ">
                        <a href="#" name="top_2" ></a>
                        <div class="destination_content">
                            <div class="col-lg-12 destination_block no-padding">
                                <div class="col-lg-6 col-md-6 col-xs-12 text-center" >
                                    <h1> <?=$data['title']?> </h1>
                                    <p> <?=substr($data['summary'], 0, 150). '...'?> </p>
                                    <button class="click_to_see"><i class="fa fa-angle-double-down"></i></button>
                                </div>
                                <!--start-images-slider-->
                                <div class="col-lg-5 col-md-5 col-xs-12 pull-left no-padding destination_img ">
                                    <div class="rslides_container">
                                        <ul class="rslides" id="slider3">
                                            <?php foreach($data['files'] as $f):?>
                                                <li><img src="<?=base_url()?><?=$f['path']?>" alt=""></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <!--/slider -->
                                <div class="col-lg-1 col-md-1 col-xs-12 pull-left no-padding" style=" min-height: 20px">
                                </div>
                            </div>
                            <div class="show_more">
                                <div class="destination_info_title col-lg-6 col-xs-12 no-padding">
                                    <h2>  <?=$data['header']?>  </h2>
                                    <span class="yellow_line"></span>
                                </div>
                                <div class="destination_info col-lg-12 col-xs-12">
                                    <p> <?=$data['description']?> </p>
                                    <button class="click_to_see"><i class="fa fa-angle-double-up"></i></button>
                                    <button class="plan_trip pull-right"><a href="<?=base_url()?>en/trip-plan"><i class="fa fa-paper-plane"></i> </a></button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif; ?>
    <?php endfor; ?>
</div>