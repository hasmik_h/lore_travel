<div class="container wallaper">
    <div class="tours-box  col-lg-12">
        <div class="row trip_plan">
            <div class="trip_img">
                <img src="<?=base_url()?>public/img/975b5e831e6b431982c2632038b7cc02.jpg">
            </div>
            <form action="#" method="post" id="lets-plan-form">
                <div class="trip_form">
                    <div class="row">
                        <label class="col-lg-3">Your Name</label>
                        <input class="col-lg-9 trip_inp" type="text" id="modal_first_name" name="modal_first_name" />
                    </div>
                    <div class="row">
                        <label class="col-lg-3">Your Email</label>
                        <input  class="col-lg-9 trip_inp" type="text" name="modal_email" id="form-email" />
                    </div>
                    <div class="row">
                        <label class="col-lg-3">Your Phone </label>
                        <input class="col-lg-9 trip_inp"  type="text" name="modal_telephone" id="form-telephone" />
                    </div>
                </div>
                <div>
                    <h2>Tell us about Your travel plans.</h2>
                    <span class="yellow_line"></span>
                    <textarea class="trip_inp" id="modal_travel_plans" name="modal_travel_plans" ></textarea>
                    <h2>Please, choose Your travel details.</h2>
                    <span class="yellow_line"></span>
                    <div class="col-lg-12 no-padding">
                        <div class="col-lg-4 row trip_form_inp">
                            <input  class="trip_inp" type="text" id="modal_destination" name="modal_destination"  placeholder="<?=$translations[43]?>" >
                        </div>
                        <div class="col-lg-4 row">
                            <select name="modal_date_to_go" id="modal_date_to_go"  class="trip_inp trip_select row">
                                <option value=""><?php echo $translations[59]?></option>
                                <option value="<?php echo $translations[60]?>"><?php echo $translations[60]?></option>
                                <option value="<?php echo $translations[61]?>"><?php echo $translations[61]?></option>
                                <option value="<?php echo $translations[62]?>"><?php echo $translations[62]?></option>
                                <option value="<?php echo $translations[63]?>"><?php echo $translations[63]?></option>
                                <option value="<?php echo $translations[64]?>"><?php echo $translations[64]?></option>
                                <option value="<?php echo $translations[65]?>"><?php echo $translations[65]?></option>
                                <option value="<?php echo $translations[66]?>"><?php echo $translations[66]?></option>
                                <option value="<?php echo $translations[67]?>"><?php echo $translations[67]?></option>
                                <option value="<?php echo $translations[68]?>"><?php echo $translations[68]?></option>
                                <option value="<?php echo $translations[69]?>"><?php echo $translations[69]?></option>
                                <option value="<?php echo $translations[70]?>"><?php echo $translations[70]?></option>
                                <option value="<?php echo $translations[71]?>"><?php echo $translations[71]?></option>
                            </select>
                            <i class="fa fa-caret-down"></i>
                        </div>
                        <div class="col-lg-4 row">
                            <input type="number" id="modal_day_tour" name="modal_day_tour" class="trip_inp trip_select row" placeholder="Days Spent" >
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-4 row">
                                <input type="number" class="trip_inp trip_select row" name="modal_count_travelers" id="modal_count_travelers" placeholder="Number of people" >
                        </div>
                        <div class="col-lg-4 row">
                                <input type="number" id="modal_amount" name="modal_amount"class="trip_inp trip_select row" placeholder="Your total budget" >
                        </div>
                        <div class="col-lg-4 row">
                            <select class="trip_inp trip_select row" name="modal_currency">
                                <option>EUR</option>
                                <option>USD</option>
                                <option>AMD</option>
                                <option>RUR</option>
                                <option>GEL</option>
                            </select>
                            <i class="fa fa-caret-down"></i>
                        </div>

                    </div>
                </div>
                <div class="col-lg-12 row">
                    <input type="submit" id="submit-modal" class="trip_submit" value="S E N D">
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    /*$(function() {
        $(".trip_select").datepicker();
    });*/
</script>