<?php //echo '<pre>';print_r($translations);die; ?>
<div class="container">
    <div class="contact-info">
        <div class="main-title text-center">
            <div class="out-divider contact_title">
                <h3><?php echo $translations[14];?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 contact_block">
<!--                <h3>--><?php //echo $translations[46]?><!--</h3>-->
                <p><?php echo $translations[91]?></p>

                <ul>
                    <li>
                         <?php echo $translations[92]?>
                    </li>
                    <li>
                        <?php echo $translations[93]?>
                    </li>
                </ul>
                <p style="height: 15px"></p>
                <p>
<!--                    <a href="mailto:--><?php //echo $settings['mail_contact_us'];?><!--"><i class="fa fa-envelope-o"></i> --><?php //echo $settings['mail_contact_us'];?><!--</a>-->
                    <?php echo $translations[133]?>
                </p>
                <p><?php echo $translations[134]?></p>

            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div id="g-map"></div>
                <script async defer src="https://maps.googleapis.com/maps/api/js?key= AIzaSyA7trF4LZHrDXKbZD7SrPt-Vp0umlqUawk&signed_in=true&callback=initMap"></script>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 follows_block no-padding">
    <div class="follows_block_container">
        <a href="javascript:void(0)" class="follow_a"><span>Follow Us</span></a>
        <a href="https://web.facebook.com/"><i class="fa fa-facebook"></i></a>
        <a href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
        <a href="https://google.com/"><i class="fa fa-google-plus"></i></i></a>
        <a href="https://youtube.com/"><i class="fa fa-youtube-play"></i></a>
        <a href="https://pinterest.com/"><i class="fa fa-pinterest-p"></i></i></a>
        <a href="https://instagram.com/"><i class="fa fa-instagram"></i></a>
    </div>
</div>

<div class="container">
    <div class="contact-info">
        <div class="main-title text-center">
            <div class="out-divider contact_title">
                <h3><?php echo 'GET IN TOUCH';?></h3>
            </div>
        </div>

        <form id="contact-us-form" method="post">
            <div class="row">
                <div class="form-group">
                    <input type="input" name="contact_us_name" placeholder="<?php echo $translations[77]?>" class="form-control">
                </div>

                <div class="form-group">
                    <input type="input" name="contact_us_email" placeholder="<?php echo $translations[50]?>" class="form-control">
                </div>

                <div class="form-group">
                    <input type="input" name="contact_us_subject" placeholder="<?php echo $translations[51]?>" class="form-control">
                </div>
            </div>
            <div class="row">
                    <textarea class="form-control" name="contact_us_message" placeholder="<?php echo $translations[52]?>" rows="8"></textarea>
            </div>
            <div class="row">
                <div class="form-group">
                        <button type="submit" class="button send-message-contact-us"><?php echo $translations[53]?></button>
                </div>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
</div>
</div>