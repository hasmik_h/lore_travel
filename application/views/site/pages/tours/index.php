<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_head tours_page">
    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 about_head_block">
        <h2 class="title_about"><?php echo $typeDescription;?></h2>
        <span>The 20 Years experiance in incomming and outgoing tourism
                We know all about the good rest ...</span>
        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-12 no-padding search_tours">
            <select class="form-control" id="countries-filter-tours" data-placeholder="<?php echo $translations[32]?>">
            </select>
        </div>
    </div>
</div>

<div class="container wallaper">
    <div class="tours-box mt50">
        <?php if ($type != TOUR_TYPE_INCOMING && $type != TOUR_TYPE_AIRTICKETS) { ?>
        <?php } ?>
        <div class="row mt20">
            <?php foreach ($tours as $tour) { ?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 filterable"
                 <?php if (isset($selectedCountryId) && isset($tourCountryRelations[$tour['id']]) && !in_array($selectedCountryId, $tourCountryRelations[$tour['id']])) {echo 'style="display:none"';}?>
                data-countries='<?php if (isset($tourCountryRelations[$tour['id']])) {echo json_encode($tourCountryRelations[$tour['id']]);} else {echo '[]';}?>'>
                <div class="box">
                    <a href="#<?php //echo site_url($lang . '/tours/' . $tour['slug'])?>">
                        <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_TOURS . $tour['thumb']) ?>" alt="">
                        <div class="team_hover"></div>
                        <div class="team_hover_name ">
                            <?php echo cutAfterN($tour['name'], 20)?> <br>
                            <small> <?php echo cutAfterN(removePFromStartAndBegining($tour['description']), 40)?> </small>
                        </div>
                    </a>
                </div>
            </div>
            <?php } ?>
            </div>
        </div>
    </div>