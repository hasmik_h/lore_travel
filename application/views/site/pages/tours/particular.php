 <div class="container wallaper tour-particular-outgoing">
    <div class="box-out-block">
        <div class="box-out-border" style="padding-bottom:0;">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="main-title text-center">
                        <div class="out-divider">
                            <h3>
                                <?php echo $tour['name']?>
                            </h3>
                        </div>
                    </div>
                    <?php if ($tour['TYPE'] == TOUR_TYPE_INCOMING) { ?>
                    <div class="single-tour mt20">
                        <img class="img-responsive dib" src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_TOURS . $tour['main_img']) ?>" alt="">
                    </div>
                        <p>
                            <a href="#" class="button-green button dib mt10" data-toggle="modal" data-target=".order-tour"><?php echo $translations[94]?></a>
                        </p>
                    <?php } else { ?>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 about-us-info-single">
                                <?php echo ($tour['description_additional'])?>
                            <p>
                                <a href="#" class="button-green button dib mt10" data-toggle="modal" data-target=".order-tour"><?php echo $translations[94]?></a>
                            </p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <img class="img-responsive dib " src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_TOURS . $tour['main_img']) ?>" alt="">
                        </div>
                    <?php } ?>

                    <div class="row">
                        <div class="col-xs-12 mt20">
                        <?php echo ($tour['description_long'])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <?php if (count($gallery)) { ?>
<div class="gallery_box tours-gallery-box">
    <div class="container wallaper">
        <div id="images_box" class="clearfix">
            <?php $i = 1; $galleryCount = count($gallery);?>
            <?php foreach ($gallery as $image) { ?>
            <?php if ($i % 4 == 1) {?>
            <div class="row">
            <?php } ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="box photo">
                        <a class="open-fancy" href="javascript:;"
                           data-params='<?php echo json_encode($gallery)?>'>
                            <img src="<?php echo $image['href'] ?>" alt="gallery image">
                        </a>
                    </div>
                </div>

                <?php if ($i++ % 4 == 0 || $i-1 == $galleryCount) { ?>
                    </div>
                    <?php } ?>
         <?php } ?>

        </div> <!-- #images_box -->
    </div>
    </div>
 <?php } ?>

 <div class="modal fade order-tour" id="order-tour" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title"><?php echo $translations[95]?></h4>
             </div>
             <div class="modal-body">
                 <form action="#" method="post" id="order-form">
                     <div class="form-group">
                         <div class="row">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <label class="required"><?php echo $translations[96]?></label>
                                 <input type="text" class="form-control" name="order_first_name">
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <label class="required"><?php echo $translations[97]?></label>
                                 <input type="text" class="form-control" name="order_last_name">
                             </div>

                         </div>
                     </div>
                     <div class="form-group">
                         <div class="row">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <label class="required"><?php echo $translations[98]?></label>
                                 <input type="text" class="form-control" name="order_travelers_count" placeholder="<?php echo $translations[110]?>">
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <label><?php echo $translations[99]?></label>
                                 <select class="form-control" name="order_room_type">
                                     <option value="<?php echo $translations[100]?>"><?php echo $translations[100]?></option>
                                     <option value="<?php echo $translations[101]?>"><?php echo $translations[101]?></option>
                                     <option value="<?php echo $translations[102]?>"><?php echo $translations[102]?></option>
                                     <option value="<?php echo $translations[103]?>"><?php echo $translations[103]?></option>
                                 </select>
                             </div>
                         </div>
                     </div>
                     <div class="form-group">
                         <label><?php echo $translations[104]?></label>
                         <div class="row">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <input type="text" class="form-control" name="order_date_from"  id="order_date_from" placeholder="<?php echo $translations[105]?>">
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <input type="text" class="form-control" name="order_date_to"  id="order_date_to" placeholder="<?php echo $translations[106]?>">
                             </div>
                         </div>
                     </div>
                     <div class="form-group">
                         <div class="row">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <label  class="required"><?php echo $translations[108]?></label>
                                 <input type="text" class="form-control" name="order_email">
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <label  class="required"><?php echo $translations[107]?></label>
                                 <input type="text" class="form-control" name="order_telephone" >
                             </div>
                         </div>
                     </div>
                     <div class="form-group">
                         <label><?php echo $translations[109]?></label>
                         <textarea class="form-control" rows="3"  name="order_notes"></textarea>
                     </div>
                     <div class="form-group">
                         <div class="row">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <label class="required"><?php echo $translations[113]?></label>
                                 <input type="text" class="form-control" name="order_captcha">
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <a href="#" class="can-not-see"><?php echo $translations[116]?></a>
                                 <img class="img-responsive captcha-image" alt="" src="<?php echo site_url(CAPTCHA_PATH_IMG_PATH . $captcha['filename'])?>">
                             </div>
                         </div>
                     </div>
                     <p class="red-txt">*<?php echo $translations[84]?></p>

             </div>
             <div class="modal-footer">
                 <button type="button" class="button button-gray" data-dismiss="modal"><?php echo $translations[85]?></button>
                 <button type="submit" class="button button-blue send-order-btn"><?php echo $translations[86]?></button>
             </div>
             </form>
         </div>
     </div>
 </div>
