    <div class="container wallaper">
        <div class="box-out-block">
            <div class="box-out-border">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="main-title text-center">
                            <div class="out-divider">
                                <h3>Oooops, the requested page is not found!!!</h3>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3 text-center">
                        <img src="<?php echo site_url(IMAGES_PATH_READ);?>/404.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>


