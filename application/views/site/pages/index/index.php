<div class="slide_container">
    <div class="continent_menus_home pull-left">
        <div class="sub_menu">
            <ul class="nav nav-tabs">
                <li><a href="#europe" aria-controls="europe" role="tab" data-toggle="tab">Europe</a></li>
                <li><a href="#america" aria-controls="america" role="tab" data-toggle="tab">America</a></li>
                <li><a href="#africa" aria-controls="africa" role="tab" data-toggle="tab">Africa</a></li>
                <li><a href="#australia" aria-controls="australia" role="tab" data-toggle="tab">Australia</a></li>
                <li><a href="#asia" aria-controls="asia" role="tab" data-toggle="tab">Asia</a></li>
            </ul>
        </div>
    </div>
    <!--start-images-slider-->
    <div class="images-slider pull-right">
        <div id="fwslider">
            <div class="slider-container">
                <?php foreach($slides as $slide) { ?>
                <div class="slide">
                    <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_SLIDER . $slide['file']) ?>" alt=""/>
                    <div class="slide-content">
                        <div class="slide-content-wrap">
                           <h4 class="title"><?php echo ($slide['title']) ? $slide['title'] : ''?></h4>
                            <p class="description"><?php echo ($slide['content']) ? removePFromStartAndBegining($slide['content']) : ''?></p>
                            <div class="slide-btns description">
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="timers"></div>
            <div class="slidePrev hidden-xs"><span> </span></div>
            <div class="slideNext hidden-xs"><span> </span></div>
        </div>
        <!--/slider -->
    </div>
</div>

<!--<div class="clock_content col-lg-12 no-padding">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center no-padding">
        <div class="clock_title">Yerevan</div>
        <ul id="clock1">
            <li class="sec"></li>
            <li class="hour"></li>
            <li class="min"></li>
        </ul>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center no-padding">
        <div class="clock_title">Moscow</div>
        <ul id="clock2">
            <li class="sec"></li>
            <li class="hour"></li>
            <li class="min"></li>
        </ul>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center no-padding">
        <div class="clock_title">London</div>
        <ul id="clock3">
            <li class="sec"></li>
            <li class="hour"></li>
            <li class="min"></li>
        </ul>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center no-padding">
        <div class="clock_title">New York</div>
        <ul id="clock4">
            <li class="sec"></li>
            <li class="hour"></li>
            <li class="min"></li>
        </ul>
    </div>
</div>
-->
<!--<div class="valute_calculate_block">
    <div class="valute_calculate">
        <h2>Currency Calculator</h2>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 valute_block no-padding">
            <select class="select_valute_from">
                <option value="AMD">AMD</option>
                <option value="USD">USD</option>
                <option value="EUR">EUR</option>
            </select>
            <input type="number" class="valute_from" value="1"/>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 valute_block no-padding">
            <select class="select_valute_to">
                <option value="USD">USD</option>
                <option value="EUR">EUR</option>
                <option value="AMD">AMD</option>
            </select>
            <input type="number" class="valute_to" readonly="readonly"/>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
            <button class="calculate_currency">Calculate</button>
        </div>
    </div>
</div>
-->

<!--<div class="container wallaper home_tours_block">-->
<!--    <div class="sub_menu_section">-->
<!--        <div class="sub_menu">-->
<!--            <ul class="nav nav-tabs">-->
<!--                <li><a href="#europe" aria-controls="europe" role="tab" data-toggle="tab">Europe</a></li>-->
<!--                <li><a href="#america" aria-controls="america" role="tab" data-toggle="tab">America</a></li>-->
<!--                <li><a href="#africa" aria-controls="africa" role="tab" data-toggle="tab">Africa</a></li>-->
<!--                <li><a href="#australia" aria-controls="australia" role="tab" data-toggle="tab">Australia</a></li>-->
<!--                <li><a href="#asia" aria-controls="asia" role="tab" data-toggle="tab">Asia</a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!--        <div class="tab-content">-->
<!--            <div role="tabpanel" id="europe" class="tab-pane news_later active">-->
<!--            <span class="title_section">Europe</span>-->
<!--            <div class="news_later_section continent_slide">-->
<!--                <div class="news_later_box">-->
<!--                    <div class="no-padding img_tour_slide">-->
<!--                        <img src="--><?php //echo base_url();?><!--public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />-->
<!--                    </div>-->
<!--                    <div class="news_content no-padding">-->
<!--                        <a class="news_content_title" href="#">Lorem Ipsum</a>-->
<!--                        <a class="news_content_date" href="#">FROM 2000</a>-->
<!--                    </div>-->
<!--                    <div class="clearfix"></div>-->
<!--                </div>-->
<!--                <div class="news_later_box">-->
<!--                    <div class="no-padding img_tour_slide">-->
<!--                        <img src="--><?php //echo base_url();?><!--public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />-->
<!--                    </div>-->
<!--                    <div class="news_content no-padding">-->
<!--                        <a class="news_content_title" href="#">Lorem Ipsum</a>-->
<!--                        <a class="news_content_date" href="#">FROM 2000</a>-->
<!--                    </div>-->
<!--                    <div class="clearfix"></div>-->
<!--                </div>-->
<!--                <div class="news_later_box">-->
<!--                    <div class="no-padding img_tour_slide">-->
<!--                        <img src="--><?php //echo base_url();?><!--public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />-->
<!--                    </div>-->
<!--                    <div class="news_content no-padding">-->
<!--                        <a class="news_content_title" href="#">Lorem Ipsum</a>-->
<!--                        <a class="news_content_date" href="#">FROM 2000</a>-->
<!--                    </div>-->
<!--                    <div class="clearfix"></div>-->
<!--                </div>-->
<!--                <div class="news_later_box">-->
<!--                    <div class="no-padding img_tour_slide">-->
<!--                        <img src="--><?php //echo base_url();?><!--public/img/photos/a9ff3b_7125cf82b18841c2961ce0a241b78826.jpg"  />-->
<!--                    </div>-->
<!--                    <div class="news_content no-padding">-->
<!--                        <a class="news_content_title" href="#">Lorem Ipsum</a>-->
<!--                        <a class="news_content_date" href="#">FROM 2000</a>-->
<!--                    </div>-->
<!--                    <div class="clearfix"></div>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--            <div role="tabpanel" class="tab-pane" id="america"></div>-->
<!--            <div role="tabpanel" class="tab-pane" id="africa"></div>-->
<!--            <div role="tabpanel" class="tab-pane" id="australia"></div>-->
<!--            <div role="tabpanel" class="tab-pane" id="asia"></div>-->
<!--        </div>-->
<!--        <div class="clearfix"></div>-->
<!--    </div>-->
<!--</div>-->
<div class="special-offers">
    <div class="container wallaper">
        <div class="main-title text-center offer_title">
            <div class="out-divider offer_title">
                <h3><?php echo $translations[15]?></h3>
            </div>
        </div>
    </div>
    <div class="container wallaper">
        <?php foreach($bestOffersOutgoing as $bestOffer) { //echo '<pre>'; print_r($bestOffer);die; ?>
            <div class="tour_item col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <a href="<?php echo $bestOffer['link']?>">
                    <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_BEST_OFFERS . $bestOffer['file']) ?>" alt="special offer">
                </a>
                <div class="grid_item_block">
                    <div>
                        <h2><a href="<?php echo $bestOffer['link']?>"><?=substr($bestOffer['content'],0,50);?></h2>
                        <span class="descr_offer">FROM <strong>$<?=$bestOffer['name'];?></strong></span><br>
                        <a href="#"><button class="view_offer">View</button></a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <div id="carousel-slider3" class="owl-carousel  owl-theme">
        <?php foreach($bestOffersIncoming as $bestOffer) {?>
            <div class="item">
                <p class="mm">
                    <a href="<?php echo $bestOffer['link']?>">
                        <img src="<?php echo site_url(UPLOAD_PATH_READ . UPLOAD_PATH_BEST_OFFERS . $bestOffer['file']) ?>" alt="special offer">
                        <span class="db font-size-12">
                           <?php echo cutAfterN($bestOffer['content'],25)?>
                        </span>
                        <span class="db font-size-12">
                            <?php echo cutAfterN($bestOffer['name'],25)?>
                        </span>
                    </a>
                </p>
            </div>
        <?php } ?>
    </div>

</div>
<div class="col-lg-12 col-xs-12" style="height: 20px; background-color: #72302E">
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_head home_about">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about_head_block">
        <h2 class="title_about"><?php echo $translations[4];?></h2>
        <span><?=$translations[136]?></span>
        <a href="<?=base_url()?>en/about-us" class="view-more-about"><i class="fa fa-chevron-circle-right"></i> View More</a>
    </div>
</div>

<div class="container wallaper">
    <div class="main-title text-center">
        <div class="out-divider">
            <h3><?php echo $translations[89]?></h3>
        </div>
    </div>
    <div class="box-out-block col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <?php $i = 1; $feedbackCount = count($feedbacks);?>
        <?php foreach ($feedbacks as $feedback) { ?>
            <?php if(isOdd($i)) {?>
                <div class="row feedback-row">
            <?php } ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 feedback-item no-padding" <?php if ($i > FEEDBACK_DEFAULT_COUNT) { echo "style='display:none;'";}?>>
                        <div class="box-out <?php if ($i > FEEDBACK_DEFAULT_COUNT) { echo "hidden-feedback'";}?>">
                            <ul>
                                <li>
                                    <p class="feedback-name"> <?php echo $feedback['customer_name']?> </p>
                                </li>
                                <li>
                                    <p class="feedback-comment">
                                        <?php echo removePFromStartAndBegining($feedback['content']) ?>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>

            <?php if(!isOdd($i) || ($i) == $feedbackCount) {?>
                </div>
                <?php } ?>
            <?php $i ++; ?>
           <?php } ?>
        <?php if ($feedbackCount > FEEDBACK_DEFAULT_COUNT) { ?>
        <div class="row">
            <div class="col-xs-12 text-center margin-bottom-15">
                <a href="#" class="dib load-more-feedbacks" style="width: 40px; margin-top: 5px;"> <i class="fa fa-chevron-circle-right"></i></a>
                <a href="#" class="dib load-more-feedbacks"> View More Feedbacks</a>
            </div>
        </div>
        <?php } ?>

        <div class="trip_block_bottom">
            <h2>We are here to Plan your next adventure</h2>
            <p>Fill out our online forme and we will find the best rest for You </p>
            <a href="<?=base_url()?>en/trip-plan"><i class="fa fa-paper-plane"></i></a>
        </div>
    </div>
</div>


<script>
    window.onload = function(e){
        var owl1 = $(".continent_slide");
        owl1.owlCarousel({
            items : 3, //10 items above 1000px browser width
            itemsDesktop : [768,2], //4 items between 1000px and 901px
            itemsDesktopSmall : [500,2], // betweem 900px and 601px
            navigation : true,
            navigationText: [
                "<i class='fa fa-chevron-left'></i>",
                "<i class='fa fa-chevron-right'></i>"
            ],
        });
    }
</script>
