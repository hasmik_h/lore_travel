<div id="about_us_page_main" class="col-md-12 no_padding">
<!--    =========   OUR TEAM    -->
    <div id="about_us_service_page">
        <div>
            <h1>HOW WE WORKING </h1>
        </div>
        <div class="col-md-4 col-md-offset-2 our_service no_padding">

            <div class="no_padding">
                <div class="no_padding">
                    <div class="no_padding">
                        <h2>Our service  information</h2>
                    </div>
                    <div class="no_padding">
                        <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                    </div>
                </div>

                <div class="no_padding">
                    <div class="no_padding">
                        <h2>Our service information</h2>
                    </div>
                    <div class="no_padding">
                        <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                    </div>
                </div>

                <div class="no_padding">
                    <div class="no_padding">
                        <h2>Our service information</h2>
                    </div>
                    <div class="no_padding">
                        <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                    </div>
                </div>

                <div class="no_padding">
                    <div class="no_padding">
                        <h2>Our service information</h2>
                    </div>
                    <div class="no_padding">
                        <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-1 no_padding">
            <img src="<?= base_url() ?>/public/img/about_us/Paris.png" alt="">
        </div>
        <div class="clearfix"></div>
    </div>
<!--=================   -->
<!--    ==============      OUR team    =======================-->
    <div id="team" class="no_padding">
        <div class="no_padding">
            <h1 class="text-center">OUR TEAM</h1>
            <div class="no_padding">
                <div class="col-md-6 no_padding">
                    <img src="<?=base_url()?>/public/img/about_us/team_3.png" alt="">
                </div>
                <div class="col-md-6 no_padding">
                    <p><span>OUR TEAM </span> The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

<!--    ===========================================-->
<!--    ==================      personal =========================-->

    <div id="personal" class="no_padding">
        <div class="col-md-8 col-md-offset-2 no_padding">
            <div class="col-md-8 no_padding">
                <h2>Person</h2>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
            </div>
            <div class="col-md-4 no_padding">
                <img src="<?=base_url()?>/public/img/about_us/person_1.png" alt="">
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2 no_padding">
            <div class="col-md-4 no_padding">
                <img src="<?=base_url()?>/public/img/about_us/person_5.png" alt="">
            </div>
            <div class="col-md-8 no_padding">
                <h2>Person</h2>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2 no_padding">
            <div class="col-md-8 no_padding">
                <h2>Person</h2>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
            </div>
            <div class="col-md-4 no_padding">
                <img src="<?=base_url()?>/public/img/about_us/person_3.png" alt="">
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2 no_padding">
            <div class="col-md-4 no_padding">
                <img src="<?=base_url()?>/public/img/about_us/person_4.png" alt="">
            </div>
            <div class="col-md-8 no_padding">
                <h2>Person</h2>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<!--    ======================== Company name   =============================-->
    <div id="company_name" class="no_padding">
        <div class="col-md-12 no_padding">
            <h1>WHY LORE</h1>
            <p class="col-md-8 col-md-offset-2 no_padding">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 fThe standard chunThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 fThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 fThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 fk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
        </div>
        <div class="clearfix"></div>
    </div>

</div>
<div class="clearfix"></div>







