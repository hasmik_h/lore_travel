<?php

defined('BASEPATH') OR exit('No direct script access allowed');



if ( ! function_exists('addJsFiles'))
{

        function addJsFiles($AdditionalJsFiles)
        {
            $result = '';
            foreach($AdditionalJsFiles as $AdditionalJsFile) {
                $result .= ' <script src="' . base_url() . $AdditionalJsFile . '"></script>' . "\n";
            }
            return $result;
        }
}

if ( ! function_exists('addCssFiles'))
{

    function addCssFiles($AdditionalCssFiles)
    {
        $result = '';
        foreach($AdditionalCssFiles as $AdditionalCssFile) {
            $result .= '<link href="' . base_url() . $AdditionalCssFile . '" rel="stylesheet">' . "\n";
        }
        return $result;
    }
}

if ( ! function_exists('createGlobalJsVariables'))
{

    function createGlobalJsVariables($globalJsVariables)
    {
        $result = '<script>';
        foreach($globalJsVariables as $globalJsVariableName => $globalJsVariableValue) {
            if (is_string($globalJsVariableValue) && !checkIfJson($globalJsVariableValue)) {
                $globalJsVariableValue = "'" . $globalJsVariableValue . "'";
            }
            $result .= "var " . $globalJsVariableName . ' = ' . $globalJsVariableValue .';' . "\n";
        }
        $result .= '</script>';
        return $result;
    }
}

if ( ! function_exists('checkIfJson'))
{
    function checkIfJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}


if ( ! function_exists('requiredAsterisk'))
{
    function requiredAsterisk()
    {
        return '<span class="text-danger">*</span>';
    }
}

if ( ! function_exists('removePFromStartAndBegining'))
{

    function removePFromStartAndBegining($text)
    {
        $text = substr($text, 3);
        $text = substr($text, 0, -4);
        return $text;
    }
}

if ( ! function_exists('isOdd'))
{
    function isOdd($number)
    {
        return ($number % 2 != 0);
    }
}
if ( ! function_exists('makeConvenientArrayForLanguages'))
{
    function makeConvenientArrayForLanguages($languages, $activeLang)
    {
        $langArray = array();
        foreach($languages as $language) {
            $langArray[$language['iso_code']] = $language;
            $langArray[$language['iso_code']] = array(
                'iso_code' => $language['iso_code'],
                'name_origin' => $language['name_origin'],
                'flag' => $language['flag'],
                'href' => site_url(str_replace('/' . $activeLang, '/' . $language['iso_code'], $_SERVER['REQUEST_URI']))
            );
        }
        return $langArray;
    }
}

if ( ! function_exists('getGoogleAnalyticsCode'))
{
    function getGoogleAnalyticsCode()
    {
        if (strpos(base_url(), '.local') === false) {
            return
                "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70503500-1', 'auto');
  ga('send', 'pageview');

            </script>";
        } else {
            return '';
        }

    }
}

if ( ! function_exists('getJivoChatCode'))
{
    function getJivoChatCode()
    {
        if (strpos(base_url(), '.local') === false) {
            return
                "<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'ErqQMfoNQl';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->";
        } else {
            return '';
        }

    }
}

if ( ! function_exists('getMetaData'))
{
    function getMetaData($metaTags)
    {
        $html = '';
        foreach($metaTags as $key => $value) {
            $html .= '<meta name="' . $key . '" content="' . $value . '">';
        }
        return $html;
    }
}

if ( ! function_exists('activeMenu'))
{
    function activeMenu($urls)
    {
        foreach ($urls as $url) {
            if ($_SERVER['REQUEST_URI'] == '/' . $url) {
                return 'active';
            }
        }
        return '';
    }
}

if ( ! function_exists('cutAfterN'))
{
    function cutAfterN($string, $n)
    {
        if (strlen(strip_tags($string)) <= $n) {
            return ($string);
        }
        return substr($string, 0, $n) . '...';
    }
}

if ( ! function_exists('shiftArray'))
{
    function shiftArray($array, $fromIndex)
    {
        if (count($array) == 1) {
            return $array;
        }
        $finalArray = array();
        for ($i = $fromIndex; $i < count($array); $i++) {
            array_push($finalArray, $array[$i]);
        }
        for ($i = 0; $i < $fromIndex; $i++) {
            array_push($finalArray, $array[$i]);
        }
        return $finalArray;

    }
}

