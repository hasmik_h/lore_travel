<?php

defined('BASEPATH') OR exit('No direct script access allowed');



if ( ! function_exists('addJsFiles'))
{

        function addJsFiles($AdditionalJsFiles)
        {
            $result = '';
            foreach($AdditionalJsFiles as $AdditionalJsFile) {
                $result .= ' <script src="' . base_url() . $AdditionalJsFile . '"></script>' . "\n";
            }
            return $result;
        }
}

if ( ! function_exists('addCssFiles'))
{

    function addCssFiles($AdditionalCssFiles)
    {
        $result = '';
        foreach($AdditionalCssFiles as $AdditionalCssFile) {
            $result .= '<link href="' . base_url() . $AdditionalCssFile . '" rel="stylesheet">' . "\n";
        }
        return $result;
    }
}

if ( ! function_exists('createGlobalJsVariables'))
{

    function createGlobalJsVariables($globalJsVariables)
    {
        $result = '<script>';
        foreach($globalJsVariables as $globalJsVariableName => $globalJsVariableValue) {
            if (is_string($globalJsVariableValue) && !checkIfJson($globalJsVariableValue)) {
                $globalJsVariableValue = "'" . $globalJsVariableValue . "'";
            }
            $result .= "var " . $globalJsVariableName . ' = ' . $globalJsVariableValue .';' . "\n";
        }
        $result .= '</script>';
        return $result;
    }
}

if ( ! function_exists('checkIfJson'))
{
    function checkIfJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

if ( ! function_exists('createSortableULForLanguages'))
{
    function createSortableULForLanguages($languages)
    {
        $htmlOutput = '<ul id="active-languages">';

        foreach($languages as $language) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $language['id'] . '"
                data-name-english="' . $language['name_english'] . '"
                data-name-origin="' . $language['name_origin'] . '"
                data-flag="' . $language['flag'] . '"
                data-is-hidden="' . $language['is_hidden'] . '"
                data-is-default="' . $language['is_default'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img src="/public/img/flags/' .$language['flag'] . '" alt="flag">&nbsp;'
                . $language['name_english'] .
                '<a href="#" class="glyphicon glyphicon glyphicon-arrow-left deactivate-language predefined" data-toggle="tooltip" title="' . lang('adc_deactivate') . '"></a>' .
                '<a href="#" class="glyphicon glyphicon-pencil edit-language"></a>' .
                '</li>';
        }

        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('createNotSortableULForLanguages'))
{
    function createNotSortableULForLanguages($languages)
    {
        $htmlOutput = '<ul id="inactive-languages">';

        foreach($languages as $language) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $language['id'] . '"
                data-name-english="' . $language['name_english'] . '"
                data-name-origin="' . $language['name_origin'] . '"
                data-flag="' . $language['flag'] . '"
                data-is-default="0"
                >' .
            '<span class="ui-icon ui-icon-arrowthick-2-n-s not-visible"></span>'
                . '<img src="/public/img/flags/' .$language['flag'] . '" alt="flag">&nbsp;'
                . $language['name_english'] .
                '<a href="#" class="glyphicon glyphicon-arrow-right activate-language" data-toggle="tooltip" title="' . lang('adc_activate') . '"></a>' .
                '</li>';
        }

        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('requiredAsterisk'))
{
    function requiredAsterisk()
    {
        return '<span class="text-danger">*</span>';
    }
}

if ( ! function_exists('explanationSign'))
{
    function explanationSign($explanation, $placement = 'top')
    {
        return '<span class="explanation-question glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="' . $placement .'" title="' . $explanation . '"></span>';
    }
}



if ( ! function_exists('translateLanguage'))
{
    function translateLanguage($activeLanguagesIsoCode)
    {

        return '(' . lang('adc_' . $activeLanguagesIsoCode['iso_code']) . ')';
    }
}

if ( ! function_exists('transformInputLanguage'))
{
    function transformInputLanguage($fieldName, $activeLanguagesIsoCode)
    {
        return $fieldName . '[' . $activeLanguagesIsoCode['iso_code'] . ']';
    }
}

if ( ! function_exists('onlyAlphabeticalCharacters'))
{
    function onlyAlphabeticalCharacters($string)
    {
      return  str_replace("-",'_',str_replace(" ",'_',preg_replace("/[^0-9a-zA-Z _-]/", "", $string)));
    }
}


if ( ! function_exists('createFileName'))
{
    function createFileName($filename, $extension)
    {
        $filename =  str_replace($extension, '', $filename);
        return onlyAlphabeticalCharacters($filename) . '_' .substr(md5(time()), 0, 5) . '.' .$extension;
    }
}

if ( ! function_exists('createSortableULForSlides'))
{
    function createSortableULForSlides($slides)
    {
        $htmlOutput = '<ul id="slides">';

        foreach($slides as $slide) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $slide['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="slider-thumb" src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_SLIDER . $slide['file']) . '" alt="">&nbsp;'
                . '<span class="slide-title">' . $slide['title'] . '</span>'.
                '<a href="#" class="glyphicon glyphicon-remove delete-slide"></a>' .
                '<a href="' . site_url('admin_slider/edit/' . $slide['id']) . '" class="glyphicon glyphicon-pencil edit-slide"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}


if ( ! function_exists('createSortableULForBestOffers'))
{
    function createSortableULForBestOffers($bestOffers)
    {
        $htmlOutput = '<ul id="best-offers">';

        foreach($bestOffers as $bestOffer) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $bestOffer['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="best-offer-thumb" src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_BEST_OFFERS . $bestOffer['file']) . '" alt="">&nbsp;'
                . '<span class="best-offer-description">' . substr($bestOffer['content'], 0, SHOW_TEXT_MAX_LENGTH) . '</span>'.
                '<a href="#" class="glyphicon glyphicon-remove delete-best-offer"></a>' .
                '<a href="' . site_url('admin_best_offers/edit/' . $bestOffer['id']) . '" class="glyphicon glyphicon-pencil edit-best-offer"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}


if ( ! function_exists('createSortableULForOurJobs'))
{
    function createSortableULForOurJobs($ourJobs)
    {
        $htmlOutput = '<ul id="best-offers">';

        foreach($ourJobs as $ourJob) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $ourJob['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="best-offer-thumb" src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_BEST_OFFERS . $ourJob['file']) . '" alt="">&nbsp;'
                . '<span class="best-offer-description">' . substr($ourJob['content'], 0, SHOW_TEXT_MAX_LENGTH) . '</span>'.
                '<a href="#" class="glyphicon glyphicon-remove delete-best-offer"></a>' .
                '<a href="' . site_url('admin_about_us_jobs/edit/' . $ourJob['id']) . '" class="glyphicon glyphicon-pencil edit-best-offer"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('createSortableULForOurTeam'))
{
    function createSortableULForOurTeam($ourTeams)
    {
        $htmlOutput = '<ul id="best-offers">';

        foreach($ourTeams as $ourTeam) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $ourTeam['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="best-offer-thumb" src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_BEST_OFFERS . $ourTeam['file']) . '" alt="">&nbsp;'
                . '<span class="best-offer-description">' . substr($ourTeam['content'], 0, SHOW_TEXT_MAX_LENGTH) . '</span>'.
                '<a href="#" class="glyphicon glyphicon-remove delete-best-offer"></a>' .
                '<a href="' . site_url('admin_our_team/edit/' . $ourTeam['id']) . '" class="glyphicon glyphicon-pencil edit-best-offer"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('createSortableULForOurSertificates'))
{
    function createSortableULForOurSertificates($ourSertificates)
    {
        $htmlOutput = '<ul id="best-offers">';

        foreach($ourSertificates as $ourSertificate) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $ourSertificate['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="best-offer-thumb" src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_BEST_OFFERS . $ourSertificate['file']) . '" alt="">&nbsp;'
                . '<span class="best-offer-description">' . substr($ourSertificate['content'], 0, SHOW_TEXT_MAX_LENGTH) . '</span>'.
                '<a href="#" class="glyphicon glyphicon-remove delete-best-offer"></a>' .
                '<a href="' . site_url('admin_our_sertificates/edit/' . $ourSertificate['id']) . '" class="glyphicon glyphicon-pencil edit-best-offer"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('createSortableULForOurPartners'))
{
    function createSortableULForOurPartners($ourPartners)
    {
        $htmlOutput = '<ul id="best-offers">';

        foreach($ourPartners as $ourPartner) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $ourPartner['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="best-offer-thumb" src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_BEST_OFFERS . $ourPartner['file']) . '" alt="">&nbsp;'
                . '<span class="best-offer-description">' . substr($ourPartner['content'], 0, SHOW_TEXT_MAX_LENGTH) . '</span>'.
                '<a href="#" class="glyphicon glyphicon-remove delete-best-offer"></a>' .
                '<a href="' . site_url('admin_our_partners/edit/' . $ourPartner['id']) . '" class="glyphicon glyphicon-pencil edit-best-offer"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('createSortableULForPartners'))
{
    function createSortableULForPartners($partners)
    {
        $htmlOutput = '<ul id="partners">';

        foreach($partners as $partner) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $partner['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="partner-thumb" src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_PARTNERS . $partner['file']) . '" alt="">&nbsp;'
                .'<a href="#" class="glyphicon glyphicon-remove delete-partner"></a>' .
                '<a href="' . site_url('admin_partners/edit/' . $partner['id']) . '" class="glyphicon glyphicon-pencil edit-partner"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('createSortableULForVisas'))
{
    function createSortableULForVisas($visas, $type, $subtype)
    {
        $htmlOutput = '<ul id="visas">';

        foreach($visas as $visa) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $visa['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="visa-thumb" src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_VISAS . $visa['file']) . '" alt="">&nbsp;'
                . '<span class="visa-description">' . substr($visa['country'], 0, SHOW_TEXT_MAX_LENGTH) . '</span>'.
                '<a href="#" class="glyphicon glyphicon-remove delete-visa"></a>' .
                '<a href="' . site_url('admin_visas/edit/' . $type . '/' . $subtype . '/' . $visa['id']) . '" class="glyphicon glyphicon-pencil edit-visa"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('createSortableULForFeedbacks'))
{
    function createSortableULForFeedbacks($feedbacks)
    {
        $htmlOutput = '<ul id="feedbacks">';

        foreach($feedbacks as $feedback) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $feedback['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<span class="feedback-customer-name">' . $feedback['customer_name'] . '</span>'.
                '<a href="#" class="glyphicon glyphicon-remove delete-feedback"></a>' .
                '<a href="' . site_url('admin_feedbacks/edit/' . $feedback['id']) . '" class="glyphicon glyphicon-pencil edit-feedback"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('createNotSortableULForCountries'))
{
    function createNotSortableULForCountries($countries)
    {
        $htmlOutput = '<ul id="countries">';

        foreach($countries as $country) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $country['id'] . '"
                >'
                . '<img class="flag-thumb" src="'. site_url(UPLOAD_PATH_READ . UPLOAD_PATH_COUNTRIES . $country['flag']) .'" alt="flag">&nbsp;'
                . $country['name'] .
                '<a href="#" class="glyphicon glyphicon-remove delete-country"></a>' .
                '<a href="' . site_url('admin_countries/edit/' . $country['id']) . '" class="glyphicon glyphicon-pencil edit-country"></a>' .
                '<a href="' . site_url('admin_countries/gallery/' . $country['id']) . '" class="glyphicon glyphicon-picture gallery-country" data-toggle="tooltip" title="' . lang('adc_gallery') . '"></a>' .
                '</li>';
        }

        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('createSortableULForTours'))
{
    function createSortableULForTours($tours, $type)
    {
        $htmlOutput = '<ul id="tours">';

        foreach($tours as $tour) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $tour['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="tour-thumb" src="'. site_url(UPLOAD_PATH_READ . UPLOAD_PATH_TOURS . $tour['thumb']) .'" alt="flag">&nbsp;'
                . $tour['name'] .
                '<a href="#" class="glyphicon glyphicon-remove delete-tour"></a>' .
                '<a href="' . site_url('admin_tours/edit/' . $type . '/' . $tour['id']) . '" class="glyphicon glyphicon-pencil edit-tour"></a>' .
                '<a href="' . site_url('admin_tours/gallery/' . $type . '/' .$tour['id']) . '" class="glyphicon glyphicon-picture gallery-tour" data-toggle="tooltip" title="' . lang('adc_gallery') . '"></a>' .
                '</li>';
        }

        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('hmBreadcrumbs'))
{
    function hmBreadcrumbs($paths)
    {
        $htmlOutputArray = array();
        foreach ($paths as $text => $link) {
            if (!is_string($text)) {
                $item = '<span class="breadcrumbs-span">' . $link . '</span>';
            } else {
                $item = '<a class="breadcrumbs-a" href="' . site_url($link) . '">' . $text . '</a>';
            }
            array_push($htmlOutputArray, $item);
        }
        return implode(' - ', $htmlOutputArray);
    }
}

if ( ! function_exists('createSortableULForCountryGallery'))
{
    function createSortableULForCountryGallery($countryImages, $countryId)
    {
        $htmlOutput = '<ul id="country-gallery">';

        foreach($countryImages as $image) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $image['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="country-gallery-thumb" src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_COUNTRY_GALLERY . $image['file']) . '" alt="">&nbsp;'
                . '<span class="country-gallery-image-description">' . substr($image['content'], 0, SHOW_TEXT_MAX_LENGTH) . '</span>'.
                '<a href="#" class="glyphicon glyphicon-remove delete-country-gallery-image"></a>' .
                '<a href="' . site_url('admin_countries/gallery_edit/' . $countryId . '/' . $image['id']) . '" class="glyphicon glyphicon-pencil edit-country-gallery-image"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}

if ( ! function_exists('createSortableULForTourGallery'))
{
    function createSortableULForTourGallery($tourType, $tourImages, $tourId)
    {
        $htmlOutput = '<ul id="tour-gallery">';

        foreach($tourImages as $image) {
            $htmlOutput .=
                '<li class="ui-state-default"
                data-id="' . $image['id'] . '"
                >' .
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                . '<img class="tour-gallery-thumb" src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_TOUR_GALLERY . $image['file']) . '" alt="">&nbsp;'
                . '<span class="tour-gallery-image-description">' . substr($image['content'], 0, SHOW_TEXT_MAX_LENGTH) . '</span>'.
                '<a href="#" class="glyphicon glyphicon-remove delete-tour-gallery-image"></a>' .
                '<a href="' . site_url('admin_tours/gallery_edit/' . $tourType . '/' . $tourId . '/' . $image['id']) . '" class="glyphicon glyphicon-pencil edit-tour-gallery-image"></a>' .
                '</li>';
        }
        $htmlOutput .= '</ul>';

        return $htmlOutput;
    }
}
