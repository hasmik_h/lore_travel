<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


define('SHOW_TEXT_MAX_LENGTH', 100);


/**
 * User Roles
 */
define('ROLE_USER_ADMIN', 1);

/**
 * Upload Paths
 */
define('UPLOAD_PATH', './public/uploads/');
define('UPLOAD_PATH_SLIDER', 'slider/');
define('UPLOAD_PATH_COUNTRIES', 'countries/');
define('UPLOAD_PATH_COUNTRY_GALLERY', 'country-gallery/');
define('UPLOAD_PATH_TOURS', 'tours/');
define('UPLOAD_PATH_TOUR_GALLERY', 'tour-gallery/');
define('UPLOAD_PATH_BEST_OFFERS', 'best-offers/');
define('UPLOAD_PATH_PARTNERS', 'partners/');
define('UPLOAD_PATH_SETTINGS', 'settings/');
define('UPLOAD_PATH_ABOUT_US', 'about-us/');
define('UPLOAD_PATH_VISAS', 'visas/');

/**
 * captcha
 */
define('CAPTCHA_PATH_IMG_PATH', 'public/captcha/');


define('IMAGES_PATH_READ', 'public/img/');

/**
 * Upload Paths to read
 */
define('UPLOAD_PATH_READ', 'public/uploads/');

/**
 * Paths for unlink the file
 */
define('UNLINK_PATH_UPLOADS', APPPATH . '../public/uploads/');

/**
 * File Sizes
 */
define('FILE_SIZE_GENERAL', 2097152 *5 ); //10MB


/*
 * Image Sizes
 */
define('SLIDER_WIDTH', 1970);
define('SLIDER_HEIGHT', 700);

define('FLAG_WIDTH', 18);
define('FLAG_HEIGHT', 12);

define('COUNTRY_THUMB_WIDTH', 250);
define('COUNTRY_THUMB_HEIGHT', 250);

define('TOUR_THUMB_WIDTH', 390);
define('TOUR_THUMB_HEIGHT', 240);

define('TOUR_MAIN_IMAGE_WIDTH', 600);
define('TOUR_MAIN_IMAGE_HEIGHT', 300);

define('TOUR_GALLERY_IMAGE_WIDTH', 800);
define('TOUR_GALLERY_IMAGE_HEIGHT', 500);

define('BEST_OFFER_WIDTH', 265);
define('BEST_OFFER_HEIGHT', 200);

define('OG_IMAGE_WIDTH', 360);
define('OG_IMAGE_HEIGHT', 360);

/*
 * Database Tables
 */
define('TBL_USER', 'adc_user');
define('TBL_LANGUAGES', 'adc_languages');

define('TBL_UNIVERSAL_TEXT', 'adc_universal_text');
define('TBL_UNIVERSAL_TEXT_TRANSLATIONS', 'adc_universal_text_translations');

define('TBL_SLIDER', 'adc_slider');
define('TBL_SLIDER_TRANSLATIONS', 'adc_slider_translations');

define('TBL_COUNTRIES', 'adc_countries');
define('TBL_COUNTRY_TRANSLATIONS', 'adc_country_translations');

define('TBL_COUNTRY_GALLERY', 'adc_country_gallery');
define('TBL_COUNTRY_GALLERY_TRANSLATIONS', 'adc_country_gallery_translations');

define('TBL_TOURS', 'adc_tours');
define('TBL_TOUR_TRANSLATIONS', 'adc_tour_translations');

define('TBL_TOUR_GALLERY', 'adc_tour_gallery');
define('TBL_TOUR_GALLERY_TRANSLATIONS', 'adc_tour_gallery_translations');

define('TBL_TOUR_COUNTRY', 'adc_tour_country');

define('TBL_BEST_OFFERS', 'adc_best_offers');
define('TBL_BEST_OFFER_TRANSLATIONS', 'adc_best_offer_translations');

define('TBL_OUR_TEAM', 'adc_our_team');
define('TBL_OUR_TEAM_TRANSLATIONS', 'adc_our_team_translations');

define('TBL_ABOUT_US_JOBS', 'adc_about_us_jobs');
define('TBL_ABOUT_US_JOBS_TRANSLATIONS', 'adc_about_us_jobs_translations');

define('TBL_OUR_SERTIFICATES', 'adc_our_sertificates');
define('TBL_OUR_SERTIFICATES_TRANSLATIONS', 'adc_our_sertificates_translations');

define('TBL_OUR_PARTNERS', 'adc_our_partners');
define('TBL_OUR_PARTNERS_TRANSLATIONS', 'adc_our_partners_translations');

define('TBL_FEEDBACKS', 'adc_feedbacks');
define('TBL_FEEDBACK_TRANSLATIONS', 'adc_feedback_translations');

define('TBL_ABOUT_US', 'adc_about_us');
define('TBL_ABOUT_US_TRANSLATIONS', 'adc_about_us_translations');

define('TBL_VISAS', 'adc_visas');
define('TBL_VISA_TRANSLATIONS', 'adc_visa_translations');

define('TBL_COMMON_SETTINGS', 'adc_common_settings');
define('TBL_COMMON_SETTINGS_TRANSLATIONS', 'adc_common_settings_translations');

define('TBL_PARTNERS', 'adc_partners');


/**
 * Slugs for pages
 */

define('SLUG_HOME', 'home');
define('SLUG_ABOUT_US', 'about-us');
define('SLUG_INCOMING_TOURS', 'incoming-tours');
define('SLUG_OUTGOING_TOURS', 'outgoing-tours');
define('SLUG_EDUCATIONAL_TOURS', 'educational-tours');
define('SLUG_AIRTICKETS', 'airtickets');
define('SLUG_DESTINATION', 'destination');
define('SLUG_BOOK_HOTEL', 'book-hotel');
define('SLUG_VISAS_TO_ARMENIA', 'visas');
define('SLUG_VISAS_FROM_ARMENIA', 'visas-from-armenia');
define('SLUG_NOT_DECIDED', 'not-decided');
define('SLUG_CONTACT_US', 'contact-us');
define('SLUG_TRIP_PLAN', 'trip-plan');

/**
 * Tour Types
 */

define('TOUR_TYPE_INCOMING', 1);
define('TOUR_TYPE_OUTGOING', 2);
define('TOUR_TYPE_EDUCATIONAL', 3);
define('TOUR_TYPE_AIRTICKETS', 4);


/**
 * About us
 */

define('ABOUT_US_GLOBE', 1);
define('ABOUT_US_CARLSON_WAGONLIT', 2);
define('ABOUT_US_HOTELS_PRO', 3);
define('ABOUT_US_WHY_US', 4);

/**
 * Visas
 */
define('VISA_TYPE_TO_ARMENIA', 1);
define('VISA_TYPE_FROM_ARMENIA', 2);

define('VISA_SUBTYPE_WITHOUT', 1);
define('VISA_SUBTYPE_BEFORE', 3);


/**
 * other
 */
define('COUNTRY_ID_ARMENIA', 13);
define('AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING', '(en|hy|ru|ge|fr|es|de|it|tr|kz|by|gr)');
define('FEEDBACK_DEFAULT_COUNT', 2);

define('MAIL_SENDER', 'developerarturpoghosyantest@gmail.com');

define('BEST_OFFER_TYPE_INCOMING', 2);
define('BEST_OFFER_TYPE_OUTGOING', 1);

define('PARTNER_TYPE_SIMPLE', 1);
define('PARTNER_TYPE_EDUCATIONAL', 2);







