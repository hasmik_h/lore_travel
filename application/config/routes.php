<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = 'home/_404';
$route['translate_uri_dashes'] = FALSE;
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING] = 'home/index/$1';
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_ABOUT_US] = 'about_us/index/$1';
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_CONTACT_US] = 'contact_us/index/$1';
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_TRIP_PLAN] = 'trip_plan/index/$1';
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_VISAS_TO_ARMENIA] = 'visas/index/$1/' . VISA_TYPE_TO_ARMENIA;
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_VISAS_FROM_ARMENIA] = 'visas/index/$1/' . VISA_TYPE_FROM_ARMENIA;
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_VISAS_FROM_ARMENIA  . '/(:any)'] = 'visas/index/$1/' . VISA_TYPE_FROM_ARMENIA . '/$2';
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_INCOMING_TOURS] = 'tours/index/$1/' . TOUR_TYPE_INCOMING;
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_OUTGOING_TOURS] = 'tours/index/$1/' . TOUR_TYPE_OUTGOING;
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_AIRTICKETS] = 'tours/index/$1/' . TOUR_TYPE_AIRTICKETS;
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_EDUCATIONAL_TOURS] = 'tours/index/$1/' . TOUR_TYPE_EDUCATIONAL;
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_INCOMING_TOURS . '/(:any)'] = 'tours/index/$1/' . TOUR_TYPE_INCOMING . '/$2';
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_OUTGOING_TOURS . '/(:any)'] = 'tours/index/$1/' . TOUR_TYPE_OUTGOING . '/$2';
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_EDUCATIONAL_TOURS . '/(:any)'] = 'tours/index/$1/' . TOUR_TYPE_EDUCATIONAL . '/$2';
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/tours/(:any)'] = 'tours/particular/$1/$2';
$route[AVAILABLE_LANGUAGES_ISO_CODES_FOR_ROUTING . '/' . SLUG_DESTINATION] = 'destination/index/$1';