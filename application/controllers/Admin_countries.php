<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_countries extends ADC_admin {
    public function __construct() {
        parent::__construct();
        $this->load->model('destination_model');
    }

    public function destination($id = null) {
        if($id){
            $pageData['destination']  = $this->destination_model->getDestinationById($id);
            $data ['pageData'] = $pageData ;
//            echo "pre"; print_r($data); die;
            $this->render('destination/index', $data);
        } else {

            $this->render('destination/index');
        }

    }

    public function add(){
        $imgs = $this->upload_files($_FILES['guide_imgs']);
        $this->destination_model->add($this->input->post('continent'),$this->input->post('title'), $this->input->post('summary'), $this->input->post('header'), $this->input->post('description'), $imgs);
        $this->upload_files($_FILES['guide_imgs']);
        header('Location: '.base_url().'admin_countries/destination_list');
        exit;
    }

    public function destination_list() {
        $pageData['destination']  = $this->destination_model->getDestinationList();
        $data ['pageData'] = $pageData ;
        $this->render('destination/list', $data);
    }

    protected function upload_files($files, $limit = 50 ) {
        // $type = $this->dwnTypes;
        $path = 'public/uploads/destination';
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);
        $images = array();
        $datas = array();
        foreach ($files['name'] as $key => $image) {
            if($limit--  > 0   ) {
                $_FILES['images[]']['name'] = $files['name'][$key];
                $_FILES['images[]']['type'] = $files['type'][$key];
                $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
                $_FILES['images[]']['error'] = $files['error'][$key];
                $_FILES['images[]']['size'] = $files['size'][$key];

                if ($this->upload->do_upload('images[]')) {
                    $data = $this->upload->data();
                    $file_path = $path . "/" . $data["file_name"];
                    array_push($datas, array(
                            "name" => $data["orig_name"],
                            "type" => $data["file_type"],
                            "size" => $data["file_size"],
                            "path" => $file_path,
                            "full_path" => $data["full_path"],
                            "date" => time())
                    );
                }
            }
        }
        return $datas;
    }

    public function delete(){
        $result['status'] = false;
        $delete_id = $this->input->post("id");
        $del = $this->destination_model->deleteById($delete_id);
        if($del){
            $result['status'] = true;
        }
        echo json_encode($result);
    }

    public function delete_img(){
        $result['status'] = false;
        $delete_id = $this->input->post("id");
        $del = $this->destination_model->deleteImageById($delete_id);
        unlink($this->input->post("path"));
        if($del){
            $result['status'] = true;
        }
        echo json_encode($result);
    }

    public function edit(){
        $imgs = $this->upload_files($_FILES['guide_imgs']);
        $edit = $this->destination_model->edit($this->input->post('continent'),$this->input->post('title'), $this->input->post('summary'), $this->input->post('header'), $this->input->post('description'), $imgs,  $this->input->post('parent_id'));
        $this->upload_files($_FILES['guide_imgs']);
        if($edit){
            header('Location: '.base_url().'admin_countries/destination_list');
            exit;
        }

    }
}


