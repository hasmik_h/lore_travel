<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_partners extends ADC_admin
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('partners_model');

    }

    public function index()
    {
        $partners = $this->partners_model->getAllPartners();
        $data = array(
            'additionalJsFiles' => array(
                'public/js/vendor/jquery-ui.js',
                'public/js/pages/admin/partners/index.js'
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css',
                'public/css/pages/admin/partners/index.css'
            ),
            'pageData' => array(
                'partners' => $partners
            ),
            'globalJsVariables' => array(
                'GLOBAL_SAVE_ORDER_URL' => site_url('admin_partners/save_order'),
                'GLOBAL_DELETE_PARTNER' => site_url('admin_partners/delete'),
            )
        );

        $this->render('partners/index', $data);
    }

    public function edit($id = 0)
    {
        if ($id) {
            $basicInfo = $this->partners_model->getBasicInfo($id);
            $basicInfo = $this->makeConvenientArray($basicInfo, array());
            if (empty($basicInfo)) {
                $this->setFlashMessage('error', lang('adc_item_does_not_exist'));
            }
        }
        try {
            if (isset($_FILES['image']['name']) && $_FILES['image']['error'] && $_FILES['image']['error'] != UPLOAD_ERR_NO_FILE) {
                $this->setFlashMessage('error', lang('adc_server_error'));
                throw new Exception();
            }
            if (isset($_FILES['image']['name']) && !empty($_FILES['image']['tmp_name'])) {
                $config['upload_path'] = UPLOAD_PATH . UPLOAD_PATH_PARTNERS;
                $config['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                $config['max_size'] = FILE_SIZE_GENERAL; //10MB
                $filenameArray = explode(".", $_FILES['image']['name']);
                $ext = end($filenameArray);
                //sometimes this library does not do right validation
                //so this is an additional check
                if (!in_array($ext, explode('|', $config['allowed_types']))) {
                    $this->setFlashMessage('error', sprintf(lang('adc_not_allowed_file_type'), $config['allowed_types']));
                    throw new Exception();
                }
                if ($_FILES['image']['size'] > FILE_SIZE_GENERAL) {
                    $this->setFlashMessage('error', sprintf(lang('adc_not_allowed_file_size'), (int)$config['max_size'] / 1024 / 1024));
                    throw new Exception();
                }
                $config['file_name'] = createFileName($_FILES['image']['name'], $ext);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $this->setFlashMessage('error', $this->upload->display_errors());
                    throw new Exception();
                }
                if ($id) {
                    //deleting old file
                    @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_PARTNERS . $basicInfo['file']);
                }
            }
            if (!empty($_POST)) {
                if ($id == 0) {
                    $res = $this->partners_model->add(
                        $this->input->post('link'),
                        $this->input->post('simple_educational'),
                        $config['file_name']);
                    if ($res == false) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $this->setFlashMessage('success', lang('adc_successfully_added'));
                    redirect('admin_partners/edit/' . $res);
                } else {
                    $fileName = (isset($config['file_name'])) ? $config['file_name'] : false;
                    if (!$this->partners_model->update(
                        $this->input->post('link'),
                        $this->input->post('simple_educational'),
                        $id,
                        $fileName
                    )
                    ) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $basicInfo = $this->partners_model->getBasicInfo($id);
                    $basicInfo = $this->makeConvenientArray($basicInfo, array());
                    $this->setFlashMessage('success', lang('adc_successfully_updated'));
                }
            }
        } catch (Exception $ex) {
            redirect('admin_partners');
        }

        $data = array(
            'additionalJsFiles' => array(
                'public/js/pages/admin/partners/edit.js'
            ),
            'globalJsVariables' => array(
                'isEdit' => ($id) ? 1 : 0
            )
        );
        if ($id) {
            $data['pageData'] = $basicInfo;
        }
        $this->render('partners/edit', $data);
    }

    public function save_order()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            if (!$this->partners_model->saveOrder($this->input->post('ids'))) {
                throw new Exception();
            }
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_updated')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function delete()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            $fileName = $this->partners_model->getFileNameById($this->input->post('id'));
            @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_PARTNERS . $fileName);
            $this->partners_model->deletePartner($this->input->post('id'));
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_removed')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


}
