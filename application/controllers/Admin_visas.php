<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_visas extends ADC_admin
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('visas_model');

    }

    public function list_of_visas($type, $subtype)
    {
        $visas = $this->visas_model->getAllVisasByTypeAndSubtype($this->getDefaultLanguageIsoCode(), $type, $subtype);
        $data = array(
            'additionalJsFiles' => array(
                'public/js/vendor/jquery-ui.js',
                'public/js/pages/admin/visas/index.js'
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css',
                'public/css/pages/admin/visas/index.css'
            ),
            'pageData' => array(
                'visas' => $visas,
                'typeText' => $this->visas_model->mapType($type),
                'subtypeText' => $this->visas_model->mapSubtype($subtype),
                'type' => $type,
                'subtype' => $subtype
            ),
            'globalJsVariables' => array(
                'GLOBAL_SAVE_ORDER_URL' => site_url('admin_visas/save_order'),
                'GLOBAL_DELETE_VISA' => site_url('admin_visas/delete'),
            )
        );

        $this->render('visas/index', $data);
    }

    public function edit($type, $subtype, $id = 0)
    {
        if ($id) {
            $basicInfo = $this->visas_model->getBasicInfo($id);
            $basicInfo = $this->makeConvenientArray($basicInfo, array('content','country'));
            if (empty($basicInfo)) {
                $this->setFlashMessage('error', lang('adc_item_does_not_exist'));
            }
        }
        try {
            if (isset($_FILES['image']['name']) && $_FILES['image']['error'] && $_FILES['image']['error'] != UPLOAD_ERR_NO_FILE) {
                $this->setFlashMessage('error', lang('adc_server_error'));
                throw new Exception();
            }
            if (isset($_FILES['image']['name']) && !empty($_FILES['image']['tmp_name'])) {
                $config['upload_path'] = UPLOAD_PATH . UPLOAD_PATH_VISAS;
                $config['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                $config['max_size'] = FILE_SIZE_GENERAL; //10MB
                $filenameArray = explode(".", $_FILES['image']['name']);
                $ext = end($filenameArray);
                //sometimes this library does not do right validation
                //so this is an additional check
                if (!in_array($ext, explode('|', $config['allowed_types']))) {
                    $this->setFlashMessage('error', sprintf(lang('adc_not_allowed_file_type'), $config['allowed_types']));
                    throw new Exception();
                }
                if ($_FILES['image']['size'] > FILE_SIZE_GENERAL) {
                    $this->setFlashMessage('error', sprintf(lang('adc_not_allowed_file_size'), (int)$config['max_size'] / 1024 / 1024));
                    throw new Exception();
                }
                $config['file_name'] = createFileName($_FILES['image']['name'], $ext);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $this->setFlashMessage('error', $this->upload->display_errors());
                    throw new Exception();
                }
                $imageConfig['image_library'] = 'gd2';
                $imageConfig['source_image'] = UPLOAD_PATH . UPLOAD_PATH_VISAS . $config['file_name'];
                $imageConfig['create_thumb'] = FALSE;
                $imageConfig['maintain_ratio'] = FALSE;
                $imageConfig['width'] = FLAG_WIDTH;
                $imageConfig['height'] = FLAG_HEIGHT;
                $this->load->library('image_lib', $imageConfig);
                if (!$this->image_lib->resize()) {
                    $this->setFlashMessage('error', $this->image_lib->display_errors());
                    throw new Exception();
                }

                if ($id) {
                    //deleting old file
                    @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_VISAS . $basicInfo['file']);
                }
            }
            if (!empty($_POST)) {
                if ($id == 0) {
                    $res = $this->visas_model->add(
                        $this->input->post('text'),
                        $this->input->post('country'),
                        $this->input->post('country_id'),
                        $type,
                        $subtype,
                        $config['file_name']);
                    if ($res == false) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $this->setFlashMessage('success', lang('adc_successfully_added'));
                    redirect('admin_visas/edit/' . $type . '/' . $subtype . '/' . $res);
                } else {
                    $fileName = (isset($config['file_name'])) ? $config['file_name'] : false;
                    if (!$this->visas_model->update(
                        $this->input->post('text'),
                        $this->input->post('country'),
                        $this->input->post('country_id'),
                        $id,
                        $fileName
                    )
                    ) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $basicInfo = $this->visas_model->getBasicInfo($id);
                    $basicInfo = $this->makeConvenientArray($basicInfo, array('content', 'country'));
                    $this->setFlashMessage('success', lang('adc_successfully_updated'));
                }
            }
        } catch (Exception $ex) {
            redirect('admin_visas');
        }

        $data = array(
            'additionalJsFiles' => array(
                'public/js/pages/admin/visas/edit.js'
            ),
            'globalJsVariables' => array(
                'isEdit' => ($id) ? 1 : 0
            )
        );
        $data['pageData'] = array();
        if ($id) {
            $data['pageData'] = $basicInfo;
        }
        $data['pageData']['typeText'] = $this->visas_model->mapType($type);
        $data['pageData']['subtypeText'] =$this->visas_model->mapSubtype($subtype);
        $this->load->model('countries_model');
        $countriesNotSetForSelect = $this->countries_model->getCountriesForSelect($this->getDefaultLanguageIsoCode());
        $data['pageData']['countriesNotSetForSelect'] = $countriesNotSetForSelect;
        $data['pageData']['subtypeText'] = $this->visas_model->mapSubtype($subtype);
        $data['pageData']['type'] = $type;

        $this->render('visas/edit', $data);
    }

    public function save_order()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            if (!$this->visas_model->saveOrder($this->input->post('ids'))) {
                throw new Exception();
            }
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_updated')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function delete()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            $fileName = $this->visas_model->getFileNameById($this->input->post('id'));
            @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_VISAS . $fileName);
            $this->visas_model->deleteVisa($this->input->post('id'));
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_removed')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


}
