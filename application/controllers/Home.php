<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_site.php');

class Home extends ADC_site
{

    public function index($lang = false)
    {
       // echo $this->get_currency('USD', 'AMD', 1);die;
        if (!$lang) {
            redirect('/' . $this->getDefaultLanguageIsoCode());
        }
        $this->preloadTranslations(array(15,16,17,89), $lang);
        $this->load->model('slider_model');
        $this->load->model('best_offers_model');
        $this->load->model('feedbacks_model');
        $slides = $this->slider_model->getAllSlides($lang);
        $bestOffersOutgoing = $this->best_offers_model->getAllBestOffersByType($lang, BEST_OFFER_TYPE_OUTGOING);
        $bestOffersIncoming = $this->best_offers_model->getAllBestOffersByType($lang, BEST_OFFER_TYPE_INCOMING);
        $feedbacks = $this->feedbacks_model->getAllFeedbacks($lang);
        $data = array(
            'pageData' => array(
                'bestOffersOutgoing' => $bestOffersOutgoing,
                'bestOffersIncoming' => $bestOffersIncoming,
                'slides' => $slides,
                'feedbacks' => $feedbacks,
            ),
        );
        $this->render('index/index', $lang, $data);

    }

    function get_currency() {

        if(!empty($this->input->post())){
            $amount = $this->input->post('amount');
            $from_Currency = $this->input->post('from');
            $to_Currency = $this->input->post('to');
        }else{
            $amount = 1;
            $from_Currency = 'USD';
            $to_Currency = 'AMD';
        }
        $from_Currency = urlencode($from_Currency);
        $to_Currency = urlencode($to_Currency);

        $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

        $ch = curl_init();
        $timeout = 0;
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt ($ch, CURLOPT_USERAGENT,
            "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $rawdata = curl_exec($ch);
        curl_close($ch);
        $data = explode('bld>', $rawdata);
        $data = explode($to_Currency, $data[1]);
        echo json_encode(round($data[0], 2));die;
        //return round($data[0], 2);
    }

// Call the function to get the currency converted


    public function _404()
    {
        $this->preloadTranslations(array(), $this->getDefaultLanguageIsoCode());
        $this->render('index/404', $this->getDefaultLanguageIsoCode(), array());
    }


}
