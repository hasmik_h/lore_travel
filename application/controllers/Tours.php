<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_site.php');


class Tours extends ADC_site
{


    public function index($lang, $type, $countrySlug = false)
    {
        $this->preloadTranslations(array(30, 31, 33, 32, 130), $lang);
        $this->load->model('tours_model');
        $this->load->model('countries_model');
        $toursAndRelations = $this->tours_model->getAllToursByTypeAndCountry($lang, $type);
        $countries = $this->countries_model->getCountriesForSelectize($lang);
        if ($type == TOUR_TYPE_INCOMING) {
            $typeDescription = $this->translations[30];
        } elseif ($type == TOUR_TYPE_OUTGOING) {
            $typeDescription = $this->translations[31];
        }  elseif ($type == TOUR_TYPE_AIRTICKETS) {
            $typeDescription = $this->translations[130];
        } else {
            $typeDescription = $this->translations[33];
        }
        $globalJsVariables = array(
            'GLOBAL_ALL_COUNTRIES' => json_encode($countries),
            'GLOBAL_COUNTRY_UPLOADS_URL' => UPLOAD_PATH_COUNTRIES
        );
        $pageData = array(
            'tours' => $toursAndRelations['tours'],
            'tourCountryRelations' => $toursAndRelations['tourCountries'],
            'type'  => $type,
            'typeDescription'  => $typeDescription,
        );
        if ($countrySlug) {
            $countryId = $this->countries_model->getCountryIdBySlug($countrySlug);
            $pageData['selectedCountryId'] = $countryId;
            $globalJsVariables['GLOBAL_SELECTED_COUNTRY_ID'] = $countryId;
        }
        $data = array(
            'pageData' => $pageData,
            'globalJsVariables' => $globalJsVariables
        );
        $this->render('tours/index', $lang, $data);
    }

    public function particular($lang, $slug)
    {
        $this->preloadTranslations(array(94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110), $lang);
        $this->load->model('tours_model');
        $tourAndGallery = $this->tours_model->getBasicInfoBySlug($slug, $lang);
        $data = array(
            'pageData' => array(
                'tour' => $tourAndGallery['tour'],
                'gallery' => $tourAndGallery['gallery']
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/masonry.css',
            ),
            'additionalJsFiles' => array(
                'public/js/vendor/masonry-docs.min.js',
            ),
            'globalJsVariables' => array(
                'GLOBAL_ORDER_TOUR' =>site_url('tours/order_tour')
            )
        );
        $this->render('tours/particular', $lang, $data);
    }

    public function order_tour()
    {
        $result = array(
            'status' => 'error',
            'msg'    => 'Server Error'
        );
        try {
            $this->preloadTranslations(array(87,114,115), $this->input->post('lang'));

            $this->form_validation->set_rules('order_first_name', 'order_first_name','trim|xss_clean|required');
            $this->form_validation->set_rules('order_last_name', 'order_last_name','trim|xss_clean|required');
            $this->form_validation->set_rules('order_travelers_count', 'order_travelers_count','trim|xss_clean|required');
            $this->form_validation->set_rules('order_email', 'order_email','trim|xss_clean|required|valid_email');
            $this->form_validation->set_rules('order_telephone', 'order_telephone','trim|xss_clean|required');
            $this->form_validation->set_rules('order_captcha', 'order_captcha','trim|xss_clean|required');
            $captchaWord = $this->session->userdata('captchaWord');
            if ($this->form_validation->run() == false ) {
                $result =  array(
                    'status' => 'error',
                    'msg'    => $this->translations[114]
                );
                throw new Exception();
            } elseif (strcmp(strtoupper($captchaWord),strtoupper($this->input->post('order_captcha'))) != 0) {
                $result =  array(
                    'status' => 'error',
                    'msg'    => $this->translations[115]
                );
                throw new Exception();
            } else {
                $settings = $this->settings_model->getAllSettingsByLang($this->input->post('lang'));
                if ($this->sendMail('order-tour', $settings['mail_order_tour'], 'Tour order', $_POST)) {
                    $result =  array(
                        'status' => 'success',
                        'msg'    => $this->translations[87]
                    );
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

}
