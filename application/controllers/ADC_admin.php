<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Languages_model $languages_model
 * @property User_model $user_model
 * @property Slider_model $slider_model
 * @property Countries_model $countries_model
 * @property Country_gallery_model $country_gallery_model
 * @property Tours_model $tours_model
 * @property Tour_gallery_model $tour_gallery_model
 * @property Best_offers_model $best_offers_model
 * @property Settings_model $settings_model
 * @property Feedbacks_model $feedbacks_model
 * @property About_us_model $about_us_model
 * @property Visas_model $visas_model
 * @property Translations_model $translations_model
 * @property Partners_model $partners_model
 */

class ADC_admin extends CI_Controller {

    private $layout = 'admin/layouts/main';
    protected $activeLanguages;

	public function __construct()
	{
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('languages_model');
        $this->load->helper('adc_admin');
        $isAdmin = $this->user_model->checkAdminIdentity();
        if ($this instanceof Admin_login && $isAdmin) {
            redirect('/admin');
        }
        if (!$isAdmin && !($this instanceof Admin_login)) {
            redirect('/admin_login');
        }
        $this->activeLanguagesIsoCodes = $this->languages_model->getActiveLanguagesIsoCodes();
        $adminLanguage = $this->languages_model->getAdminLanguage();

        if (NULL === $adminLanguage) {
            // session is not set, get the info from db
            //and put it in the session
            $this->languages_model->setLanguageForAdmin();
            $adminLanguage = $this->languages_model->getAdminLanguage();
        }

        $adminLanguageEnglishName = $adminLanguage['nameEnglish'];

        if ($adminLanguageEnglishName  != $this->lang->getDefault()) {
            $this->lang->setDefault($adminLanguageEnglishName);
        }
        $this->lang->load("common",$adminLanguageEnglishName);

    }

    protected function render($page, $data = array()) {
        if (!isset($data['pageData'])) {
            $data['pageData'] = array();
        }
        $data['pageData']['activeLanguagesIsoCodes'] = $this->activeLanguagesIsoCodes;
        $pageView = $this->load->view('admin/pages/' . $page,$data['pageData'],true);
        unset($data['pageData']);
        $data['pageView'] = $pageView;

        if (empty($data['globalJsVariables'])) {
            $data['globalJsVariables'] = array();
        }
        $data['globalJsVariables']['GLOBAL_BASE_URL'] = base_url();
        $data['globalJsVariables']['GLOBAL_LANG_ISO'] = $this->languages_model->getAdminLanguage()['isoCode'];
        if ($this->haveFlashMessage()) {
            $data['globalJsVariables']['GLOBAL_FLASH_MESSAGE'] = $this->getFlashDataJson();
        }
        $this->load->view($this->getLayout(),$data);
    }

    protected function setFlashMessage($status, $msg)
    {
        $this->session->set_flashdata('msg', array('status' => $status, 'msg' => $msg));
    }

    protected function haveFlashMessage()
    {
        return (NULL !== $this->session->flashdata('msg'));
    }

    protected function getFlashDataJson()
    {   $msg = json_encode($this->session->flashdata('msg'));
        $this->session->unset_userdata(array('msg'));
        return $msg;
    }

    protected function setLayout($layout)
    {
        $this->layout = $layout;
    }

    protected function getLayout()
    {
        return $this->layout;
    }

    protected function makeConvenientArray($dbResult, $translateFields)
    {
        $resultArray = array();
        foreach($dbResult as $row) {
            foreach($row as $key=>$value) {
                if($key == 'iso_code') {
                    continue;
                }
                if (in_array($key, $translateFields)) {
                    $resultArray[$key][$row['iso_code']] = $value;
                } else {
                    $resultArray[$key] = $value;
                }
            }
        }
        return $resultArray;
    }

    protected function getDefaultLanguageIsoCode()
    {
        return $this->languages_model->getDefaultLanguageIsoCode();
    }

    protected function uploadThumbImage($imageName, $uploadPath = false, $width = false, $height = false)
    {
        if (!$_FILES[$imageName]['error'] && $_FILES[$imageName]['error'] != UPLOAD_ERR_NO_FILE
            && isset($_FILES[$imageName]['name']) && !empty($_FILES[$imageName]['tmp_name'])
        ) {
            $config['upload_path'] = UPLOAD_PATH . $uploadPath;
            $config['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
            $config['max_size'] = FILE_SIZE_GENERAL; //10MB
            $filenameArray = explode(".", $_FILES[$imageName]['name']);
            $ext = end($filenameArray);
            if (in_array($ext, explode('|', $config['allowed_types']))) {
                if ($_FILES[$imageName]['size'] <= FILE_SIZE_GENERAL) {
                    $config['file_name'] = createFileName($_FILES[$imageName]['name'], $ext);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload($imageName)) {
                        if ($width) {
                            $imageConfig['image_library'] = 'gd2';
                            $imageConfig['source_image'] = UPLOAD_PATH . $uploadPath . $config['file_name'];
                            $imageConfig['create_thumb'] = FALSE;
                            $imageConfig['maintain_ratio'] = FALSE;
                            $imageConfig['width'] = $width;
                            $imageConfig['height'] = $height;
                            $this->image_lib->initialize($imageConfig);
                            $this->image_lib->source_image = UPLOAD_PATH . $uploadPath . $config['file_name'];
                            $this->image_lib->resize();
                            $this->image_lib->clear();
                        }
                        return $config['file_name'];
                    }
                }
            }
        }
        return false;
    }

}
