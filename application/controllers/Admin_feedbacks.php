<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_feedbacks extends ADC_admin
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('feedbacks_model');

    }

    public function index()
    {
        $feedbacks = $this->feedbacks_model->getAllFeedbacks($this->getDefaultLanguageIsoCode());
        $data = array(
            'additionalJsFiles' => array(
                'public/js/vendor/jquery-ui.js',
                'public/js/pages/admin/feedbacks/index.js'
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css',
                'public/css/pages/admin/feedbacks/index.css'
            ),
            'pageData' => array(
                'feedbacks' => $feedbacks
            ),
            'globalJsVariables' => array(
                'GLOBAL_SAVE_ORDER_URL' => site_url('admin_feedbacks/save_order'),
                'GLOBAL_DELETE_FEEDBACK' => site_url('admin_feedbacks/delete'),
            )
        );

        $this->render('feedbacks/index', $data);
    }

    public function edit($id = 0)
    {
        echo '<pre>'; print_r($_POST);die;
        if ($id) {
            $basicInfo = $this->feedbacks_model->getBasicInfo($id);
            $basicInfo = $this->makeConvenientArray($basicInfo, array('content', 'customer_name'));
            if (empty($basicInfo)) {
                $this->setFlashMessage('error', lang('adc_item_does_not_exist'));
            }
        }
        try {

            if (!empty($_POST)) {
                if ($id == 0) {
                    $res = $this->feedbacks_model->add(
                        $this->input->post('score'),
                        $this->input->post('text'),
                        $this->input->post('customer_name')
                    );
                    if ($res == false) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $this->setFlashMessage('success', lang('adc_successfully_added'));
                    redirect('admin_feedbacks/edit/' . $res);
                } else {
                    if (!$this->feedbacks_model->update(
                        $this->input->post('score'),
                        $this->input->post('text'),
                        $this->input->post('customer_name'),
                        $id
                    )
                    ) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $basicInfo = $this->feedbacks_model->getBasicInfo($id);
                    $basicInfo = $this->makeConvenientArray($basicInfo, array('content', 'customer_name'));
                    $this->setFlashMessage('success', lang('adc_successfully_updated'));
                }
            }
        } catch (Exception $ex) {
            redirect('admin_feedbacks');
        }

        $data = array(
            'additionalJsFiles' => array(
                'public/js/pages/admin/feedbacks/edit.js'
            ),
            'globalJsVariables' => array(
                'isEdit' => ($id) ? 1 : 0
            )
        );
        if ($id) {
            $data['pageData'] = $basicInfo;
        }
        $this->render('feedbacks/edit', $data);
    }

    public function save_order()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            if (!$this->feedbacks_model->saveOrder($this->input->post('ids'))) {
                throw new Exception();
            }
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_updated')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function delete()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            $this->feedbacks_model->deleteFeedback($this->input->post('id'));
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_removed')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


}
