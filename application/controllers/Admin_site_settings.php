<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_site_settings extends ADC_admin
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('settings_model');
    }


    public function edit_meta_tags($pageSlug)
    {
        $pageTitle = lang('adc_meta_tags') . ' (';
        switch ($pageSlug)
        {
            case SLUG_HOME:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                        'home_page_og_type',
                        'home_page_og_image',
                        'home_page_og_url'
                    ),
                    'translations_table' => array(
                        'meta_keywords_home_page',
                        'meta_description_home_page',
                        'home_page_og_title',
                        'home_page_og_description'
                    ),
                );
                $pageTitle .= lang('adc_home') . ')';
                break;
            case SLUG_ABOUT_US:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                    ),
                    'translations_table' => array(
                        'meta_keywords_about_us',
                        'meta_description_about_us'
                    ),
                );
                $pageTitle .= lang('adc_about_us') . ')';
                break;
            case SLUG_INCOMING_TOURS:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                    ),
                    'translations_table' => array(
                        'meta_keywords_tours_incoming',
                        'meta_description_tours_incoming'
                    ),
                );
                $pageTitle .= lang('adc_tours_incoming') . ')';
                break;
            case SLUG_OUTGOING_TOURS:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                    ),
                    'translations_table' => array(
                        'meta_keywords_tours_outgoing',
                        'meta_description_tours_outgoing'
                    ),
                );
                $pageTitle .= lang('adc_tours_outgoing') . ')';
                break;
            case SLUG_EDUCATIONAL_TOURS:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                    ),
                    'translations_table' => array(
                        'meta_keywords_tours_education',
                        'meta_description_tours_education'
                    ),
                );
                $pageTitle .= lang('adc_tours_educational') . ')';
                break;
            case SLUG_DESTINATION:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                    ),
                    'translations_table' => array(
                        'meta_keywords_description',
                        'meta_description_description'
                    ),
                );
                $pageTitle .= lang('adc_destination') . ')';
                break;
            case SLUG_BOOK_HOTEL:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                    ),
                    'translations_table' => array(
                        'meta_keywords_book_hotel',
                        'meta_description_book_hotel'
                    ),
                );
                $pageTitle .= lang('adc_book_hotel') . ')';
                break;
            case SLUG_VISAS_TO_ARMENIA:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                    ),
                    'translations_table' => array(
                        'meta_keywords_visas_to_armenia',
                        'meta_description_visas_to_armenia'
                    ),
                );
                $pageTitle .= lang('adc_visas_to_armenia') . ')';
                break;
            case SLUG_VISAS_FROM_ARMENIA:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                    ),
                    'translations_table' => array(
                        'meta_keywords_visas_from_armenia',
                        'meta_description_visas_from_armenia'
                    ),
                );
                $pageTitle .= lang('adc_visas_from_armenia') . ')';
                break;
            case SLUG_NOT_DECIDED:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                    ),
                    'translations_table' => array(
                        'meta_keywords_not_decided',
                        'meta_description_not_decided'
                    ),
                );
                $pageTitle .= lang('adc_not_decided') . ')';
                break;
            case SLUG_CONTACT_US:
                $selectArray = array(
                    'main_table' => array(
                        'id',
                    ),
                    'translations_table' => array(
                        'meta_keywords_contact_us',
                        'meta_description_contact_us'
                    ),
                );
                $pageTitle .= lang('adc_contact_us') . ')';
                break;
        }

        $basicInfo = $this->settings_model->getBasicInfo($selectArray);
        $basicInfo = $this->makeConvenientArray($basicInfo, $selectArray['translations_table']);
        if (empty($basicInfo)) {
            $this->setFlashMessage('error', lang('adc_item_does_not_exist'));
        }

        if (!empty($_POST)) {
            if ($pageSlug == SLUG_HOME) {
                $this->load->library('image_lib');
                $this->load->library('upload');
                $ogImage = $this->uploadThumbImage('home_page_og_image',UPLOAD_PATH_SETTINGS, OG_IMAGE_WIDTH, OG_IMAGE_HEIGHT);
                if ($ogImage && !is_null($basicInfo['home_page_og_image']) && $basicInfo['home_page_og_image']) {
                    @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_SETTINGS . $basicInfo['home_page_og_image']);
                }
                if ($ogImage) {
                    $_POST['home_page_og_image'] = $ogImage;
                }
            }
            $this->settings_model->updateMetaTags($selectArray, $_POST);
            $basicInfo = $this->settings_model->getBasicInfo($selectArray);
            $basicInfo = $this->makeConvenientArray($basicInfo, $selectArray['translations_table']);
            $this->setFlashMessage('success', lang('adc_successfully_updated'));
        }

        $data = array(
            'additionalJsFiles' => array(
                'public/js/pages/admin/site-settings/edit-meta-tags.js'
            )
        );
        $data['pageData'] = $basicInfo;
        $data['pageData']['slug'] = $pageSlug;
        $data['pageData']['pageTitle'] = $pageTitle;
        $this->render('site-settings/edit-meta-tags', $data);
    }

    public function common()
    {
        $data = array(
            'additionalJsFiles' => array(
            'public/js/pages/admin/site-settings/common.js'
        )
        );
        $settings = $this->settings_model->getAllSettings();
        if (!empty($_POST)) {
            $this->settings_model->save(
                $_POST
            );
            $settings = $this->settings_model->getAllSettings();
            $this->setFlashMessage('success', lang('adc_successfully_updated'));
        }
        $data['pageData'] = $settings;
        $this->render('site-settings/common', $data);
    }


}
