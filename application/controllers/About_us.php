<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_site.php');


class About_us extends ADC_site
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('feedbacks_model');

    }

    public function index($lang)
    {
        $this->preloadTranslations(array(26, 27, 28, 29, 121, 122), $lang);
        $this->load->model('about_us_model');
        $this->load->model('about_us_jobs_model');
        $this->load->model('our_team_model');
        $this->load->model('our_sertificates_model');
        $this->load->model('our_partners_model');
        $this->load->model('partners_model');
        $this->load->model('feedbacks_model');
        $aboutUs = $this->about_us_model->getAll($lang);
        $our_jobs = $this->about_us_jobs_model->getAllJobs($lang);
        $our_team = $this->our_team_model->getAllTeams($lang);
        $our_sertificates = $this->our_sertificates_model->getAllSertificates($lang);
        $our_partners = $this->our_partners_model->getAllPartners($lang);
        $feedbacks = $this->feedbacks_model->getAllFeedbacks($lang);
        $simplePartners = $this->partners_model->getAllPartnersByType(PARTNER_TYPE_SIMPLE);
        $educationalPartners = $this->partners_model->getAllPartnersByType(PARTNER_TYPE_EDUCATIONAL);

        $data = array(
            'pageData' => array(
                'aboutUs' => $aboutUs,
                'our_jobs' => $our_jobs,
                'our_team' => $our_team,
                'our_sertificates' => $our_sertificates,
                'our_partners' => $our_partners,
                'feedbacks' => $feedbacks,
                'simplePartners' => $simplePartners,
                'educationalPartners' => $educationalPartners,
                'activeLanguagesIsoCodes' => $this->activeLanguagesIsoCodes,
            ),
             'additionalCssFiles' => array(
                 'public/css/vendor/masonry.css',
                 'public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css',
                 'public/css/pages/admin/feedbacks/index.css'
             ),
            'additionalJsFiles' => array(
               'public/js/vendor/masonry-docs.min.js',
               'public/js/pages/admin/feedbacks/index.js',
               'public/js/pages/admin/feedbacks/edit.js'
             ),
            'globalJsVariables' => array(
                'GLOBAL_SAVE_ORDER_URL' => site_url('admin_feedbacks/save_order'),
                'GLOBAL_DELETE_FEEDBACK' => site_url('admin_feedbacks/delete'),
            )
        );
        $this->render('about-us/index', $lang, $data);
    }

    public function edit()
    {

        if (!empty($_POST)) {

            //echo '<pre>'; print_r($_POST);die;
            $score = '5';
            $res = $this->feedbacks_model->add(
                    $score,
                    $this->input->post('text'),
                    $this->input->post('customer_name')
                );
                if ($res == false) {
                    $this->setFlashMessage('error', lang('adc_server_error'));
                    throw new Exception();
                }
                $this->setFlashMessage('success', lang('adc_successfully_added'));
                redirect('en/about-us');
        }
    }

}
