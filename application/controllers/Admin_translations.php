<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_translations extends ADC_admin
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('translations_model');
    }

    public function index()
    {
        if (!empty($_POST)){
            $this->translations_model->save($this->input->post('id'),$this->input->post('content'));
        }
        $translations = $this->translations_model->getAllTranslations();
        $aaData = array();
        foreach ($translations as $translation) {
            array_push($aaData,
                array(
                    lang('adc_' . $translation['iso_code']),
                    $translation['id'],
                    $translation['content'],
                    '<a class="edit-translation btn btn-xs btn-primary" data-id="' . $translation['id'] . '">' . lang('adc_edit') . '</a>',
                )
                );
        }
        $data = array(
            'additionalJsFiles' => array(
                'public/js/vendor/jquery.dataTables.min.js',
                'public/js/vendor/DT_bootstrap.js',
                'public/js/pages/admin/translations/index.js'
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/datatables.bootstrap.css',
            ),
            'pageData' => array(),
            'globalJsVariables' => array(
                'GLOBAL_GET_BY_ID_URL' => site_url('admin_translations/get_by_id'),
                'aaData' => json_encode($aaData),
            )
        );

        $this->render('translations/index', $data);
    }

    public function get_by_Id()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
        $basicInfo = $this->translations_model->getById($this->input->post('id'));
        $basicInfo = $this->makeConvenientArray($basicInfo, array('content'));
        $this->setFlashMessage('success', lang('adc_successfully_updated'));
            $result = array(
                'status' => 'success',
                'basicIfo'    => $basicInfo
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

}
