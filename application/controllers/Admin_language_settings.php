<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_language_settings extends ADC_admin {

	public function index()
	{

        $languages = $this->languages_model->getActiveAndInactiveLanguages();

        $data = array(
            'pageData' => array(
                'activeLanguages' => $languages['active'],
                'inactiveLanguages' => $languages['inactive']
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css',
                'public/css/pages/admin/languagesettings/index.css'
            ),
            'additionalJsFiles' => array(
                'public/js/vendor/jquery-ui.js',
                'public/js/pages/admin/languagesettings/index.js'
            ),
            'globalJsVariables' => array(
                'GLOBAL_ADC_ACTIVATE' => lang('adc_activate'),
                'GLOBAL_ADC_DEACTIVATE' => lang('adc_deactivate'),
                'GLOBAL_ADC_LOADING' => lang('adc_loading'),
                'GLOBAL_ADC_SAVE' => lang('adc_save'),
                'GLOBAL_SAVE_LANGUAGES' => site_url('admin_language_settings/save'),
            )
        );
        $this->render('languagesettings/index', $data);
	}

    public function save()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            ini_set('max_execution_time', 0);
            if (!$this->languages_model->saveLanguages($this->input->post('data'))) {
                throw new Exception();
            }
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_updated')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


}
