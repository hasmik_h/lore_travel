<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_settings extends ADC_admin {

	public function index()
	{
        $userData = $this->user_model->getUserIdentity();
        $data = array(
            'pageData' => $userData,
            'additionalJsFiles' => array(
                'public/js/pages/admin/settings/index.js'
            ),
            'globalJsVariables' => array(
                'GLOBAL_CHANGE_PASSWORD' => 'admin_settings/changepassword',
                'GLOBAL_PASSWORDS_DO_NOT_MATCH' => lang("adc_passwords_do_not_match"),
            )
        );
        $this->render('settings/index', $data);
	}

    public function changepassword()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            if(!$this->user_model->checkUserPassword($this->input->post('current_password'))) {
                $result['msg'] = lang('adc_current_password_is_wrong');
                throw new Exception();
            }
            $this->user_model->changePassword($this->input->post('new_password'));
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_updated')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function language()
    {
        $possibleAdminLanguages = $this->languages_model->getAdminLanguages();
        $data = array(
            'pageData' => array('possibleAdminLanguages' =>$possibleAdminLanguages),
            'additionalCssFiles' => array(
                'public/css/vendor/msdropdown.css',
            ),
            'additionalJsFiles' => array(
                'public/js/vendor/msdropdown.min.js',
                'public/js/pages/admin/settings/language.js',
            ),
            'globalJsVariables' => array(
                'GLOBAL_CHANGE_LANGUAGE' => 'admin_settings/changelanguage'
            )
        );
        $this->render('settings/language', $data);
    }

    public function changelanguage()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
           $this->languages_model->changeAdminLanguage($this->input->post('languageId'));
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_updated')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


}
