<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Languages_model $languages_model
 * @property User_model $user_model
 * @property Slider_model $slider_model
 * @property Countries_model $countries_model
 * @property Country_gallery_model $country_gallery_model
 * @property Tours_model $tours_model
 * @property Tour_gallery_model $tour_gallery_model
 * @property Best_offers_model $best_offers_model
 * @property Settings_model $settings_model
 * @property Feedbacks_model $feedbacks_model
 * @property About_us_model $about_us_model
 * @property Visas_model $visas_model
 * @property Translations_model $translations_model
 * @property Partners_model $partners_model
 */

class ADC_site extends CI_Controller {

    private $layout = 'site/layouts/main';
    protected $translations = array();

    protected static $layoutTranslations = array(
        1,2,3,4,5,6,7,8,9,10,11,12,13,14,
        18,19,20,21,22,23,24,25,43,40,42,
        54,55,56,57,58,59,60,61,
        62,63,64,65,66,67,68,69,
        70,71,72,73,74,75,76,77,
        78,79,80,81,82,83,84,85,86,
        87,88,90,113,116, 129,132,133,134,135,136,
        144,145,146,147
    );

	public function __construct()
	{
        parent::__construct();
        $this->load->helper('adc_site');
        $this->load->model('languages_model');
        $this->load->model('translations_model');
        $this->load->model('settings_model');
        $this->load->model('languages_model');
        $this->load->model('best_offers_model');
        $this->activeLanguagesIsoCodes = $this->languages_model->getActiveLanguagesIsoCodes();
    }

    protected function render($page, $lang, $data = array()) {
        if (!isset($data['pageData'])) {
            $data['pageData'] = array();
        }
        $data['pageData']['bestOffers'] = $this->best_offers_model->getlastBestOffers($lang);
        $activeAndVisibleLanguages = $this->languages_model->getActiveLanguages(true);
        $settings = $this->settings_model->getAllSettingsByLang($lang);
        $data['pageData']['translations'] = $this->translations;
        $data['pageData']['lang'] = $lang;
        $data['pageData']['settings'] = $settings;
        $captchaConfig = array(
            'img_path' => CAPTCHA_PATH_IMG_PATH,
            'img_url' => base_url() . CAPTCHA_PATH_IMG_PATH,
            'expiration' => 7200
        );
        $captcha = create_captcha($captchaConfig);
        $this->session->set_userdata('captchaWord', $captcha['word']);
        $data['pageData']['captcha'] = $captcha;
        $pageView = $this->load->view('site/pages/' . $page,$data['pageData'],true);
        unset($data['pageData']);
        $data['pageView'] = $pageView;
        if (empty($data['globalJsVariables'])) {
            $data['globalJsVariables'] = array();
        }
        $data['globalJsVariables']['GLOBAL_BASE_URL'] = base_url();
        $data['globalJsVariables']['GLOBAL_LANG'] = $lang;
        $data['globalJsVariables']['GLOBAL_LANGUAGES'] = json_encode(makeConvenientArrayForLanguages($activeAndVisibleLanguages, $lang));
        $data['globalJsVariables']['GLOBAL_IMAGES_URL'] = site_url(IMAGES_PATH_READ);
        $data['globalJsVariables']['GLOBAL_UPLOADS_URL'] = site_url(UPLOAD_PATH_READ) . '/';
        $data['globalJsVariables']['SEND_LETS_PLAN_YOUR_TRIP_URL'] = site_url('contact_us/lets_plan_your_trip');
        $data['globalJsVariables']['DO_NOT_OPEN_AGAIN'] = site_url('contact_us/do_not_open_again');
        $data['globalJsVariables']['CHANGE_CAPTCHA'] = site_url('contact_us/can_not_see');
        if ($this->session->userdata('is_closed') == null) {
            $data['globalJsVariables']['GLOBAL_OPEN'] = 1;
        }
        $data['translations'] = $this->translations;
        $data['settings'] = $settings;
        $data['captcha'] = $captcha;
        $data['metaTags'] = $this->getMetaData($settings);
        $this->load->view($this->getLayout(),$data);
    }


    protected function getLayout()
    {
        return $this->layout;
    }

    protected function getDefaultLanguageIsoCode()
    {
        return $this->languages_model->getDefaultLanguageIsoCode();
    }

    protected function preloadTranslations(array $ids, $lang)
    {
        $this->translations = $this->translations_model->
        preloadTranslations(array_merge($ids, self::$layoutTranslations), $lang);
    }

    protected function setFlashMessage($status, $msg)
    {
        $this->session->set_flashdata('msg', array('status' => $status, 'msg' => $msg));
    }

    private function getMetaData($settings)
    {
        $metaKeysAndDesc = array('keywords' => $settings['meta_keywords_home_page'],
            'description' => $settings['meta_description_home_page']);

        if ($this->router->fetch_class() == 'home' && $this->router->fetch_method() == 'index'){
            if ($settings['home_page_og_title'] && !is_null($settings['home_page_og_title'])) {
                $metaKeysAndDesc['og:title'] = $settings['home_page_og_title'];
            }
            if ($settings['home_page_og_description'] && !is_null($settings['home_page_og_description'])) {
                $metaKeysAndDesc['og:description'] = $settings['home_page_og_description'];
            }
            if ($settings['home_page_og_image'] && !is_null($settings['home_page_og_image'])) {
                $metaKeysAndDesc['og:image'] = site_url(UPLOAD_PATH_READ . UPLOAD_PATH_SETTINGS . $settings['home_page_og_image']);
            }
            if ($settings['home_page_og_url'] && !is_null($settings['home_page_og_url'])) {
                $metaKeysAndDesc['og:url'] = $settings['home_page_og_url'];
            }
            if ($settings['home_page_og_type'] && !is_null($settings['home_page_og_type'])) {
                $metaKeysAndDesc['og:type'] = $settings['home_page_og_type'];
            }
            $metaKeysAndDesc['og:site_name'] = 'Globe Travel';
        } elseif ($this->router->fetch_class() == 'about_us'){
            if ($settings['meta_keywords_about_us'] && !is_null($settings['meta_keywords_about_us'])) {
                $metaKeysAndDesc['keywords'] = $settings['meta_keywords_about_us'];
            }
            if ($settings['meta_description_about_us'] && !is_null($settings['meta_description_about_us'])) {
                $metaKeysAndDesc['description'] = $settings['meta_description_about_us'];
            }
        } elseif ($this->router->fetch_class() == 'tours'){

                if (strpos($_SERVER['REQUEST_URI'], SLUG_INCOMING_TOURS)) {
                    if ($settings['meta_keywords_tours_incoming'] && !is_null($settings['meta_keywords_tours_incoming'])) {
                        $metaKeysAndDesc['keywords'] = $settings['meta_keywords_tours_incoming'];
                    }
                    if ($settings['meta_description_tours_incoming'] && !is_null($settings['meta_description_tours_incoming'])) {
                        $metaKeysAndDesc['description'] = $settings['meta_description_tours_incoming'];
                    }
                } elseif (strpos($_SERVER['REQUEST_URI'], SLUG_OUTGOING_TOURS)) {
                    if ($settings['meta_keywords_tours_outgoing'] && !is_null($settings['meta_keywords_tours_outgoing'])) {
                        $metaKeysAndDesc['keywords'] = $settings['meta_keywords_tours_outgoing'];
                    }
                    if ($settings['meta_description_tours_outgoing'] && !is_null($settings['meta_description_tours_outgoing'])) {
                        $metaKeysAndDesc['description'] = $settings['meta_description_tours_outgoing'];
                    }
                } elseif (strpos($_SERVER['REQUEST_URI'], SLUG_EDUCATIONAL_TOURS)) {
                    if ($settings['meta_keywords_tours_education'] && !is_null($settings['meta_keywords_tours_education'])) {
                        $metaKeysAndDesc['keywords'] = $settings['meta_keywords_tours_education'];
                    }
                    if ($settings['meta_description_tours_education'] && !is_null($settings['meta_description_tours_education'])) {
                        $metaKeysAndDesc['description'] = $settings['meta_description_tours_education'];
                    }
                }

        } elseif ($this->router->fetch_class() == 'destination'){
        if ($settings['meta_keywords_destination'] && !is_null($settings['meta_keywords_destination'])) {
                $metaKeysAndDesc['keywords'] = $settings['meta_keywords_destination'];
            }
            if ($settings['meta_description_destination'] && !is_null($settings['meta_description_destination'])) {
                $metaKeysAndDesc['description'] = $settings['meta_description_destination'];
            }
        } elseif ($this->router->fetch_class() == 'book_hotel'){
            if ($settings['meta_keywords_book_hotel'] && !is_null($settings['meta_keywords_book_hotel'])) {
                $metaKeysAndDesc['keywords'] = $settings['meta_keywords_book_hotel'];
            }
            if ($settings['meta_description_book_hotel'] && !is_null($settings['meta_description_book_hotel'])) {
                $metaKeysAndDesc['description'] = $settings['meta_description_book_hotel'];
            }
        }  elseif ($this->router->fetch_class() == 'visas'){

            if (strpos($_SERVER['REQUEST_URI'], SLUG_VISAS_TO_ARMENIA)) {
                if ($settings['meta_keywords_visas_to_armenia'] && !is_null($settings['meta_keywords_visas_to_armenia'])) {
                    $metaKeysAndDesc['keywords'] = $settings['meta_keywords_visas_to_armenia'];
                }
                if ($settings['meta_description_visas_to_armenia'] && !is_null($settings['meta_description_visas_to_armenia'])) {
                    $metaKeysAndDesc['description'] = $settings['meta_description_visas_to_armenia'];
                }
            } elseif (strpos($_SERVER['REQUEST_URI'], SLUG_VISAS_FROM_ARMENIA)) {
                if ($settings['meta_keywords_visas_from_armenia'] && !is_null($settings['meta_keywords_visas_from_armenia'])) {
                    $metaKeysAndDesc['keywords'] = $settings['meta_keywords_visas_from_armenia'];
                }
                if ($settings['meta_description_visas_from_armenia'] && !is_null($settings['meta_description_visas_from_armenia'])) {
                    $metaKeysAndDesc['description'] = $settings['meta_description_visas_from_armenia'];
                }
            }
        } elseif ($this->router->fetch_class() == 'contact_us'){
            if ($settings['meta_keywords_contact_us'] && !is_null($settings['meta_keywords_contact_us'])) {
                $metaKeysAndDesc['keywords'] = $settings['meta_keywords_contact_us'];
            }
            if ($settings['meta_description_contact_us'] && !is_null($settings['meta_description_contact_us'])) {
                $metaKeysAndDesc['description'] = $settings['meta_description_contact_us'];
            }
        }

        return $metaKeysAndDesc;

        }


    protected function sendMail($template, $to, $subject, $data)
    {
        $this->load->library('email');
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->from(MAIL_SENDER, 'Loretravel');
        $this->email->to($to);
        $this->email->subject($subject);
        $message = $this->load->view('mail-templates/' . $template, $data, true);
        $this->email->message($message);
        return $this->email->send();
    }

}
