<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_site.php');


class Trip_plan extends ADC_site
{
    public function index($lang) {
        $this->preloadTranslations(array(30, 31, 33, 32, 130), $lang);

        $this->render('trip_plan/index', $lang );
    }

    public function particular($lang, $slug) {
        $this->preloadTranslations(array(94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110), $lang);
        $this->render('/particular', $lang);
    }


}
