<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_slider extends ADC_admin
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('slider_model');

    }

    public function index()
    {
        $slides = $this->slider_model->getAllSlides($this->getDefaultLanguageIsoCode());
        $data = array(
            'additionalJsFiles' => array(
                'public/js/vendor/jquery-ui.js',
                'public/js/pages/admin/slider/index.js'
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css',
                'public/css/pages/admin/slider/index.css'
            ),
            'pageData' => array(
                'slides' => $slides
            ),
            'globalJsVariables' => array(
                'GLOBAL_SAVE_ORDER_URL' => site_url('admin_slider/save_order'),
                'GLOBAL_DELETE_SLIDE' => site_url('admin_slider/deleteslide'),
            )
        );

        $this->render('slider/index', $data);
    }

    public function edit($id = 0)
    {
        if ($id) {
            $basicInfo = $this->slider_model->getBasicInfo($id);
            $basicInfo = $this->makeConvenientArray($basicInfo, array('content', 'title', 'link_text'));
            if (empty($basicInfo)) {
                $this->setFlashMessage('error', lang('adc_item_does_not_exist'));
            }
        }
        try {
            if (isset($_FILES['slider_image']['name']) && $_FILES['slider_image']['error'] && $_FILES['slider_image']['error'] != UPLOAD_ERR_NO_FILE) {
                $this->setFlashMessage('error', lang('adc_server_error'));
                throw new Exception();
            }
            if (isset($_FILES['slider_image']['name']) && !empty($_FILES['slider_image']['tmp_name'])) {
                $config['upload_path'] = UPLOAD_PATH . UPLOAD_PATH_SLIDER;
                $config['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                $config['max_size'] = FILE_SIZE_GENERAL; //10MB
                $filenameArray = explode(".", $_FILES['slider_image']['name']);
                $ext = end($filenameArray);
                //sometimes this library does not do right validation
                //so this is an additional check
                if (!in_array($ext, explode('|', $config['allowed_types']))) {
                    $this->setFlashMessage('error', sprintf(lang('adc_not_allowed_file_type'), $config['allowed_types']));
                    throw new Exception();
                }
                if ($_FILES['slider_image']['size'] > FILE_SIZE_GENERAL) {
                    $this->setFlashMessage('error', sprintf(lang('adc_not_allowed_file_size'), (int)$config['max_size'] / 1024 / 1024));
                    throw new Exception();
                }
                $config['file_name'] = createFileName($_FILES['slider_image']['name'], $ext);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('slider_image')) {
                    $this->setFlashMessage('error', $this->upload->display_errors());
                    throw new Exception();
                }
                $imageConfig['image_library'] = 'gd2';
                $imageConfig['source_image'] = UPLOAD_PATH . UPLOAD_PATH_SLIDER . $config['file_name'];
                $imageConfig['create_thumb'] = FALSE;
                $imageConfig['maintain_ratio'] = FALSE;
                $imageConfig['width'] = SLIDER_WIDTH;
                $imageConfig['height'] = SLIDER_HEIGHT;
                $this->load->library('image_lib', $imageConfig);
                if (!$this->image_lib->resize()) {
                    $this->setFlashMessage('error', $this->image_lib->display_errors());
                    throw new Exception();
                }

                if ($id) {
                    //deleting old file
                    @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_SLIDER . $basicInfo['file']);
                }
            }
            if (!empty($_POST)) {
                if ($id == 0) {
                    $res = $this->slider_model->add(
                        $this->input->post('text'),
                        $this->input->post('title'),
                        $this->input->post('link_text'),
                        $this->input->post('link'),
                        $config['file_name']);
                    if ($res == false) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $this->setFlashMessage('success', lang('adc_successfully_added'));
                    redirect('admin_slider/edit/' . $res);
                } else {
                    $fileName = (isset($config['file_name'])) ? $config['file_name'] : false;
                    if (!$this->slider_model->update(
                        $this->input->post('text'),
                        $this->input->post('title'),
                        $this->input->post('link_text'),
                        $this->input->post('link'),
                        $id,
                        $fileName
                    )
                    ) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $basicInfo = $this->slider_model->getBasicInfo($id);
                    $basicInfo = $this->makeConvenientArray($basicInfo, array('content', 'title', 'link_text'));
                    $this->setFlashMessage('success', lang('adc_successfully_updated'));
                }
            }
        } catch (Exception $ex) {
            redirect('admin_slider');
        }

        $data = array(
            'additionalJsFiles' => array(
                'public/js/pages/admin/slider/edit.js'
            ),
            'globalJsVariables' => array(
                'isEdit' => ($id) ? 1 : 0
            )
        );
        if ($id) {
            $data['pageData'] = $basicInfo;
        }
        $this->render('slider/edit', $data);
    }

    public function save_order()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            if (!$this->slider_model->saveOrder($this->input->post('ids'))) {
                throw new Exception();
            }
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_updated')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function deleteslide()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            $fileName = $this->slider_model->getFileNameById($this->input->post('id'));
            @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_SLIDER . $fileName);
            $this->slider_model->deleteSlide($this->input->post('id'));
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_removed')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


}
