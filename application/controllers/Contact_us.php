<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_site.php');


class Contact_us extends ADC_site
{


    public function index($lang)
    {
        $this->preloadTranslations(array(46,91,92,93,47,49,50,48,50,51,52,53), $lang);
        $data = array(
            'globalJsVariables' => array(
                'GLOBAL_SEND_CONTACT_US_MSG' =>site_url('contact_us/send_msg')
            )
        );
        $this->render('contact-us/index', $lang, $data);
    }


    public function send_msg()
    {
        $result = array(
            'status' => 'error',
            'msg'    => 'Server Error'
        );
        try {
            $this->preloadTranslations(array(87,114,115), $this->input->post('lang'));

            $this->form_validation->set_rules('contact_us_name', 'contact_us_name','trim|xss_clean|required');
            $this->form_validation->set_rules('contact_us_subject', 'contact_us_subject','trim|xss_clean|required');
            $this->form_validation->set_rules('contact_us_message', 'contact_us_message','trim|xss_clean|required');
            $this->form_validation->set_rules('contact_us_email', 'contact_us_email','trim|xss_clean|required|valid_email');
            //$this->form_validation->set_rules('contact_us_captcha', 'contact_us_captcha','trim|xss_clean|required');
            //$captchaWord = $this->session->userdata('captchaWord');
            if ($this->form_validation->run() == false ) {
                $result =  array(
                    'status' => 'error',
                    'msg'    => $this->translations[114]
                );
                throw new Exception();
            }/* elseif (strcmp(strtoupper($captchaWord),strtoupper($this->input->post('contact_us_captcha'))) != 0) {
                $result =  array(
                    'status' => 'error',
                    'msg'    => $this->translations[115]
                );
                throw new Exception();
            }*/ else {
                $settings = $this->settings_model->getAllSettingsByLang($this->input->post('lang'));
                if ($this->sendMail('contact-us', $settings['mail_contact_us'], $this->input->post('contact_us_subject'), $_POST)) {
                    $result =  array(
                        'status' => 'success',
                        'msg'    => $this->translations[87]
                    );
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function lets_plan_your_trip()
    {
        $result = array(
            'status' => 'error',
            'msg'    => 'Server Error'
        );
        try {
            $this->preloadTranslations(array(87,114,115), $this->input->post('lang'));

            $this->form_validation->set_rules('modal_destination', 'modal_destination','trim|xss_clean');
            $this->form_validation->set_rules('modal_travel_plans', 'modal_travel_plans','trim|xss_clean');
            $this->form_validation->set_rules('modal_date_to_go', 'modal_date_to_go','trim|xss_clean');
            $this->form_validation->set_rules('modal_day_tour', 'modal_day_tour','trim|xss_clean');
            $this->form_validation->set_rules('modal_count_travelers', 'modal_count_travelers','trim|xss_clean');
            $this->form_validation->set_rules('modal_amount', 'modal_amount','trim|xss_clean');
            $this->form_validation->set_rules('modal_currency', 'modal_currency','trim|xss_clean');
            $this->form_validation->set_rules('modal_comments', 'modal_comments','trim|xss_clean');
            $this->form_validation->set_rules('modal_first_name', 'modal_first_name','trim|xss_clean|required');
            $this->form_validation->set_rules('modal_last_name', 'modal_last_name','trim|xss_clean|required');
            $this->form_validation->set_rules('modal_last_name', 'modal_last_name','trim|xss_clean|required');
            $this->form_validation->set_rules('modal_email', 'modal_email','trim|xss_clean|required|valid_email');
            $this->form_validation->set_rules('modal_telephone', 'modal_telephone','trim|xss_clean|required');
            $this->form_validation->set_rules('modal_captcha', 'modal_captcha','trim|xss_clean|required');
            $captchaWord = $this->session->userdata('captchaWord');
            if ($this->form_validation->run() == false ) {
                $result =  array(
                    'status' => 'error',
                    'msg'    => $this->translations[114]
                );
                throw new Exception();
            } elseif (strcmp(strtoupper($captchaWord),strtoupper($this->input->post('modal_captcha'))) != 0) {
                $result =  array(
                    'status' => 'error',
                    'msg'    => $this->translations[115]
                );
                throw new Exception();
            } else {
                $settings = $this->settings_model->getAllSettingsByLang($this->input->post('lang'));
                if ($this->sendMail('lets-plan-your-trip', $settings['mail_lets_plan_your_trip'], 'Trip planning request', $_POST)) {
                    $result =  array(
                        'status' => 'success',
                        'msg'    => $this->translations[87]
                    );
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function can_not_see()
    {
        $result = array(
            'status' => 'error',
            'msg'    => 'Server Error'
        );
        try {
            $captchaConfig = array(
                'img_path' => CAPTCHA_PATH_IMG_PATH,
                'img_url' => base_url() . CAPTCHA_PATH_IMG_PATH,
                'expiration' => 7200
            );
            $captcha = create_captcha($captchaConfig);
            $this->session->set_userdata('captchaWord', $captcha['word']);
            $result = array(
                'status' => 'success',
                'msg'    => 'OK',
                'image'  => site_url(CAPTCHA_PATH_IMG_PATH . $captcha['filename'])
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function do_not_open_again()
    {
        $result = array(
            'status' => 'error',
            'msg'    => 'Server Error'
        );
        try {
            $this->session->set_userdata('is_closed', 1);
            $result = array(
                'status' => 'success',
                'msg'    => 'OK',
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


}
