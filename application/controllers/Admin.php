<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin extends ADC_admin {

	public function index()
	{
        $this->render('admin/index');
	}

    public function logout()
    {
        $this->user_model->logout();
        redirect('admin_login');
    }

}
