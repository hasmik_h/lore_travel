<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_site.php');


class Destination extends ADC_site {
    public function index($lang) {
        $this->preloadTranslations(array(151, 152, 139, 140, 141, 142,143), $lang);
        $this->load->model('destination_model');
        $destination  = $this->destination_model->getDestinations($lang);
        //echo '<pre>'; print_r($destination);die;
        $pageData['cont1'] = [];
        $pageData['cont2'] = [];
        $pageData['cont3'] = [];
        $pageData['cont4'] = [];
        $pageData['cont5'] = [];
        foreach($destination  as $a){

            if($a['continent'] == 1){
                array_push($pageData['cont1'],$a);
            } elseif($a['continent'] == 2){
                array_push($pageData['cont2'],$a);
            }elseif($a['continent'] == 3){
                array_push($pageData['cont3'],$a);
            }elseif($a['continent'] == 4){
                array_push($pageData['cont4'],$a);
            }elseif($a['continent'] == 5){
                array_push($pageData['cont5'],$a);
            }
        }
        $data ['pageData']['pageData'] = $pageData ;
        $this->render('destination/index', $lang, $data);
    }
}
