<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');

/**
 * @property User_model $user_model
 */

class Admin_login extends ADC_admin {

	public function index()
	{
        $this->setLayout('admin/layouts/login');
        $data = array(
            'additionalJsFiles' => array(
                'public/js/pages/admin/login/index.js'
            ),
            'additionalCssFiles' => array(
                'public/css/pages/admin/login/index.css',
            ),
            'globalJsVariables' => array(
                'GLOBAL_ADMIN_LOGIN_URL' => 'admin_login/login',
                'GLOBAL_REDIRECT_ADMIN_URL' => 'admin',
            )
        );
        $this->render('login/index', $data);
	}

    public function login()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            $authentication = $this->user_model->checkAndLoadUser(
                $this->input->post('username'),
                $this->input->post('password'),
                ROLE_USER_ADMIN
                );
            if (FALSE === $authentication) {
                $result['msg'] = lang('adc_wrong_username_or_password');
               throw new Exception;
            }

           $result = array(
               'status' => 'success',
           );
        } catch (Exception $ex) {

        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($result));
    }
}
