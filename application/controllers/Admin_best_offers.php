<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_best_offers extends ADC_admin
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('best_offers_model');

    }

    public function index()
    {
        $bestOffers = $this->best_offers_model->getAllBestOffers($this->getDefaultLanguageIsoCode());
        $data = array(
            'additionalJsFiles' => array(
                'public/js/vendor/jquery-ui.js',
                'public/js/pages/admin/best-offers/index.js'
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css',
                'public/css/pages/admin/best-offers/index.css'
            ),
            'pageData' => array(
                'bestOffers' => $bestOffers
            ),
            'globalJsVariables' => array(
                'GLOBAL_SAVE_ORDER_URL' => site_url('admin_best_offers/save_order'),
                'GLOBAL_DELETE_BEST_OFFER' => site_url('admin_best_offers/delete'),
            )
        );

        $this->render('best-offers/index', $data);
    }

    public function edit($id = 0)
    {
        echo '<pre>';print_r($_POST);die;
        if ($id) {
            $basicInfo = $this->best_offers_model->getBasicInfo($id);
            $basicInfo = $this->makeConvenientArray($basicInfo, array('content', 'name'));
            if (empty($basicInfo)) {
                $this->setFlashMessage('error', lang('adc_item_does_not_exist'));
            }
        }
        try {
            if (isset($_FILES['image']['name']) && $_FILES['image']['error'] && $_FILES['image']['error'] != UPLOAD_ERR_NO_FILE) {
                $this->setFlashMessage('error', lang('adc_server_error'));
                throw new Exception();
            }
            if (isset($_FILES['image']['name']) && !empty($_FILES['image']['tmp_name'])) {
                $config['upload_path'] = UPLOAD_PATH . UPLOAD_PATH_BEST_OFFERS;
                $config['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                $config['max_size'] = FILE_SIZE_GENERAL; //10MB
                $filenameArray = explode(".", $_FILES['image']['name']);
                $ext = end($filenameArray);
                //sometimes this library does not do right validation
                //so this is an additional check
                if (!in_array($ext, explode('|', $config['allowed_types']))) {
                    $this->setFlashMessage('error', sprintf(lang('adc_not_allowed_file_type'), $config['allowed_types']));
                    throw new Exception();
                }
                if ($_FILES['image']['size'] > FILE_SIZE_GENERAL) {
                    $this->setFlashMessage('error', sprintf(lang('adc_not_allowed_file_size'), (int)$config['max_size'] / 1024 / 1024));
                    throw new Exception();
                }
                $config['file_name'] = createFileName($_FILES['image']['name'], $ext);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $this->setFlashMessage('error', $this->upload->display_errors());
                    throw new Exception();
                }
                $imageConfig['image_library'] = 'gd2';
                $imageConfig['source_image'] = UPLOAD_PATH . UPLOAD_PATH_BEST_OFFERS . $config['file_name'];
                $imageConfig['create_thumb'] = FALSE;
                $imageConfig['maintain_ratio'] = FALSE;
                $imageConfig['width'] = BEST_OFFER_WIDTH;
                $imageConfig['height'] = BEST_OFFER_HEIGHT;
                $this->load->library('image_lib', $imageConfig);
                if (!$this->image_lib->resize()) {
                    $this->setFlashMessage('error', $this->image_lib->display_errors());
                    throw new Exception();
                }

                if ($id) {
                    //deleting old file
                    @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_BEST_OFFERS . $basicInfo['file']);
                }
            }
            if (!empty($_POST)) {
                if ($id == 0) {
                    $res = $this->best_offers_model->add(
                        $this->input->post('text'),
                        $this->input->post('name'),
                        $this->input->post('link'),
                        $this->input->post('incoming_outgoing'),
                        $config['file_name']);
                    if ($res == false) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $this->setFlashMessage('success', lang('adc_successfully_added'));
                    redirect('admin_best_offers/edit/' . $res);
                } else {
                    $fileName = (isset($config['file_name'])) ? $config['file_name'] : false;
                    if (!$this->best_offers_model->update(
                        $this->input->post('text'),
                        $this->input->post('name'),
                        $this->input->post('link'),
                        $this->input->post('incoming_outgoing'),
                        $id,
                        $fileName
                    )
                    ) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $basicInfo = $this->best_offers_model->getBasicInfo($id);
                    $basicInfo = $this->makeConvenientArray($basicInfo, array('content', 'name'));
                    $this->setFlashMessage('success', lang('adc_successfully_updated'));
                }
            }
        } catch (Exception $ex) {
            redirect('admin_best_offers');
        }

        $data = array(
            'additionalJsFiles' => array(
                'public/js/pages/admin/best-offers/edit.js'
            ),
            'globalJsVariables' => array(
                'isEdit' => ($id) ? 1 : 0
            )
        );
        if ($id) {
            $data['pageData'] = $basicInfo;
        }
        $this->render('best-offers/edit', $data);
    }

    public function save_order()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            if (!$this->best_offers_model->saveOrder($this->input->post('ids'))) {
                throw new Exception();
            }
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_updated')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function delete()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            $fileName = $this->best_offers_model->getFileNameById($this->input->post('id'));
            @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_BEST_OFFERS . $fileName);
            $this->best_offers_model->deleteBestOffer($this->input->post('id'));
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_removed')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


}
