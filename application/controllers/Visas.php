<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_site.php');


class Visas extends ADC_site
{


    public function index($lang, $type, $countrySlug = false)
    {

        $preloadArray = ($type == VISA_TYPE_TO_ARMENIA) ? array(37,38,39) : array(40,41,42,118,119,120);
        $this->preloadTranslations(array_merge($preloadArray,array(43,44,45)), $lang);
        $this->load->model('visas_model');
        $countries = $this->visas_model->getAllVisasByType($lang, $type);
        $aaDataWithout = array();
        $aaDataWith = array();
        if ($type == VISA_TYPE_TO_ARMENIA) {
            foreach ($countries[VISA_SUBTYPE_WITHOUT] as $country) {
                array_push(
                    $aaDataWithout,
                    array(
                        $country['order'],
                        '<img src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_VISAS . $country['file']) . '" alt="">',
                        $country['country'],
                        removePFromStartAndBegining($country['content']),

                    )
                );
//                var_dump($country);die;
            }
            foreach ($countries[VISA_SUBTYPE_BEFORE] as $country) {
                array_push(
                    $aaDataWith,
                    array(
                        $country['order'],
                        '<img src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_VISAS . $country['file']) . '" alt="">',
                        $country['country'],
                        removePFromStartAndBegining($country['content']),

                    )
                );
            }
//            var_dump($aaDataWithout);die;
            $data = array(
                'globalJsVariables' => array(
                    'aaDataWithout' => json_encode($aaDataWithout),
                    'aaDataWith' => json_encode($aaDataWith)
                ),
                'additionalJsFiles' => array(
                    'public/js/vendor/jquery.dataTables.min.js',
                    'public/js/vendor/DT_bootstrap.js',
                ),
                'additionalCssFiles' => array(
                    'public/css/vendor/datatables.bootstrap.css',
                ),
            );

            $this->render('visas/incoming', $lang, $data);
        }
        else {
//            $data = array(
//                'pageData' => array(
//                    'countries' => $countries,
//                ),
//                'globalJsVariables' => $globalJsVariables
//            );

            foreach ($countries[VISA_SUBTYPE_WITHOUT] as $country) {
                array_push(
                    $aaDataWithout,
                    array(
                        $country['order'],
                        '<img src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_VISAS . $country['file']) . '" alt="">',
                        $country['country'],
                        removePFromStartAndBegining($country['content'])
                    )
                );
            }

            foreach ($countries[VISA_SUBTYPE_BEFORE] as $country) {
                array_push(
                    $aaDataWith,
                    array(
                        $country['order'],
                        '<img src="' . site_url(UPLOAD_PATH_READ . UPLOAD_PATH_VISAS . $country['file']) . '" alt="">',
                        $country['country'],
                        removePFromStartAndBegining($country['content']),

                    )
                );
            }
            $globalJsVariables = array(
                'aaDataWithout' => json_encode($aaDataWithout),
                'aaDataWith' => json_encode($aaDataWith)
            );
        if ($countrySlug) {
            $this->load->model('countries_model');
            $countryInfo = $this->countries_model->getCountryNameBySlug($countrySlug, $lang);
            $pageData['countryInfo'] = $countryInfo;
            $globalJsVariables['GLOBAL_SELECTED_COUNTRY_NAME_FOR_VISAS'] = $countryInfo['name'];
            $globalJsVariables['GLOBAL_SELECTED_COUNTRY_SUBTYPE_FOR_VISAS'] = $countryInfo['subtype'];
        }
            $data = array(
                'globalJsVariables' => $globalJsVariables,
                'additionalJsFiles' => array(
                    'public/js/vendor/jquery.dataTables.min.js',
                    'public/js/vendor/DT_bootstrap.js',
                ),
                'additionalCssFiles' => array(
                    'public/css/vendor/datatables.bootstrap.css',
                ),
            );
            $this->render('visas/outgoing-educational', $lang, $data);
        }



    }

}
