<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/ADC_admin.php');


class Admin_tours extends ADC_admin
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('tours_model');
        $this->load->model('tour_gallery_model');
        $this->load->model('settings_model');
    }

    public function list_of_tours($type)
    {
        $tours = $this->tours_model->getAllToursByType($this->getDefaultLanguageIsoCode(), $type);
        $tourTypeName = $this->tours_model->tourTypes[$type];
        $data = array(
            'additionalJsFiles' => array(
                'public/js/vendor/jquery-ui.js',
                'public/js/pages/admin/tours/index.js'
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css',
                'public/css/pages/admin/tours/index.css'
            ),
            'pageData' => array(
                'tours' => $tours,
                'tourTypeName' => $tourTypeName,
                'type' => $type,
            ),
            'globalJsVariables' => array(
                'GLOBAL_DELETE_TOUR' => site_url('admin_tours/delete'),
                'GLOBAL_SAVE_ORDER_URL' => site_url('admin_tours/save_order'),
            )
        );

        $this->render('tours/index', $data);
    }
    public function save_order()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            if (!$this->tours_model->saveOrder($this->input->post('ids'))) {
                throw new Exception();
            }
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_updated')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function edit($type, $id = 0)
    {
        $this->load->model('countries_model');
        $tourTypeName = $this->tours_model->tourTypes[$type];
        $countriesForSelect = $this->countries_model->getCountriesForSelect($this->getDefaultLanguageIsoCode());
        unset($countriesForSelect[-1]);
        $selectedCountries = array();
        try {
            if ($id) {
                $basicInfo = $this->tours_model->getBasicInfo($id);
                $basicInfo = $this->makeConvenientArray($basicInfo, array('name', 'description', 'description_long', 'description_additional'));
                $selectedCountries = $this->tours_model->getSelectedCountries($id);
            }

            if (!empty($_POST)) {
                ini_set('upload_max_filesize', '100M');
                ini_set('post_max_size ', '100M');
                ini_set('max_execution_time ', 0);
                $this->load->library('image_lib');
                $this->load->library('upload');

                $thumb = $this->uploadThumbImage('thumb', UPLOAD_PATH_TOURS, TOUR_THUMB_WIDTH, TOUR_THUMB_HEIGHT);
                $mainImg = $this->uploadThumbImage('main_img', UPLOAD_PATH_TOURS, TOUR_MAIN_IMAGE_WIDTH, TOUR_MAIN_IMAGE_HEIGHT);
                if ($id) {
                    if ($mainImg) {
                        //deleting old file
                        @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_TOURS . $basicInfo['main_img']);
                    }
                    if (!is_null($basicInfo['thumb']) && $thumb != false) {
                        @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_TOURS . $basicInfo['thumb']);
                    }
                    $this->tours_model->update(
                        $thumb,
                        $mainImg,
                        $this->input->post('slug'),
                        $this->input->post('name'),
                        $this->input->post('description'),
                        $this->input->post('description_long'),
                        $this->input->post('description_additional'),
                        $this->input->post('countries_attached'),
                        $id
                    );
                    $basicInfo = $this->tours_model->getBasicInfo($id);
                    $basicInfo = $this->makeConvenientArray($basicInfo, array('name', 'description', 'description_long', 'description_additional'));
                    $selectedCountries = $this->tours_model->getSelectedCountries($id);
                    $this->setFlashMessage('success', lang('adc_successfully_updated'));
                } else {
                    $res = $this->tours_model->add(
                        $type,
                        $thumb,
                        $mainImg,
                        $this->input->post('slug'),
                        $this->input->post('name'),
                        $this->input->post('description'),
                        $this->input->post('description_long'),
                        $this->input->post('description_additional'),
                        $this->input->post('countries_attached')
                    );
                    $this->setFlashMessage('success', lang('adc_successfully_added'));
                    redirect('admin_tours/edit/' .$type . '/' . $res);
                }

            }
        } catch (Exception $ex) {
            redirect('admin_tours/' . $type);
        }
        $data = array(
            'additionalJsFiles' => array(
                'public/js/vendor/selectize.js',
                'public/js/pages/admin/tours/edit.js'
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/selectize.bootstrap3.min.css'
            ),
            'globalJsVariables' => array(
                'isEdit' => ($id) ? 1 : 0
            )
        );
        if ($id) {
            $data['pageData'] = $basicInfo;
        } else {
            $data['pageData'] = array();
        }
        $data['pageData']['tourTypeName'] = $tourTypeName;
        $data['pageData']['type'] = $type;
        $data['pageData']['countriesForSelect'] = $countriesForSelect;
        $data['pageData']['selectedCountries'] = $selectedCountries;
        $this->render('tours/edit', $data);
    }

    public function delete()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            $galleryFileNames = $this->tour_gallery_model->getFilesByTourId($this->input->post('id'));
            foreach ($galleryFileNames as $galleryFileName) {
                @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_TOUR_GALLERY . $galleryFileName['file']);
            }
            $this->tour_gallery_model->deleteTourGalleryImagesByTourId($this->input->post('id'));
            $filesToDelete = $this->tours_model->getFileNamesById($this->input->post('id'));
            foreach ($filesToDelete as $fileName) {
                if (!is_null($fileName)) {
                    @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_TOURS . $fileName);
                }
            }
            $this->tours_model->deleteTour($this->input->post('id'));

            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_removed')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function gallery($tourType, $tourId)
    {
        $tourTypeName = $this->tours_model->tourTypes[$tourType];
        $tourName = $this->tours_model->getTourNameByIdAndLang($this->getDefaultLanguageIsoCode(), $tourId);
        $tourImages = $this->tour_gallery_model->getImagesForTour($this->getDefaultLanguageIsoCode(), $tourId);
        $data = array(
            'additionalJsFiles' => array(
                'public/js/vendor/jquery-ui.js',
                'public/js/pages/admin/tour-gallery/index.js'
            ),
            'additionalCssFiles' => array(
                'public/css/vendor/jquery-ui/themes/smoothness/jquery-ui.css',
                'public/css/pages/admin/tour-gallery/index.css'
            ),
            'pageData' => array(
                'tourImages' => $tourImages,
                'tourName'   => $tourName['name'],
                'tourId'     => $tourId,
                'tourTypeName' => $tourTypeName,
                'tourType' => $tourType,
            ),
            'globalJsVariables' => array(
                'GLOBAL_SAVE_ORDER_URL' => site_url('admin_tours/save_gallery_order'),
                'GLOBAL_DELETE_GALLERY_IMAGE' => site_url('admin_tours/delete_gallery_image'),
            )
        );

        $this->render('tour-gallery/index', $data);
    }


    public function gallery_edit($tourType, $tourId, $id = 0)
    {
        $tourTypeName = $this->tours_model->tourTypes[$tourType];
        if ($id) {
            $basicInfo = $this->tour_gallery_model->getBasicInfo($id);
            $basicInfo = $this->makeConvenientArray($basicInfo, array('content'));
            if (empty($basicInfo)) {
                $this->setFlashMessage('error', lang('adc_item_does_not_exist'));
            }
        }
        try {
            if (isset($_FILES['image']['name']) && $_FILES['image']['error'] && $_FILES['image']['error'] != UPLOAD_ERR_NO_FILE) {
                $this->setFlashMessage('error', lang('adc_server_error'));
                throw new Exception();
            }
            if (isset($_FILES['image']['name']) && !empty($_FILES['image']['tmp_name'])) {
                $config['upload_path'] = UPLOAD_PATH . UPLOAD_PATH_TOUR_GALLERY;
                $config['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                $config['max_size'] = FILE_SIZE_GENERAL; //10MB
                $filenameArray = explode(".", $_FILES['image']['name']);
                $ext = end($filenameArray);
                //sometimes this library does not do right validation
                //so this is an additional check
                if (!in_array($ext, explode('|', $config['allowed_types']))) {
                    $this->setFlashMessage('error', sprintf(lang('adc_not_allowed_file_type'), $config['allowed_types']));
                    throw new Exception();
                }
                if ($_FILES['image']['size'] > FILE_SIZE_GENERAL) {
                    $this->setFlashMessage('error', sprintf(lang('adc_not_allowed_file_size'), (int)$config['max_size'] / 1024 / 1024));
                    throw new Exception();
                }
                $config['file_name'] = createFileName($_FILES['image']['name'], $ext);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $this->setFlashMessage('error', $this->upload->display_errors());
                    throw new Exception();
                }
                $imageConfig['image_library'] = 'gd2';
                $imageConfig['source_image'] = UPLOAD_PATH . UPLOAD_PATH_TOUR_GALLERY . $config['file_name'];
                $imageConfig['create_thumb'] = FALSE;
                $imageConfig['maintain_ratio'] = FALSE;
                $imageConfig['width'] = TOUR_GALLERY_IMAGE_WIDTH;
                $imageConfig['height'] = TOUR_GALLERY_IMAGE_HEIGHT;
                $this->load->library('image_lib', $imageConfig);
                if (!$this->image_lib->resize()) {
                    $this->setFlashMessage('error', $this->image_lib->display_errors());
                    throw new Exception();
                }

                if ($id) {
                    //deleting old file
                    @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_TOUR_GALLERY . $basicInfo['file']);
                }
            }
            if (!empty($_POST)) {
                if ($id == 0) {
                    $res = $this->tour_gallery_model->add(
                        $this->input->post('text'),
                        $config['file_name'],
                        $tourId
                        );
                    if ($res == false) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $this->setFlashMessage('success', lang('adc_successfully_added'));
                    redirect('admin_tours/gallery_edit/' . $tourType . '/' . $tourId . '/' . $res);
                } else {
                    $fileName = (isset($config['file_name'])) ? $config['file_name'] : false;
                    if (!$this->tour_gallery_model->update(
                        $this->input->post('text'),
                        $id,
                        $fileName
                    )
                    ) {
                        $this->setFlashMessage('error', lang('adc_server_error'));
                        throw new Exception();
                    }
                    $basicInfo = $this->tour_gallery_model->getBasicInfo($id);
                    $basicInfo = $this->makeConvenientArray($basicInfo, array('content'));
                    $this->setFlashMessage('success', lang('adc_successfully_updated'));
                }
            }
        } catch (Exception $ex) {
            redirect('admin_tours/gallery/' . $tourId);
        }

        $data = array();
        if ($id) {
            $data['pageData'] = $basicInfo;
        } else {
            $data['pageData'] = array();
        }
        $data['pageData']['tourId'] = $tourId;
        $tourName = $this->tours_model->getTourNameByIdAndLang($this->getDefaultLanguageIsoCode(), $tourId);
        $data['pageData']['tourName'] = $tourName['name'];
        $data['pageData']['tourTypeName'] = $tourTypeName;
        $data['pageData']['tourType'] = $tourType;
        $this->render('tour-gallery/edit', $data);
    }

    public function save_gallery_order()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            if (!$this->tour_gallery_model->saveOrder($this->input->post('ids'))) {
                throw new Exception();
            }
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_updated')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


    public function delete_gallery_image()
    {
        $result = array(
            'status' => 'error',
            'msg'    => lang('adc_server_error')
        );
        try {
            $fileName = $this->tour_gallery_model->getFileNameById($this->input->post('id'));
            @unlink(UNLINK_PATH_UPLOADS . UPLOAD_PATH_TOUR_GALLERY . $fileName);
            $this->tour_gallery_model->deleteTourGalleryImage($this->input->post('id'));
            $result = array(
                'status' => 'success',
                'msg'    => lang('adc_successfully_removed')
            );
        } catch (Exception $ex) {

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


}
