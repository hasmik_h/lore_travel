<?php
$lang['adc_hy'] = 'Հայերեն';
$lang['adc_ru'] = 'Ռուսերեն';
$lang['adc_en'] = 'Անգլերեն';
$lang['adc_ge'] = 'Վրացերեն';
$lang['adc_fr'] = 'Ֆրանսերեն';
$lang['adc_es'] = 'Իսպանեռեն';
$lang['adc_de'] = 'Գերմաներեն';
$lang['adc_it'] = 'Իտալերեն';
$lang['adc_tr'] = 'Թուրքերեն';
$lang['adc_kz'] = 'Ղազախերեն';
$lang['adc_by'] = 'Բելորուսերեն';
$lang['adc_gr'] = 'Հուներեն';
$lang["adc_lorem_ipsum"] = "Լորեմ Իպսում";
$lang["adc_server_error"] = "<ul><li>Սերվերային պրոբլեմ,</li><li> Խնդրում ենք փորձել մի փոքր ուշ!</li>";
$lang["adc_administration_system"] = "Ադմինիստրացիոն համակարգ";
$lang["adc_username"] = "Օգտանուն";
$lang["adc_password"] = "Գաղտնաբառ";
$lang["adc_sign_in"] = "Մուտք!";
$lang["adc_wrong_username_or_password"] = "Սխալ օգտանուն կամ գաղտնաբառ";
$lang["adc_admin_settings"] = "Փոխել գաղտնաբառը";
$lang["adc_admin_language"] = "Ադմինիստրացիոն համակարգի լեզուն";
$lang["adc_logout"] = "Ելք";
$lang["adc_old_password"] = "Գործող գաղտնաբառ";
$lang["adc_new_password"] = "Նոր գաղտնաբառ";
$lang["adc_repeat_new_password"] = "Նոր գաղտնաբառի կրկնություն";
$lang["adc_save"] = "Պահպանել";
$lang["adc_reset"] = "Ջնջել";
$lang["adc_close"] = "Փակել";
$lang["adc_cancel"] = "Չեղյալ հայտարարել";
$lang["adc_passwords_do_not_match"] = "Նոր գաղտնաբառերը չեն համընկնում";
$lang["adc_current_password_is_wrong"] = "Գործող գաղտնաբառը սխալ է";
$lang["adc_successfully_updated"] = "Հաջողությամբ փոխվեց";
$lang["adc_successfully_added"] = "Հաջողությամբ ավելացվեց";
$lang["adc_settings"] = "Կարգավորումներ";
$lang["adc_languages"] = "Լեզուներ";
$lang["adc_language"] = "Լեզու";
$lang["adc_common"] = "Ընդհանուր";
$lang['adc_languages_on_right'] = "Այստեղ ակտիվ լեզուներն են: Դուք կարող եք <<բռնել>> լեզուն և փոխել նրա դիրքը, ինչպես նաև ջնջել, կամ խմբագրել նրա հետ կապված ինֆորմացիան: Ուշադրություն՝ փոոփխությունները կպահպանվեն միայն <<" . $lang["adc_save"] . ">> սեղմելուց հետո:";
$lang['adc_languages_on_left']  = "Այստեղ Ոչ ակտիվ լեզուներն են:";
$lang['adc_remove'] = "Ջնջել";
$lang['adc_edit']   = "Խմբագրել";
$lang['adc_deactivate']   = "Դեակտիվացնել";
$lang['adc_activate']   = "Ակտիվացնել";
$lang['adc_are_you_sure']   = "Դուք վստա՞հ եք";
$lang['adc_default_language']   = "Լռելյայն լեզու";
$lang['adc_warning_deactivate_language']   = "Դուք պատրաստվում եք դեակտիվացնել լեզուն: Այդ դեպքում տվյալ լեզվով ամբոզջ ինֆորմացիան անվերադարձ կկորի: Եթե դուք վստահ չեք, որ հետո այն կարող է Ձեզ պետք գալ, դուք կարող եք թաքցնել այն, խմբագրելով լեզուն:";
$lang['adc_language_original_name']   = "Լեզվի անունը օրիգինալ լեզվով";
$lang['adc_hidden']   = "Թաքցրած";
$lang['adc_not_hidden']   = "Չթաքցրած";
$lang['adc_choose_another_default_language'] = "Դուք չեք կարող դեակտիվացնել լռելյայն լեզուն:Դեակտիվացնելու համար խնդրում եմ փոխեք լռելյայն լեզուն";
$lang['adc_loading'] = "Բեռնվում է";
$lang['adc_text'] = "Տեքստ";
$lang['adc_slider'] = "Սլայդեր";
$lang['adc_image'] = "Նկար";
$lang['adc_best_size']  = "Լավագույն չափս՝";
$lang['adc_not_allowed_file_type']  = "<ul><li>Դուք փորձում եք բեռնել չթույլատրված ֆայլի տիպ:</li><li>Թույատրված տիպերն են %1s:</li>";
$lang['adc_not_allowed_file_size']  = "Բեռնվող ֆայլի չափը չպետք է գերազանցի %1d MB";
$lang['adc_item_does_not_exist']  = "Գոյություն չունի";
$lang['adc_title'] = 'Վերնագիր';
$lang['adc_link'] = 'Հղում';
$lang['adc_link_text'] = 'Հղման տեքստ';
$lang['adc_add'] = 'Ավելացնել';
$lang['adc_are_you_sure'] = 'Դուք վստա՞հ եք';
$lang['adc_yes'] = 'Այո';
$lang['adc_successfully_removed'] = 'Հաջողությամբ ջնջվեց';
$lang['adc_home'] = 'Գլխավոր էջ';
$lang['adc_best_offer'] = 'Թեժ տուրեր';
$lang['adc_countries'] = 'Երկրներ';
$lang['adc_country'] = 'Երկիր';
$lang['adc_list'] = 'Ցանկ';
$lang['adc_map'] = 'Քարտեզ';
$lang['adc_name'] = 'Անուն';
$lang['adc_comment_on_hover'] = 'Կարճ նկարագրություն';
$lang['adc_comment_on_click'] = 'Երկար նկարագրություն';
$lang['adc_slug'] = 'Սլագ';
$lang['adc_flag'] = 'Դրոշ';
$lang['adc_image'] = 'Նկար';
$lang['adc_color_default'] = 'Գույն ըստ լռելյայն';
$lang['adc_color_hover'] = 'Գույն, որը երևալու է երկրի վրա մկնիկի սլաքը պահելիս';
$lang['adc_width'] = 'Լայնություն';
$lang['adc_height'] = 'Բարձրություն';
$lang['adc_image_little'] = 'Փոքր նկար';
$lang['adc_image_opening'] = 'Բացվող նկար';
$lang['adc_slug_explanation_countries'] = 'Կարող է պարունակել միայն լատինական այբուբենի փոքրատառեր, թվեր և \'-\' անգլերեն լեզվով:  Օրինակ <<Հարավ Աֆրիկյան Հանրապետության>> համար պետք է գրել <<south-african-republic>>:';
$lang['adc_slug_explanation_tours'] = 'Կարող է պարունակել միայն լատինական այբուբենի փոքրատառեր, թվեր և \'-\'  անգլերեն լեզվով:  Օրինակ <<Անմոռանալի ճանապարհորդություն դեպի Արևելյան Եվրոպա>>-ի համար պետք է գրել <<south-african-republic>>:';
$lang['adc_meta_tags'] = 'Մեթա թագեր';
$lang['adc_about_us'] = 'Մեր մասին';
$lang['adc_tours_incoming'] = 'Ներգնա տուրեր';
$lang['adc_tours_outgoing'] = 'Արտագնա տուրեր';
$lang['adc_tours_educational'] = 'Կրթական տուրեր';
$lang['adc_destination'] = 'Ինտերակտիվ քարտեզ';
$lang['adc_book_hotel'] = 'Հյուրանոցների ամրագրում';
$lang['adc_visas_to_armenia'] = 'Վիզաներ դեպի Հայաստան';
$lang['adc_visas_from_armenia'] = 'Վիզաներ Հայաստանից';
$lang['adc_not_decided'] = 'Չորոշված';
$lang['adc_contact_us'] = 'Հետադարձ կապ';
$lang['adc_meta_og_type'] = 'Meta og:type';
$lang['adc_meta_og_type_explanation'] = 'Facebook-ում կայքը share անելիս, նշվում է որպես գործունեության տեսակ: Հիմնականում լինում է <<company>>';
$lang['adc_meta_og_image'] = 'og:image';
$lang['adc_meta_og_image_explanation'] = 'Facebook-ում կայքը share անելիս, երևում է այս նկարը';
$lang['adc_meta_og_url'] = 'Meta og:url';
$lang['adc_meta_og_url_explanation'] = 'Facebook-ում կայքը share անելիս, ցույց է տալիս կայքի հասցեն: Հիմանականում լինում է կայքի գլխավոր հասցեն';
$lang['adc_meta_og_title'] = 'Meta og:title';
$lang['adc_meta_og_title_explanation'] = 'Facebook-ում կայքը share անելիս, երևում է որպես վերնագիր';
$lang['adc_meta_og_description'] = 'Meta og:description';
$lang['adc_meta_og_description_explanation'] = 'Facebook-ում կայքը share անելիս, երևում է որպես նկարագրություն';
$lang['adc_meta_keywords'] = 'Meta keywords';
$lang['adc_meta_keywords_explanation'] = 'Սրանք ստորակետով բաժանվող բառեր, կամ կարճ արտահայտություններ են, որոնք նկարագրում են Էջը: Խորհուրդ է տրվում էջի վրա ունենալ 8-15 բառ/արտահայտություն, ընդհանուր 70-160 նիշերով: Օգտագործվում են Google-ի և այլ Search Engine-ների կողմից, կայքը փնտրելու համար';
$lang['adc_meta_description'] = 'Meta description';
$lang['adc_meta_description_explanation'] = 'Էջի նկարագրությունը, որը երևում է, Search Engine-ներում(Google, Bing, Yandex...), որպես փնտրման արդյունք: Նիշերի օպտիմալ քանակն է 70-160';
$lang['adc_color_default'] = 'Երկրների գույնը ըստ լռելյայն';
$lang['adc_color_default_hover'] = 'Երկրների գույնը ըստ լռելյայն, մկնիկը անցկացնելիս';
$lang['adc_not_selected'] = 'Ընտրված չէ';
$lang['adc_feedbacks'] = 'Հաճախորդների կարծիքներ';
$lang['adc_customer_name'] = 'Հաճախորդների անունը';
$lang['adc_score'] = 'Գնահատական';
$lang['adc_gallery'] = 'Պատկերասրահ';
$lang['adc_tours'] = 'Տուրեր';
$lang['adc_incoming'] = 'Ներգնա';
$lang['adc_outgoing'] = 'Արտագնա';
$lang['adc_educational'] = 'Կրթական';
$lang['adc_main_image'] = 'Գլխավոր նկար';
$lang['adc_description'] = 'Նկարագրություն';
$lang['adc_short_description'] = 'Կարճ նկարագրություն';
$lang['adc_long_description'] = 'Երկար նկարագրություն';
$lang['adc_attached_countries'] = 'Երկրներ';
$lang['adc_you_can_select_several'] = 'Դուք կարող եք ընտրել մի քանիսը';
$lang['adc_source_code_protection'] = 'Տվյալ նախագծի ծրագրային կոդը պաշտպանված է Հայաստանի Հանրապետության մտավոր սեփականության մասին օրենքով:';
$lang['adc_ADC_production'] = 'ADC պրոդակշն';
$lang['adc_common_settings'] = 'Ընդհանուր կարգավորումներ';
$lang['adc_mail_contact_us'] = '<<Հետադարձ կապ՚՚՚՚>> էջի էլ. հասցե';
$lang['adc_mail_lets_plan_your_trip'] = '<<Պլանավորենք ձեր հանգիստ>>-ի էլ. հասցե';
$lang['adc_mail_order_tour'] = 'Տուրի պատվերի  էլ. հասցե';
$lang['adc_url_facebook'] = 'Ֆեյսբւքի էջի հասցե';
$lang['adc_url_google_plus'] = 'Գուգլ պլյուսի էջի հասցե';
$lang['adc_twitter'] = 'Թվիթերի էջի հասցե';
$lang['adc_pinterest'] = 'Պինթեսրեսթի էջի հասցե';
$lang['adc_linked_in'] = 'Լինքդինի էջի հասցե';
$lang['adc_about_us'] = 'Մեր մասին';
$lang['adc_why_us'] = 'Ինչու մենք';
$lang['adc_visas'] = 'Վիզաներ';
$lang['adc_to_armenia'] = 'Դեպի Հայաստան';
$lang['adc_from_armenia'] = 'Հայաստանից';
$lang['adc_without_visa'] = 'Առանց վիզա կամ Վիզան սահմանին';
$lang['adc_visa_on_border'] = 'Վիզան սահմանին';
$lang['adc_visa_before_trip'] = 'Մինչև Ճանապարհորդություն';
$lang['adc_translations'] = 'Թարգմանություններ';
$lang['adc_partners'] = 'Գործընկերներ';




















