<?php
$lang['adc_hy'] = 'по Армянский';
$lang['adc_ru'] = 'по Русский';
$lang['adc_en'] = 'по Английский';
$lang['adc_ge'] = 'по Грузинский';
$lang['adc_fr'] = 'по Французский';
$lang['adc_es'] = 'по Испанский';
$lang['adc_de'] = 'по Немецкий';
$lang['adc_it'] = 'по Италянский';
$lang['adc_tr'] = 'по Турецкий';
$lang['adc_kz'] = 'по Казахский';
$lang['adc_by'] = 'по Белорусский';
$lang['adc_gr'] = 'по Греческий';
$lang["adc_lorem_ipsum"] = "Лорем Ипсум";
$lang["adc_server_error"] = "<ul><li>Серверная Ошибка,</li><li> Пожалуйста попробуйте еще раз!</li>";
$lang["adc_administration_system"] = "Административная система";
$lang["adc_username"] = "Имя пользователя";
$lang["adc_password"] = "Пароль";
$lang["adc_sign_in"] = "Войти!";
$lang["adc_wrong_username_or_password"] = "Неправильный имя пользователя или пароль";
$lang["adc_admin_settings"] = "Поменять пароль";
$lang["adc_admin_language"] = "Язык административной системы";
$lang["adc_logout"] = "Выйти";
$lang["adc_old_password"] = "Действующий пароль";
$lang["adc_new_password"] = "Новый пароль";
$lang["adc_repeat_new_password"] = "Повтор нового пароля";
$lang["adc_save"] = "Сохранить";
$lang["adc_reset"] = "Сброс";
$lang["adc_close"] = "Закрыть";
$lang["adc_cancel"] = "Отменить";
$lang["adc_passwords_do_not_match"] = "Новые пароли не совпадают";
$lang["adc_current_password_is_wrong"] = "Настоящий пароль неправильный";
$lang["adc_successfully_updated"] = "Успешно обновлено";
$lang["adc_successfully_added"] = "Успешно добавлено";
$lang["adc_settings"] = "Настройки";
$lang["adc_languages"] = "Языки";
$lang["adc_language"] = "Язык";
$lang["adc_common"] = "Общие";
$lang['adc_languages_on_right'] = "Здесь активные языки. Можно изменить их порядок, перетаскивая их. Так же вы можете удалить, или редактировать их. Вниание: Изменения будут сохранени только после того как вы нажмете '" . $lang["adc_save"] . "'";
$lang['adc_languages_on_left']  = "Здесь не активные языки.";
$lang['adc_remove'] = "Удалить";
$lang['adc_edit']   = "Редактировать";
$lang['adc_deactivate']   = "Деактивировать";
$lang['adc_activate']   = "Активировать";
$lang['adc_are_you_sure']   = "Вы уверены?";
$lang['adc_default_language']   = "Язык по умолчанию";
$lang['adc_warning_deactivate_language']   = "Вы собираетесь деактивировать язык. В этом случае информация на этом языке будет потеряна. Если вы не уверены, чта информация не понадобится вам в будущем, можно спрятать язик, редактировая ее.";
$lang['adc_language_original_name']   = "Имя языка по оригинальному языку";
$lang['adc_hidden']   = "Скрытый";
$lang['adc_not_hidden']   = " Не Скрытый";
$lang['adc_choose_another_default_language'] = "Вы не можете деактивировать язык по умолчанию. Для деактивации, пожалуйста измените язык по умолчанию";
$lang['adc_loading'] = "Загрузка";
$lang['adc_text'] = "Текст";
$lang['adc_slider'] = "Слайдер";
$lang['adc_image'] = "Картинка";
$lang['adc_best_size']  = "Лучший размер:";
$lang['adc_not_allowed_file_type']  = "<ul><li>Вы пытаетесь загрузить файл, тип которого не поддерживается</li><li>Поддержиемие типи: %1s</li>";
$lang['adc_not_allowed_file_size']  = "Загружаемый файл не может быть болше %1d MB";
$lang['adc_item_does_not_exist']  = "Не существует";
$lang['adc_title'] = 'Имя';
$lang['adc_link'] = 'Ссылка';
$lang['adc_link_text'] = 'Текст ссылкы';
$lang['adc_add'] = 'Добавыть';
$lang['adc_are_you_sure'] = 'Вы уверены?';
$lang['adc_yes'] = 'Да';
$lang['adc_successfully_removed'] = 'Успешно удалено';
$lang['adc_home'] = 'Главная страница';
$lang['adc_best_offer'] = 'Горящие путевки';
$lang['adc_countries'] = 'Страны';
$lang['adc_country'] = 'Стран';
$lang['adc_list'] = 'Список';
$lang['adc_map'] = 'Карта';
$lang['adc_name'] = 'Имя';
$lang['adc_comment_on_hover'] = 'Короткый Коментарий';
$lang['adc_comment_on_click'] = 'Длинный Коментарий';
$lang['adc_slug'] = 'Слаг (Только буквы)';
$lang['adc_flag'] = 'Флаг';
$lang['adc_image'] = 'Картинка';
$lang['adc_color_default'] = 'Цвет по умолчанию';
$lang['adc_color_hover'] = 'Цвет при отведения мышы';
$lang['adc_width'] = 'Ширина';
$lang['adc_height'] = 'Высота';
$lang['adc_image_little'] = 'Маленькая картинка';
$lang['adc_image_opening'] = 'Открывающися картинка';
$lang['adc_slug_explanation_countries'] = 'Можно писать только Латинские строчные буквы, цифры и знак \'-\' на английском язике. К примеру для Южно африканской республики нужно ввести \'south-african-republic\'.';
$lang['adc_slug_explanation_tours'] = 'Можно писать только Латинские строчные буквы, цифры и знак \'-\'  на английском язике. К примеру для \' Незабываемое путеществие в Восточную Европу\' нужно ввести \'unforgettable-trip-to-eastern-europe\'.';
$lang['adc_meta_tags'] = 'Мета тагы';
$lang['adc_about_us'] = 'О нас';
$lang['adc_tours_incoming'] = 'Входящие туры';
$lang['adc_tours_outgoing'] = 'Исходящие туры';
$lang['adc_tours_educational'] = 'Познавательные туры';
$lang['adc_destination'] = 'Интерактивная карта';
$lang['adc_book_hotel'] = 'Бронировать отель';
$lang['adc_visas_to_armenia'] = 'Визы в Армению';
$lang['adc_visas_from_armenia'] = 'Визы из Армении';
$lang['adc_not_decided'] = 'Не решил';
$lang['adc_contact_us'] = 'Связь';
$lang['adc_meta_og_type'] = 'Meta og:type';
$lang['adc_meta_og_type_explanation'] = 'Если поделится сайтом на Фэйсбуке, это будет воспринематся как тип вашего предприятия, в основном ставится \'company\'';
$lang['adc_meta_og_image'] = 'og:image';
$lang['adc_meta_og_image_explanation'] = 'Если поделится сайтом на Фэйсбуке, эта картинка будет видна';
$lang['adc_meta_og_url'] = 'Meta og:url';
$lang['adc_meta_og_url_explanation'] = 'Если поделится сайтом на Фэйсбуке,  это будет воспринематся как главная страница сайта. В основном ставится адрес главной страницы сайта';
$lang['adc_meta_og_title'] = 'Meta og:title';
$lang['adc_meta_og_title_explanation'] = 'Если поделится сайтом на Фэйсбуке, это будет показано как заглавие поста';
$lang['adc_meta_og_description'] = 'Meta og:description';
$lang['adc_meta_og_description_explanation'] = 'Если поделится сайтом на Фэйсбуке, это будет показано как описание поста';
$lang['adc_meta_keywords'] = 'Meta keywords';
$lang['adc_meta_keywords_explanation'] = 'Разделенные запятой слова или фразы, которые описивают страницы. Рекомендуется иметь 8-15 слов/фраз, с длиной в 70-160 символов. Исползуется Гуглом и другими поясковиками , чтоб найти страницу.';
$lang['adc_meta_description'] = 'Meta description';
$lang['adc_meta_description_explanation'] = 'Oписание страници, которая показивается как результат поиска в поисковиках (Google, Bing, Yandex...). Оптимальная длина 70-160 символов';
$lang['adc_color_default'] = 'Цвет стран по умолчанию';
$lang['adc_color_default_hover'] = 'Цвет стран по умолчанию, при наведении мишкой';
$lang['adc_not_selected'] = 'Не выбран';
$lang['adc_feedbacks'] = 'Мнения клиентов';
$lang['adc_customer_name'] = 'Имя клиента';
$lang['adc_score'] = 'Оценка';
$lang['adc_gallery'] = 'Галлерея';
$lang['adc_tours'] = 'Туры';
$lang['adc_incoming'] = 'Вьездные';
$lang['adc_outgoing'] = 'Выездные';
$lang['adc_educational'] = 'Образовательные';
$lang['adc_main_image'] = 'Главная картинка';
$lang['adc_description'] = 'Описание';
$lang['adc_short_description'] = 'Короткое описание';
$lang['adc_long_description'] = 'Длинное описание';
$lang['adc_attached_countries'] = 'Страны';
$lang['adc_you_can_select_several'] = 'Вы можете указать несколько';
$lang['adc_source_code_protection'] = 'Исходний код данного проекта защищен законом о интеллектуальной собственности Республики Армения.';
$lang['adc_adc_production'] = 'ADC продакшн';
$lang['adc_common_settings'] = 'Общие настройки';
$lang['adc_mail_contact_us'] = 'Эмэйл для страницы "Связь"';
$lang['adc_mail_lets_plan_your_trip'] = 'Эмэйл для "Запланируем ваш отдых"';
$lang['adc_mail_order_tour'] = 'Эмэйл для заказа тура';
$lang['adc_url_facebook'] = 'Адресс страницы Фэйсбука';
$lang['adc_url_google_plus'] = 'Адресс страницы Гугл Плюс';
$lang['adc_twitter'] = 'Адресс страницы Твитера';
$lang['adc_pinterest'] = 'Адресс страницы Пинтереста';
$lang['adc_linked_in'] = 'Адресс страницы Линкдина';
$lang['adc_about_us'] = 'О нас';
$lang['adc_why_us'] = 'Почему мы';
$lang['adc_visas'] = 'Визы';
$lang['adc_to_armenia'] = 'В Армению';
$lang['adc_from_armenia'] = 'Из Армении';
$lang['adc_without_visa'] = 'Без визы или при прибитии';
$lang['adc_visa_on_border'] = 'На границе';
$lang['adc_visa_before_trip'] = 'До путешествия';
$lang['adc_translations'] = 'Переводы';
$lang['adc_partners'] = 'Партнеры';





























