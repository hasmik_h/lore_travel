<?php
$lang['adc_hy'] = 'Armenian';
$lang['adc_ru'] = 'Russian';
$lang['adc_en'] = 'English';
$lang['adc_ge'] = 'Georgian';
$lang['adc_fr'] = 'French';
$lang['adc_es'] = 'Spanish';
$lang['adc_de'] = 'German';
$lang['adc_it'] = 'Italian';
$lang['adc_tr'] = 'Turkish';
$lang['adc_kz'] = 'Kazakh';
$lang['adc_by'] = 'Belorussian';
$lang['adc_gr'] = 'Greek';
$lang["adc_lorem_ipsum"] = "Lorem Ipsum";
$lang["adc_server_error"] = "<ul><li>Server Error,</li><li> Please Try Again!</li>";
$lang["adc_administration_system"] = "Administration System";
$lang["adc_username"] = "Username";
$lang["adc_password"] = "Password";
$lang["adc_sign_in"] = "Sign In!";
$lang["adc_wrong_username_or_password"] = "Wrong Username or Password";
$lang["adc_admin_settings"] = "Change Password";
$lang["adc_admin_language"] = "Administration System Language";
$lang["adc_logout"] = "Logout";
$lang["adc_old_password"] = "Current Password";
$lang["adc_new_password"] = "New Password";
$lang["adc_repeat_new_password"] = "Retype New Password";
$lang["adc_save"] = "Save";
$lang["adc_reset"] = "Reset";
$lang["adc_close"] = "Close";
$lang["adc_cancel"] = "Cancel";
$lang["adc_passwords_do_not_match"] = "New Passwords Do Not Match";
$lang["adc_current_password_is_wrong"] = "Current Password Is Wrong";
$lang["adc_successfully_updated"] = "Successfully Updated";
$lang["adc_successfully_added"] = "Successfully Added";
$lang["adc_settings"] = "Settings";
$lang["adc_languages"] = "Languages";
$lang["adc_language"] = "Language";
$lang["adc_common"] = "Common";
$lang['adc_languages_on_right'] = "We have active languages on this side.You can change their order by dragging and dropping them. You can also remove and edit them. Caution: The changes will be saved after you click '" . $lang["adc_save"] . "'";
$lang['adc_languages_on_left']  = "We have inactive languages on this side";
$lang['adc_remove'] = "Remove";
$lang['adc_edit']   = "Edit";
$lang['adc_deactivate']   = "Deactivate";
$lang['adc_activate']   = "Activate";
$lang['adc_are_you_sure']   = "Are you sure?";
$lang['adc_default_language']   = "Default language";
$lang['adc_warning_deactivate_language']   = "You are going to deactivate language. In this case all the information for this language will be lost. If you are not sure that you will not need it in the future, you can hide it, by editing the language's options";
$lang['adc_language_original_name']   = "Language name in original language";
$lang['adc_hidden']   = "Hidden";
$lang['adc_not_hidden']   = "Not Hidden";
$lang['adc_choose_another_default_language'] = "You can not deactivate the default language. For deactivating, please change the default language";
$lang['adc_loading'] = "Loading";
$lang['adc_text'] = "Text";
$lang['adc_slider'] = "Slider";
$lang['adc_image'] = "Image";
$lang['adc_best_size']  = "Best size:";
$lang['adc_not_allowed_file_type']  = "<ul><li>You are trying to upload a file, which type is not supported.</li><li>Supported types are %1s</li>";
$lang['adc_not_allowed_file_size']  = "Uploading File Size shall not be bigger then %1d MB";
$lang['adc_item_does_not_exist']  = "Item does not exist";
$lang['adc_title'] = 'Title';
$lang['adc_link'] = 'Link';
$lang['adc_link_text'] = 'Link text';
$lang['adc_add'] = 'Add';
$lang['adc_are_you_sure'] = 'Are you sure?';
$lang['adc_yes'] = 'Yes';
$lang['adc_successfully_removed'] = 'Successfully removed';
$lang['adc_home'] = 'Home page';
$lang['adc_best_offer'] = 'Best offers';
$lang['adc_countries'] = 'Countries';
$lang['adc_country'] = 'Country';
$lang['adc_list'] = 'List';
$lang['adc_map'] = 'Map';
$lang['adc_name'] = 'Name';
$lang['adc_comment_on_hover'] = 'Short Comment';
$lang['adc_comment_on_click'] = 'Long Comment';
$lang['adc_slug'] = 'Slug';
$lang['adc_flag'] = 'Flag';
$lang['adc_image'] = 'Image';
$lang['adc_color_default'] = 'Default color';
$lang['adc_color_hover'] = 'Color on mouse over';
$lang['adc_width'] = 'Width';
$lang['adc_height'] = 'Height';
$lang['adc_image_little'] = 'Little image';
$lang['adc_image_opening'] = 'Opening Image';
$lang['adc_slug_explanation_countries'] = 'Can contain only Latin lowercase letters, numbers and the \'-\' sign in English language. For example for South African Republic you shall write \'south-african-republic\'.';
$lang['adc_slug_explanation_tours'] = 'Can contain only Latin lowercase letters, numbers and the \'-\' sign  in English language. For example for \' Unforgettable trip to Eastern Europe\' you shall write \'unforgettable-trip-to-eastern-europe\'.';
$lang['adc_meta_tags'] = 'Meta tags';
$lang['adc_about_us'] = 'About us';
$lang['adc_tours_incoming'] = 'Incoming tours';
$lang['adc_tours_outgoing'] = 'Outgoing tours';
$lang['adc_tours_educational'] = 'Educational tours';
$lang['adc_destination'] = 'Destination';
$lang['adc_book_hotel'] = 'Book hotel';
$lang['adc_visas_to_armenia'] = 'Visas to Armenia';
$lang['adc_visas_from_armenia'] = 'Visas from Armenia';
$lang['adc_not_decided'] = 'Not decided';
$lang['adc_contact_us'] = 'Contact us';
$lang['adc_meta_og_type'] = 'Meta og:type';
$lang['adc_meta_og_type_explanation'] = 'On sharing the site on facebook, this is detected as type of your business, usually set as \'company\'';
$lang['adc_meta_og_image'] = 'og:image';
$lang['adc_meta_og_image_explanation'] = 'On sharing the site on facebook, this image is shown';
$lang['adc_meta_og_url'] = 'Meta og:url';
$lang['adc_meta_og_url_explanation'] = 'On sharing the site on facebook, this is detected as site\'s url. Usually it is set to site\'s home page url';
$lang['adc_meta_og_title'] = 'Meta og:title';
$lang['adc_meta_og_title_explanation'] = 'On sharing the site on facebook, this is shown as the title of the post';
$lang['adc_meta_og_description'] = 'Meta og:description';
$lang['adc_meta_og_description_explanation'] = 'On sharing the site on facebook, this is shown as the description of the post';
$lang['adc_meta_keywords'] = 'Meta keywords';
$lang['adc_meta_keywords_explanation'] = 'Comma separated words or phrases, that describe the page. It is recommended to have 8-15 words/phrases, with total length of 70-160 characters. It is used by Google or other Search engines to detect the page.';
$lang['adc_meta_description'] = 'Meta description';
$lang['adc_meta_description_explanation'] = 'Description of the page that is shown as a result of search in Search Engines (Google, Bing, Yandex...). The optimal length is 70-160 characters';
$lang['adc_color_default'] = 'Countries default color';
$lang['adc_color_default_hover'] = 'Countries default color on mouse over';
$lang['adc_not_selected'] = 'Not Selected';
$lang['adc_feedbacks'] = 'Customers feedback';
$lang['adc_customer_name'] = 'Customer\'s name';
$lang['adc_score'] = 'Score';
$lang['adc_gallery'] = 'Gallery';
$lang['adc_tours'] = 'Tours';
$lang['adc_incoming'] = 'Incoming';
$lang['adc_outgoing'] = 'Outgoing';
$lang['adc_educational'] = 'Educational';
$lang['adc_main_image'] = 'Main Image';
$lang['adc_description'] = 'Description';
$lang['adc_short_description'] = 'Short Description';
$lang['adc_long_description'] = 'Long Description';
$lang['adc_attached_countries'] = 'Countries';
$lang['adc_you_can_select_several'] = 'You can choose several';
$lang['adc_source_code_protection'] = 'Source code of this project is protected by Intellectual property law of Republic of Armenia. ';
$lang['adc_adc_production'] = 'ADC Production';
$lang['adc_criminal'] = 'Source code of this project is protected by Intellectual property law of Republic of Armenia. ';
$lang['adc_common_settings'] = 'Common settings';
$lang['adc_mail_contact_us'] = 'Email for "Contact Us" page';
$lang['adc_mail_lets_plan_your_trip'] = 'Email for "Lets plan your trip"';
$lang['adc_mail_order_tour'] = 'Email for order tour';
$lang['adc_url_facebook'] = 'url of Facebook page';
$lang['adc_url_google_plus'] = 'url of Google plus page';
$lang['adc_twitter'] = 'url of Twitter plus page';
$lang['adc_pinterest'] = 'url of Pinterest plus page';
$lang['adc_linked_in'] = 'url of Linked In plus page';
$lang['adc_about_us'] = 'About us';
$lang['adc_why_us'] = 'Why us';
$lang['adc_visas'] = 'Visas';
$lang['adc_to_armenia'] = 'To Armenia';
$lang['adc_from_armenia'] = 'From Armenia';
$lang['adc_without_visa'] = 'Without visa or upon arrival';
$lang['adc_visa_on_border'] = 'Visa on the border';
$lang['adc_visa_before_trip'] = 'Visa before trip';
$lang['adc_translations'] = 'Translations';
$lang['adc_partners'] = 'Partners';
$lang['adc_destination'] = 'Destination';



























